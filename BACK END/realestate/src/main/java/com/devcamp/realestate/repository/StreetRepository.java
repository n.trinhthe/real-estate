package com.devcamp.realestate.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.realestate.model.Street;
import com.devcamp.realestate.model.custom.IStreetCountEstate;

public interface StreetRepository extends JpaRepository<Street, Integer>{
    Optional<Street> findById(Integer id);

    @Query(value = "SELECT s.id AS id, s._name AS name, s._prefix AS prefix , COUNT(re.street_id) AS countEstate "  
    + "FROM street AS s "
    + "LEFT JOIN realestate AS re ON re.street_id = s.id "
    + "GROUP BY s._name "
    + "ORDER BY s.id "
    ,nativeQuery = true)
    List<IStreetCountEstate> findAllStreet();
}

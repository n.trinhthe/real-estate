package com.devcamp.realestate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.model.Customer;
import com.devcamp.realestate.repository.CustomerRepository;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    public List<Customer> getAllCustomer() {
        ArrayList<Customer> customerList = new ArrayList<>();
        customerRepository.findAll().forEach(customerList::add);
        return customerList;
        // return customerRepository.getAllCustomer();
    }

    public Customer findCustomerById(Integer id) {
        Optional<Customer> customerData = customerRepository.findById(id);
        if (customerData.isPresent()) {
            return customerData.get();
        } else {
            return null;
        }
    }

    public Customer createCustomer(Customer pCustomer) {
        Customer newCustomer = new Customer();
        newCustomer.setContactName(pCustomer.getContactName());
        newCustomer.setEmail(pCustomer.getEmail());
        newCustomer.setMobile(pCustomer.getMobile());
        newCustomer.setAddress(pCustomer.getAddress());
        newCustomer.setContactTitle(pCustomer.getContactTitle());
        newCustomer.setNote(pCustomer.getNote());
        Customer createdCustomer = customerRepository.save(newCustomer);
        return createdCustomer;
    }

    public Customer updateCustomer(Customer pCustomer, Integer id) {
        Optional<Customer> customerData = customerRepository.findById(id);
        if (customerData.isPresent()) {
            Customer foundCustomer = customerData.get();
            foundCustomer.setContactName(pCustomer.getContactName());
            foundCustomer.setEmail(pCustomer.getEmail());
            foundCustomer.setMobile(pCustomer.getMobile());
            foundCustomer.setAddress(pCustomer.getAddress());
            foundCustomer.setContactTitle(pCustomer.getContactTitle());
            foundCustomer.setNote(pCustomer.getNote());
            Customer updatedCustomer = customerRepository.save(foundCustomer);
            return updatedCustomer;
        } else {
            return null;
        }
    }

    public void deleteCutomer(Integer id){
        customerRepository.deleteById(id);
    }

    public void deleteAllCutomer(){
        customerRepository.deleteAll();;
    }

    public List<Customer> findCustomerByEmployee(Integer employeeId){
        return customerRepository.findCustomerByEmployee(employeeId);
    }

    public List<Customer> findCustomerBySeller(Integer employeeId){
        return customerRepository.findCustomerBySeller(employeeId);
    }
}

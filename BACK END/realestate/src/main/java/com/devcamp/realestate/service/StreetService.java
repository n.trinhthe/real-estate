package com.devcamp.realestate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.model.District;
import com.devcamp.realestate.model.Street;
import com.devcamp.realestate.model.custom.IStreetCountEstate;
import com.devcamp.realestate.repository.DistrictRepository;
import com.devcamp.realestate.repository.StreetRepository;

@Service
public class StreetService {
    @Autowired
    StreetRepository streetRepository;
    @Autowired
    DistrictRepository districtRepository;

    public List<Street> getAllStreets() {
        ArrayList<Street> streetList = new ArrayList<>();
        streetRepository.findAll().forEach(streetList::add);
        return streetList;
    }

    public List<IStreetCountEstate> getAllStreetCount() {
        return streetRepository.findAllStreet();
    }

    public Street getStreetById(Integer id) {
        Optional<Street> streetData = streetRepository.findById(id);
        if (streetData.isPresent()) {
            return streetData.get();
        } else {
            return null;
        }
    }

    public Street createStreet(Street pStreet){
        Street newStreet = new Street();
        newStreet.setName(pStreet.getName());
        newStreet.setPrefix(pStreet.getPrefix());
        newStreet.setDistrict(pStreet.getDistrict());
        streetRepository.save(newStreet);
        return newStreet;
    }

    public Street updateStreetById(Street pStreet, Integer id) {
        Optional<Street> streetData = streetRepository.findById(id);
        if (streetData.isPresent()) {
            Street updatedStreet = streetData.get();
            updatedStreet.setName(pStreet.getName());
            updatedStreet.setPrefix(pStreet.getPrefix());
            updatedStreet.setDistrict(pStreet.getDistrict());
            streetRepository.save(updatedStreet);
            return updatedStreet;
        } else {
            return null;
        }
    }

    public void deleteStreet(Integer id){
        streetRepository.deleteById(id);
    }

    public Set<Street> getStreetsOfDistrict(Integer districtId) {
        Optional<District> districtData = districtRepository.findById(districtId);
        if (districtData.isPresent()) {
            return districtData.get().getStreets();
        } else {
            return null;
        }
    }
}

package com.devcamp.realestate.model.custom;

public interface IDistrictCountEstate {
    Integer getId();
    String getName();
    String getPrefix();
    Integer getCountEstate();
    // Integer getCountProject();
}

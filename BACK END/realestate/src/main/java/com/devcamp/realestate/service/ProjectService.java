package com.devcamp.realestate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.model.District;
import com.devcamp.realestate.model.Project;
import com.devcamp.realestate.repository.ConstructionContractorRepository;
import com.devcamp.realestate.repository.DesignUnitRepository;
import com.devcamp.realestate.repository.DistrictRepository;
import com.devcamp.realestate.repository.InvestorRepository;
import com.devcamp.realestate.repository.ProjectRepository;
import com.devcamp.realestate.repository.ProvinceRepository;
import com.devcamp.realestate.repository.StreetRepository;
import com.devcamp.realestate.repository.WardRepository;

@Service
public class ProjectService {
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    ProvinceRepository provinceRepository;
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    WardRepository wardRepository;
    @Autowired
    StreetRepository streetRepository;
    @Autowired
    InvestorRepository investorRepository;
    @Autowired
    ConstructionContractorRepository constructionContractorRepository;
    @Autowired
    DesignUnitRepository designUnitRepository;

    public List<Project> getAllProjects() {
        ArrayList<Project> projectList = new ArrayList<>();
        projectRepository.findAll().forEach(projectList::add);
        return projectList;
    }

    public Project getProjectById(Integer id) {
        Optional<Project> projectData = projectRepository.findById(id);
        if (projectData.isPresent()) {
            return projectData.get();
        } else {
            return null;
        }
    }

    public Project createProject(Project pProject) {
        Project newProject = new Project();
        newProject.setName(pProject.getName());
        newProject.setProvince(pProject.getProvince());
        newProject.setDistrict(pProject.getDistrict());
        newProject.setWard(pProject.getWard());
        newProject.setStreet(pProject.getStreet());
        newProject.setAddress(pProject.getAddress());
        newProject.setSlogan(pProject.getSlogan());
        newProject.setDescription(pProject.getDescription());
        newProject.setAcreage(pProject.getAcreage());
        newProject.setConstructArea(pProject.getConstructArea());
        newProject.setNumBlock(pProject.getNumBlock());
        newProject.setNumFloors(pProject.getNumFloors());
        newProject.setNumApartment(pProject.getNumApartment());
        newProject.setApartmenttArea(pProject.getApartmenttArea());
        newProject.setInvestor(pProject.getInvestor());
        newProject.setConstructionContractor(pProject.getConstructionContractor());
        newProject.setDesignUnit(pProject.getDesignUnit());
        newProject.setUtilities(pProject.getUtilities());
        newProject.setRegionLink(pProject.getRegionLink());
        newProject.setPhoto(pProject.getPhoto());
        newProject.setLat(pProject.getLat());
        newProject.setLng(pProject.getLng());
        Project createdProject =  projectRepository.save(newProject);
        return createdProject;
    }

    public Project updateProjectById(Project pProject, Integer id) {
        Optional<Project> projectData = projectRepository.findById(id);
        if (projectData.isPresent()) {
            Project foundProject = projectData.get();
            foundProject.setName(pProject.getName());
            foundProject.setProvince(pProject.getProvince());
            foundProject.setDistrict(pProject.getDistrict());
            foundProject.setWard(pProject.getWard());
            foundProject.setStreet(pProject.getStreet());
            foundProject.setAddress(pProject.getAddress());
            foundProject.setSlogan(pProject.getSlogan());
            foundProject.setDescription(pProject.getDescription());
            foundProject.setAcreage(pProject.getAcreage());
            foundProject.setConstructArea(pProject.getConstructArea());
            foundProject.setNumBlock(pProject.getNumBlock());
            foundProject.setNumFloors(pProject.getNumFloors());
            foundProject.setNumApartment(pProject.getNumApartment());
            foundProject.setApartmenttArea(pProject.getApartmenttArea());
            foundProject.setInvestor(pProject.getInvestor());
            foundProject.setConstructionContractor(pProject.getConstructionContractor());
            foundProject.setDesignUnit(pProject.getDesignUnit());
            foundProject.setUtilities(pProject.getUtilities());
            foundProject.setRegionLink(pProject.getRegionLink());
            foundProject.setPhoto(pProject.getPhoto());
            foundProject.setLat(pProject.getLat());
            foundProject.setLng(pProject.getLng());
            Project updatedProject =  projectRepository.save(foundProject);
            return updatedProject;
        } else {
            return null;
        }
    }

    public void deleteProject(Integer id) {
        projectRepository.deleteById(id);
    }

    public Set<Project> getProjectsOfDistrict(Integer districtId) {
        Optional<District> districtData = districtRepository.findById(districtId);
        if (districtData.isPresent()) {
            return districtData.get().getProjects();
        } else {
            return null;
        }
    }

    public List<Project> findProjectByProvince(Integer provinceId) {
        List<Project> listEstate = new ArrayList<>();
        listEstate = projectRepository.findProjectByProvince(provinceId);
        return listEstate;
    }

    public List<Project> findProjectByDistrict(Integer districtId) {
        List<Project> listEstate = new ArrayList<>();
        listEstate = projectRepository.findProjectByDistrict(districtId);
        return listEstate;
    }

    public List<Project> findProjectByWard(Integer wardId) {
        List<Project> listEstate = new ArrayList<>();
        listEstate = projectRepository.findProjectByWard(wardId);
        return listEstate;
    }
}

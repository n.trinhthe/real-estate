package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.model.Province;
import com.devcamp.realestate.model.custom.IProvinceCountEstate;
import com.devcamp.realestate.service.ProvinceService;

@RestController
@CrossOrigin
public class ProvinceController {
    @Autowired
    ProvinceService provinceService;
    
    @GetMapping("/provinces")
    public ResponseEntity<List<Province>> getAllProvince() {
        try {
            return new ResponseEntity<>(provinceService.getAllProvince(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/provinces/count")
    public ResponseEntity<List<IProvinceCountEstate>> getAllProvinceCount() {
        try {
            return new ResponseEntity<>(provinceService.getAllProvinceCount(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces/{id}")
    public ResponseEntity<Province> getProvinceById(@PathVariable("id") Integer id) {
        Province findProvince = provinceService.getProvinceById(id);
        if (findProvince != null) {
            try {
                return new ResponseEntity<>(findProvince, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/provinces")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Province> createProvince(@RequestBody Province pProvince) {
        Province newProvince = provinceService.createProvince(pProvince);
        try {
            return new ResponseEntity<>(newProvince, HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/provinces/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Province> updateProvince(@RequestBody Province pProvince, @PathVariable("id") Integer id) {
        Province updateProvince = provinceService.updateProvince(pProvince, id);
        if (updateProvince != null) {
            try {
                return new ResponseEntity<>(updateProvince, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/provinces/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Object> deleteProvince(@PathVariable("id") Integer id) {
        try {
            provinceService.deleteProvince(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/provinces", params = "code")
    public ResponseEntity<Object> getProvinceByCode(@RequestParam(value = "code") String code) {
        Province province = provinceService.getProvinceByCode(code);
        if (province != null) {
            try {
                return new ResponseEntity<>(province, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}

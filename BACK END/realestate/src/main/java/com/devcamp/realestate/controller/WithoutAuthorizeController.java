package com.devcamp.realestate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Employee;
import com.devcamp.realestate.repository.EmployeeRepository;
import com.devcamp.realestate.service.EmployeeService;

@RestController
public class WithoutAuthorizeController {
	@Autowired
	private EmployeeRepository employeeRepository;
	
    /**
     * Test trường hợp khôngcheck quyền Authorize lấy là danh sách employee
     * @return
     */
    // @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    // @GetMapping("/employees")
    // public ResponseEntity<List<Object>> getEmployees() {
    //     return new ResponseEntity(employeeRepository.findAll(),HttpStatus.OK);
    // }
    /**
     * Test trường hợp khôngcheck quyền Authorize
     * Tạo mới employee
     * @param employee
     * @return
     */
    @PostMapping("/employees/createAdmin")
    public ResponseEntity<Object> create(@RequestBody Employee employee) {
    	employee.setPassword(new BCryptPasswordEncoder().encode(employee.getPassword()));
    	return new ResponseEntity(employeeRepository.save(employee),HttpStatus.CREATED);
    }
    
    @Autowired
    private EmployeeService employeeService;
    /**
     * Test có kiểm tra quyền.
     * @param employee
     * @return
     */
    @PostMapping("/employees/create")
    @PreAuthorize("hasAnyAuthority('USER_CREATE')")
    public Employee register(@RequestBody Employee employee) {
        employee.setPassword(new BCryptPasswordEncoder().encode(employee.getPassword()));
        return employeeService.createEmployee(employee);
    }
}

package com.devcamp.realestate.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.realestate.model.Image;

public interface ImageRepository extends JpaRepository<Image, Long> {
    Optional<Image> findByName(String name);

    Boolean existsImageByName(String name);

    void deleteImageByName(String name);

    @Query(value = "DELETE FROM image AS i WHERE i.name = :name", nativeQuery = true)
    void deleteImage(@Param("name") String name);
}

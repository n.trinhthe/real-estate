package com.devcamp.realestate.controller;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.realestate.entity.Employee;
import com.devcamp.realestate.security.UserPrincipal;
import com.devcamp.realestate.service.impl.EmployeeServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin
public class EmployeeController {
    @Autowired
    EmployeeServiceImpl employeeServiceeImpl;

    @GetMapping("/employees")
    // @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<List<Employee>> getAllEmployee() {
        try {
            return new ResponseEntity<>(employeeServiceeImpl.getAllEmployee(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/employees")
    public ResponseEntity<Object> createEmployee(@Valid @RequestBody Employee employee) {
        Employee createdEmployee = employeeServiceeImpl.createEmployee(employee);
        try {
            return new ResponseEntity<>(createdEmployee, HttpStatus.CREATED);
        } catch (Exception ex) {
            return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
        }
    }

    @GetMapping("/employees/{id}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Integer id) {
        Employee foundEmployee = employeeServiceeImpl.findEmployeeById(id);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        if (foundEmployee != null) {
            try {
                if (authentication.getAuthorities().toString().contains("ROLE_ADMIN")
                        || authentication.getAuthorities().toString().contains("ROLE_MANAGER")) {
                    return new ResponseEntity<Employee>(foundEmployee, HttpStatus.OK);
                } else {
                    if (userPrincipal.getUserId().equals(id)) {
                        return new ResponseEntity<Employee>(foundEmployee, HttpStatus.OK);
                    } else {
                        return new ResponseEntity<>(null, HttpStatus.NON_AUTHORITATIVE_INFORMATION);
                    }
                }
            } catch (Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/employees/{id}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Object> updateEmployee(@Valid @RequestParam("employee") String pEmployee,
            @Valid @PathVariable("id") Integer id,
            @Valid @RequestParam("image") MultipartFile file) throws IOException {
        // Chuyển Object Employee từ JSON
        ObjectMapper mapper = new ObjectMapper();
        Employee employee = mapper.readValue(pEmployee, Employee.class);
        Employee updatedEmployee = employeeServiceeImpl.updateEmployee(employee, id, file);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        if (updatedEmployee != null) {
            try {
                if (authentication.getAuthorities().toString().contains("ROLE_ADMIN")
                        || authentication.getAuthorities().toString().contains("ROLE_MANAGER")) {
                    return new ResponseEntity<Object>(updatedEmployee, HttpStatus.OK);
                } else {
                    if (userPrincipal.getUserId().equals(id)) {
                        return new ResponseEntity<Object>(updatedEmployee, HttpStatus.OK);
                    } else {
                        return new ResponseEntity<>(null, HttpStatus.NON_AUTHORITATIVE_INFORMATION);
                    }
                }
            } catch (Exception ex) {
                return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/employees/{id}/profile")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Object> updateEmployeeProfile(@RequestBody Employee pEmployee,
            @PathVariable("id") Integer id) {
        Employee updatedEmployee = employeeServiceeImpl.updateEmployeeProfile(pEmployee, id);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        if (updatedEmployee != null) {
            try {
                if (authentication.getAuthorities().toString().contains("ROLE_ADMIN")
                        || authentication.getAuthorities().toString().contains("ROLE_MANAGER")) {
                    return new ResponseEntity<Object>(updatedEmployee, HttpStatus.OK);
                } else {
                    if (userPrincipal.getUserId().equals(id)) {
                        return new ResponseEntity<Object>(updatedEmployee, HttpStatus.OK);
                    } else {
                        return new ResponseEntity<>(null, HttpStatus.NON_AUTHORITATIVE_INFORMATION);
                    }
                }
            } catch (Exception ex) {
                return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/employees/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable("id") Integer id) {
        try {
            employeeServiceeImpl.deleteEmployee(id);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/employees/all")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Employee> deleteAllEmployee() {
        try {
            employeeServiceeImpl.deleteAllEmployee();
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

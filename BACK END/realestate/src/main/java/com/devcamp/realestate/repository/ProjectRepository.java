package com.devcamp.realestate.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.model.Project;

public interface ProjectRepository extends JpaRepository<Project, Integer>{
    Optional<Project> findById(Integer id);

    List<Project> findProjectByProvince(Integer provinceId);
    
    List<Project> findProjectByDistrict(Integer districtId);

    List<Project> findProjectByWard(Integer wardId);

    List<Project> findProjectByStreet(Integer streetId);
}

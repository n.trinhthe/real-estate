package com.devcamp.realestate.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "realestate")
@EntityListeners(AuditingEntityListener.class)
public class RealEstate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;
    private Integer type;
    private Integer request;

    @Column(name = "province_id")
    private Integer provinceId;

    @Column(name = "district_id")
    private Integer districtId;

    @Column(name = "wards_id")
    private Integer wardId;

    @Column(name = "street_id")
    private Integer streetId;

    @Column(name = "project_id")
    private Integer projectId;

    @Column(name = "address")
    private String address;

    @Column(name = "customer_id")
    private Integer customerId;

    private Long price;

    @Column(name = "price_min")
    private Long priceMin;

    @Column(name = "price_time")
    private Byte priceTime;

    @Column(name = "date_create")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @CreatedDate
    private Date dateCreate;

    private BigDecimal acreage;
    private Integer direction;

    @Column(name = "total_floors")
    private Integer totalFloors;

    @Column(name = "number_floors")
    private Integer numberFloors;
    private Integer bath;

    @Column(name = "apart_code",nullable = true)
    private String apartCode;

    @Column(name = "wall_area")
    private BigDecimal wallArea;

    private Byte bedroom;
    private Byte balcony;

    @Column(name = "landscape_view")
    private String landscapeView;

    @Column(name = "apart_loca")
    private Byte apartLoca;

    @Column(name = "apart_type")
    private Byte apartType;

    @Column(name = "furniture_type")
    private Byte furnitureType;

    @Column(name = "price_rent")
    private Integer priceRent;

    @Column(name = "return_rate")
    private Double returnRate;

    @Column(name = "legal_doc")
    private Integer legalDoc;
    private String description;

    @Column(name = "width_y")
    private Integer widthY;

    @Column(name = "long_x")
    private Integer longX;

    @Column(name = "street_house")
    private Byte streetHouse;
    private Byte FSBO;

    @Column(name = "view_num")
    private Integer viewNum;

    @Column(name = "create_by")
    @CreatedBy
    private Integer createBy;

    @Column(name = "update_by")
    @LastModifiedBy
    private Integer updateBy;

    private String shape;
    private Integer distance2facade;

    @Column(name = "adjacent_facade_num")
    private Integer adjacentFacadeNum;

    @Column(name = "adjacent_road")
    private String adjacentRoad;

    @Column(name = "alley_min_width")
    private Integer alleyMinWidth;

    @Column(name = "adjacent_alley_min_width")
    private Integer adjacentAlleyMinWidth;
    private Integer factor;
    private String structure;
    private Integer DTSXD;
    private Integer CLCL;

    @Column(name = "CTXD_price")
    private Integer CTXDPrice;
    @Column(name = "CTXD_value")
    private Integer CTXDValue;
    private String photo;
    @Column(name = "_lat")
    private Double lat;
    @Column(name = "_lng")
    private Double lng;
    @Column(name = "censor")
    private Boolean censorred = false;

    public RealEstate() {
    }


    public RealEstate(String title, Integer type, Integer request, Integer provinceId, Integer districtId, Integer wardId,
    Integer streetId, Integer projectId, String address, Integer customerId, Long price, Long priceMin,
            Byte priceTime, Date dateCreate, BigDecimal acreage, Integer direction, Integer totalFloors,
            Integer numberFloors,
            Integer bath, String apartCode, BigDecimal wallArea, Byte bedroom, Byte balcony, String landscapeView,
            Byte apartLoca, Byte apartType, Byte furnitureType, Integer priceRent, Double returnRate, Integer legalDoc,
            String description, Integer widthY, Integer longX, Byte streetHouse, Byte FSBO, Integer viewNum,
            Integer createBy,
            Integer updateBy, String shape, Integer distance2facade, Integer adjacentFacadeNum, String adjacentRoad,
            Integer alleyMinWidth, Integer adjacentAlleyMinWidth, Integer factor, String structure, Integer DTSXD,
            Integer CLCL,
            Integer CTXDPrice, Integer CTXDValue, String photo, Double lat, Double lng, Boolean censorred) {
        this.title = title;
        this.type = type;
        this.request = request;
        this.provinceId = provinceId;
        this.districtId = districtId;
        this.wardId = wardId;
        this.streetId = streetId;
        this.projectId = projectId;
        this.address = address;
        this.customerId = customerId;
        this.price = price;
        this.priceMin = priceMin;
        this.priceTime = priceTime;
        this.dateCreate = dateCreate;
        this.acreage = acreage;
        this.direction = direction;
        this.totalFloors = totalFloors;
        this.numberFloors = numberFloors;
        this.bath = bath;
        this.apartCode = apartCode;
        this.wallArea = wallArea;
        this.bedroom = bedroom;
        this.balcony = balcony;
        this.landscapeView = landscapeView;
        this.apartLoca = apartLoca;
        this.apartType = apartType;
        this.furnitureType = furnitureType;
        this.priceRent = priceRent;
        this.returnRate = returnRate;
        this.legalDoc = legalDoc;
        this.description = description;
        this.widthY = widthY;
        this.longX = longX;
        this.streetHouse = streetHouse;
        this.FSBO = FSBO;
        this.viewNum = viewNum;
        this.createBy = createBy;
        this.updateBy = updateBy;
        this.shape = shape;
        this.distance2facade = distance2facade;
        this.adjacentFacadeNum = adjacentFacadeNum;
        this.adjacentRoad = adjacentRoad;
        this.alleyMinWidth = alleyMinWidth;
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
        this.factor = factor;
        this.structure = structure;
        this.DTSXD = DTSXD;
        this.CLCL = CLCL;
        this.CTXDPrice = CTXDPrice;
        this.CTXDValue = CTXDValue;
        this.photo = photo;
        this.lat = lat;
        this.lng = lng;
        this.censorred = censorred;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRequest() {
        return request;
    }

    public void setRequest(Integer request) {
        this.request = request;
    }

    

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(Long priceMin) {
        this.priceMin = priceMin;
    }

    public Byte getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(Byte priceTime) {
        this.priceTime = priceTime;
    }
    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public Integer getTotalFloors() {
        return totalFloors;
    }

    public void setTotalFloors(Integer totalFloors) {
        this.totalFloors = totalFloors;
    }

    public Integer getNumberFloors() {
        return numberFloors;
    }

    public void setNumberFloors(Integer numberFloors) {
        this.numberFloors = numberFloors;
    }

    public Integer getBath() {
        return bath;
    }

    public void setBath(Integer bath) {
        this.bath = bath;
    }

    public String getApartCode() {
        return apartCode;
    }

    public void setApartCode(String apartCode) {
        this.apartCode = apartCode;
    }

    public BigDecimal getWallArea() {
        return wallArea;
    }

    public void setWallArea(BigDecimal wallArea) {
        this.wallArea = wallArea;
    }

    public Byte getBedroom() {
        return bedroom;
    }

    public void setBedroom(Byte bedroom) {
        this.bedroom = bedroom;
    }

    public Byte getBalcony() {
        return balcony;
    }

    public void setBalcony(Byte balcony) {
        this.balcony = balcony;
    }

    public String getLandscapeView() {
        return landscapeView;
    }

    public void setLandscapeView(String landscapeView) {
        this.landscapeView = landscapeView;
    }

    public Byte getApartLoca() {
        return apartLoca;
    }

    public void setApartLoca(Byte apartLoca) {
        this.apartLoca = apartLoca;
    }

    public Byte getFurnitureType() {
        return furnitureType;
    }

    public void setFurnitureType(Byte furnitureType) {
        this.furnitureType = furnitureType;
    }

    public Integer getPriceRent() {
        return priceRent;
    }

    public void setPriceRent(Integer priceRent) {
        this.priceRent = priceRent;
    }

    public Double getReturnRate() {
        return returnRate;
    }

    public void setReturnRate(Double returnRate) {
        this.returnRate = returnRate;
    }

    public Integer getLegalDoc() {
        return legalDoc;
    }

    public void setLegalDoc(Integer legalDoc) {
        this.legalDoc = legalDoc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getWidthY() {
        return widthY;
    }

    public void setWidthY(Integer widthY) {
        this.widthY = widthY;
    }

    public Integer getLongX() {
        return longX;
    }

    public void setLongX(Integer longX) {
        this.longX = longX;
    }

    public Byte getStreetHouse() {
        return streetHouse;
    }

    public void setStreetHouse(Byte streetHouse) {
        this.streetHouse = streetHouse;
    }

    public Byte getFSBO() {
        return FSBO;
    }

    public void setFSBO(Byte fSBO) {
        FSBO = fSBO;
    }

    public Integer getViewNum() {
        return viewNum;
    }

    public void setViewNum(Integer viewNum) {
        this.viewNum = viewNum;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public Integer getDistance2facade() {
        return distance2facade;
    }

    public void setDistance2facade(Integer distance2facade) {
        this.distance2facade = distance2facade;
    }

    public Integer getAdjacentFacadeNum() {
        return adjacentFacadeNum;
    }

    public void setAdjacentFacadeNum(Integer adjacentFacadeNum) {
        this.adjacentFacadeNum = adjacentFacadeNum;
    }

    public String getAdjacentRoad() {
        return adjacentRoad;
    }

    public void setAdjacentRoad(String adjacentRoad) {
        this.adjacentRoad = adjacentRoad;
    }

    public Integer getAlleyMinWidth() {
        return alleyMinWidth;
    }

    public void setAlleyMinWidth(Integer alleyMinWidth) {
        this.alleyMinWidth = alleyMinWidth;
    }

    public Integer getAdjacentAlleyMinWidth() {
        return adjacentAlleyMinWidth;
    }

    public void setAdjacentAlleyMinWidth(Integer adjacentAlleyMinWidth) {
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
    }

    public Integer getFactor() {
        return factor;
    }

    public void setFactor(Integer factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public Integer getDTSXD() {
        return DTSXD;
    }

    public void setDTSXD(Integer dTSXD) {
        DTSXD = dTSXD;
    }

    public Integer getCLCL() {
        return CLCL;
    }

    public void setCLCL(Integer cLCL) {
        CLCL = cLCL;
    }

    public Integer getCTXDPrice() {
        return CTXDPrice;
    }

    public void setCTXDPrice(Integer cTXDPrice) {
        CTXDPrice = cTXDPrice;
    }

    public Integer getCTXDValue() {
        return CTXDValue;
    }

    public void setCTXDValue(Integer cTXDValue) {
        CTXDValue = cTXDValue;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Byte getApartType() {
        return apartType;
    }

    public void setApartType(Byte apartType) {
        this.apartType = apartType;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }
    public Integer getProvinceId() {
        return provinceId;
    }
    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }
    public Integer getDistrictId() {
        return districtId;
    }
    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }
    public Integer getWardId() {
        return wardId;
    }
    public void setWardId(Integer wardId) {
        this.wardId = wardId;
    }
    public Integer getStreetId() {
        return streetId;
    }
    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }
    public Integer getProjectId() {
        return projectId;
    }
    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
    public Integer getCustomerId() {
        return customerId;
    }
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
    public Boolean getCensorred() {
        return censorred;
    }
    public void setCensorred(Boolean censorred) {
        this.censorred = censorred;
    }
}

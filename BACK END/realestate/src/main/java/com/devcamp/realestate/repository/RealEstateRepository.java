package com.devcamp.realestate.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.realestate.model.RealEstate;

public interface RealEstateRepository extends JpaRepository<RealEstate, Integer> {
        Optional<RealEstate> findById(Integer id);

        @Query(value = "SELECT * FROM realestate WHERE 1", nativeQuery = true)
        Page<RealEstate> findAllRealEstate(Pageable pageable);

        List<RealEstate> findByCustomerId(Integer customerId);

        @Query(nativeQuery = true, value = "SELECT * FROM realestate WHERE realestate.create_by = :employeeId")
        Page<RealEstate> findEstateByEmployee(Integer employeeId, Pageable pageable);

        @Query(nativeQuery = true, value = "SELECT * FROM realestate AS re "
                        + "JOIN employees AS e ON re.create_by = e.EmployeeID "
                        + "WHERE re.create_by = :employeeId "
                        + "OR e.ReportsTo = :employeeId "
                        + "ORDER BY re.id", countQuery = "SELECT * FROM realestate AS re "
                                        + "JOIN employees AS e ON re.create_by = e.EmployeeID "
                                        + "WHERE re.create_by = :employeeId "
                                        + "OR e.ReportsTo = :employeeId ")
        Page<RealEstate> findEstateBySeller(@Param("employeeId") Integer employeeId, Pageable pageable);

        @Query(nativeQuery = true, value = "SELECT * FROM realestate WHERE realestate.censor = 1")
        Page<RealEstate> findEstateCensorred(Pageable pageable);

        @Query(value = "SELECT * FROM realestate AS re "
                        + "LEFT JOIN  province AS p ON re.province_id = p.id "
                        + "LEFT JOIN district AS d ON re.district_id = d.id "
                        + "LEFT JOIN ward AS w ON re.wards_id = w.id "
                        + "LEFT JOIN street AS s ON re.street_id = s.id "
                        + "WHERE re.censor = 1 "
                        + "AND IF(:acreage = '',re.acreage >=0 OR re.acreage IS NULL,re.acreage >= :acreage) "
                        + "AND re.price <= :price "
                        + "AND re.request = :request "
                        + "AND re.type = :type "
                        + "AND (p._name LIKE %:title% OR d._name LIKE %:title% OR w._name LIKE %:title% OR s._name LIKE %:title%) ", nativeQuery = true)
        List<RealEstate> findEstateSearch(@Param("title") String title, @Param("request") Integer request,
                        @Param("type") Integer type, @Param("price") Integer price,
                        @Param("acreage") BigDecimal acreage);

        // @Query(nativeQuery = true, value = "SELECT * FROM realestate AS re "
        // + "JOIN province AS p ON re.province_id = p.id "
        // + "JOIN district AS d ON re.district_id = d.id "
        // + "JOIN ward AS w ON re.wards_id = w.id "
        // + "JOIN street AS s ON re.street_id = s.id "
        // + "WHERE re.censor = 1 "
        // + "AND re.request = :request "
        // + "AND re.type = :type "
        // + "AND (IF (:acreage = '',re.acreage != '',re.acreage >= :acreage )) "
        // + "AND (p._name LIKE %:title% OR d._name LIKE %:title% OR w._name LIKE
        // %:title% OR s._name LIKE %:title%) "
        // + "AND re.price <= :price")
        // List<RealEstate> findEstateSearch(String title, Integer request, Integer
        // type, Integer price, BigDecimal acreage);

        @Query(nativeQuery = true, value = "SELECT * FROM realestate AS re "
                        + "WHERE CASE "
                        + "WHEN :type != '' THEN (re.censor = 1 AND  re.type = :type) "
                        + "WHEN :type = '' THEN re.censor = 1 "
                        + "END "
                        + "ORDER BY RAND() LIMIT 4")
        List<RealEstate> findTypeProperty(String type);

        @Query(value = "SELECT * FROM realestate AS re "
                        + "LEFT JOIN province AS p ON re.province_id = p.id "
                        + "LEFT JOIN district AS d ON re.district_id = d.id "
                        + "LEFT JOIN ward AS w ON re.wards_id = w.id "
                        + "LEFT JOIN street AS s ON re.street_id = s.id "
                        + "WHERE re.title LIKE %:search% "
                        + "OR re.request LIKE %:search% "
                        + "OR p._name LIKE %:search% "
                        + "OR d._name LIKE %:search% "
                        + "OR w._name LIKE %:search% "
                        + "OR s._name LIKE %:search% ", nativeQuery = true)
        List<RealEstate> findEstateSearch(@Param("search") String search);

        Page<RealEstate> findRealEstateByDistrictId(Integer districtId, Pageable pageable);

        Page<RealEstate> findRealEstateByProvinceId(Integer provinceId, Pageable pageable);

        Page<RealEstate> findRealEstateByWardId(Integer wardId, Pageable pageable);

        Page<RealEstate> findRealEstateByStreetId(Integer streetId, Pageable pageable);

        @Query(nativeQuery = true, value = "SELECT * FROM realestate AS re "
                        + "LEFT JOIN province AS p ON re.province_id = p.id "
                        + "LEFT JOIN district AS d ON re.district_id = d.id "
                        + "LEFT JOIN ward AS w ON re.wards_id = w.id "
                        + "LEFT JOIN street AS s ON re.street_id = s.id "
                        + "LEFT JOIN customers AS c ON re.customer_id = c.id "
                        + "WHERE 1 "
                        + "AND IF(:acreage = '' OR :acreage IS NULL,re.acreage >= 0 OR re.acreage IS NULL,re.acreage >= :acreage) "
                        + "AND IF(:priceMin = '' OR :priceMin IS NULL,(re.price >= 0 OR re.price IS NULL) ,re.price >= :priceMin) "
                        + "AND IF(:priceMax = '' OR :priceMax IS NULL,(re.price <= (SELECT MAX(re.price) FROM realestate)) ,re.price <= :priceMax) "
                        + "AND IF(:customerId = '' OR :customerId IS NULL,re.customer_id >=0 OR re.customer_id IS NULL,re.customer_id = :customerId) "
                        + "AND IF(:direction = '' OR :direction IS NULL,re.direction >=0 OR re.direction IS NULL,re.direction = :direction) "
                        + "AND IF(:request = '' OR :request IS NULL,re.request >= 0 OR re.request IS NULL,re.request = :request) "
                        + "AND IF(:type = '' OR :type IS NULL,re.type >= 0 OR re.type IS NULL,re.type = :type) "
                        + "AND (re.date_create >= IF(:startDate = '' OR :startDate IS NULL, 0, :startDate) AND re.date_create <= IF(:endDate = '' OR :endDate IS NULL, NOW(), :endDate)) "
                        + "AND IF(:censored = -1, re.censor >= 0 OR re.censor IS NULL, re.censor = :censored) "
                        + "AND (p._name LIKE %:location% OR d._name LIKE %:location% OR w._name LIKE %:location% OR s._name LIKE %:location%)", countQuery = "SELECT count(re.id) FROM realestate AS re "
                                        + "LEFT JOIN province AS p ON re.province_id = p.id "
                                        + "LEFT JOIN district AS d ON re.district_id = d.id "
                                        + "LEFT JOIN ward AS w ON re.wards_id = w.id "
                                        + "LEFT JOIN street AS s ON re.street_id = s.id "
                                        + "LEFT JOIN customers AS c ON re.customer_id = c.id "
                                        + "WHERE 1 "
                                        + "AND IF(:acreage = '' OR :acreage IS NULL,re.acreage >= 0 OR re.acreage IS NULL,re.acreage >= :acreage) "
                                        + "AND IF(:priceMin = '' OR :priceMin IS NULL,re.price >= 0 OR re.price IS NULL,re.price >= :priceMin) "
                                        + "AND IF(:priceMax = '' OR :priceMax IS NULL,re.price <= (SELECT MAX(re.price) FROM realestate),re.price <= :priceMax) "
                                        + "AND IF(:customerId = '' OR :customerId IS NULL,re.customer_id >=0 OR re.customer_id IS NULL,re.customer_id = :customerId) "
                                        + "AND IF(:direction = '' OR :direction IS NULL,re.direction >=0 OR re.direction IS NULL,re.direction = :direction) "
                                        + "AND IF(:request = '' OR :request IS NULL,re.request >= 0 OR re.request IS NULL,re.request = :request) "
                                        + "AND IF(:type = '' OR :type IS NULL,re.type >= 0 OR re.type IS NULL,re.type = :type) "
                                        + "AND (re.date_create >= IF(:startDate = '' OR :startDate IS NULL, 0, :startDate) AND re.date_create <= IF(:endDate = '' OR :endDate IS NULL, 0, :endDate)) "
                                        + "AND IF(:censored = -1, re.censor >= 0 OR re.censor IS NULL, re.censor = :censored) "
                                        + "AND (p._name LIKE %:location% OR d._name LIKE %:location% OR w._name LIKE %:location% OR s._name LIKE %:location%)")
        Page<RealEstate> findEstateAdvancedSearch(
                        @Param("location") String location,
                        @Param("request") Integer request,
                        @Param("type") Integer type,
                        @Param("direction") Integer direction,
                        @Param("priceMin") Long priceMin,
                        @Param("priceMax") Long priceMax,
                        @Param("acreage") BigDecimal acreage,
                        @Param("customerId") Integer customerId,
                        @Param("startDate") Date startDate,
                        @Param("endDate") Date endDate,
                        @Param("censored") Integer censored,
                        Pageable pageable);

        @Query(nativeQuery = true, value = "SELECT * FROM realestate AS re "
                        + "LEFT JOIN province AS p ON re.province_id = p.id "
                        + "LEFT JOIN district AS d ON re.district_id = d.id "
                        + "LEFT JOIN ward AS w ON re.wards_id = w.id "
                        + "LEFT JOIN street AS s ON re.street_id = s.id "
                        + "LEFT JOIN customers AS c ON re.customer_id = c.id "
                        + "WHERE re.create_by = :employeeId "
                        + "AND IF(:acreage = '' OR :acreage IS NULL,re.acreage >= 0 OR re.acreage IS NULL,re.acreage >= :acreage) "
                        + "AND IF(:priceMin = '' OR :priceMin IS NULL,(re.price >= 0 OR re.price IS NULL) ,re.price >= :priceMin) "
                        + "AND IF(:priceMax = '' OR :priceMax IS NULL,(re.price <= (SELECT MAX(re.price) FROM realestate)) ,re.price <= :priceMax) "
                        + "AND IF(:customerId = '' OR :customerId IS NULL,re.customer_id >= 0 OR re.customer_id IS NULL,re.customer_id = :customerId) "
                        + "AND IF(:direction = '' OR :direction IS NULL,re.direction >=0 OR re.direction IS NULL,re.direction = :direction) "
                        + "AND IF(:request = '' OR :request IS NULL,re.request >=0 OR re.request IS NULL,re.request = :request) "
                        + "AND IF(:type = '' OR :type IS NULL,re.type >=0 OR re.type IS NULL,re.type = :type) "
                        + "AND (re.date_create >= IF(:startDate = '' OR :startDate IS NULL, 0, :startDate) AND re.date_create <= IF(:endDate = '' OR :endDate IS NULL, NOW(), :endDate)) "
                        + "AND IF(:censored = -1, re.censor >= 0 OR re.censor IS NULL, re.censor = :censored) "
                        + "AND (p._name LIKE %:location% OR d._name LIKE %:location% OR w._name LIKE %:location% OR s._name LIKE %:location%)", countQuery = "SELECT count(re.id) FROM realestate AS re "
                                        + "LEFT JOIN province AS p ON re.province_id = p.id "
                                        + "LEFT JOIN district AS d ON re.district_id = d.id "
                                        + "LEFT JOIN ward AS w ON re.wards_id = w.id "
                                        + "LEFT JOIN street AS s ON re.street_id = s.id "
                                        + "LEFT JOIN customers AS c ON re.customer_id = c.id "
                                        + "WHERE re.create_by = :employeeId "
                                        + "AND IF(:acreage = '' OR :acreage IS NULL,re.acreage >=0 OR re.acreage IS NULL,re.acreage >= :acreage) "
                                        + "AND IF(:priceMin = '' OR :priceMin IS NULL,(re.price >= 0 OR re.price IS NULL) ,re.price >= :priceMin) "
                                        + "AND IF(:priceMax = '' OR :priceMax IS NULL,(re.price <= (SELECT MAX(re.price) FROM realestate)) ,re.price <= :priceMax) "
                                        + "AND IF(:customerId = '' OR :customerId IS NULL,re.customer_id >=0 OR re.customer_id IS NULL,re.customer_id = :customerId) "
                                        + "AND IF(:direction = '' OR :direction IS NULL,re.direction >=0 OR re.direction IS NULL,re.direction = :direction) "
                                        + "AND IF(:request = '' OR :request IS NULL,re.request >=0 OR re.request IS NULL,re.request = :request) "
                                        + "AND IF(:type = '' OR :type IS NULL,re.type >=0 OR re.type IS NULL,re.type = :type) "
                                        + "AND (re.date_create >= IF(:startDate = '' OR :startDate IS NULL, 0, :startDate) AND re.date_create <= IF(:endDate = '' OR :endDate IS NULL, NOW(), :endDate)) "
                                        + "AND IF(:censored = -1, re.censor >= 0 OR re.censor IS NULL, re.censor = :censored) "
                                        + "AND (p._name LIKE %:location% OR d._name LIKE %:location% OR w._name LIKE %:location% OR s._name LIKE %:location%)")
        Page<RealEstate> findEstateAdvancedSearchByEmployee(@Param("employeeId") Integer employeeId,
                        @Param("location") String location,
                        @Param("request") Integer request,
                        @Param("type") Integer type,
                        @Param("direction") Integer direction,
                        @Param("priceMin") Long priceMin,
                        @Param("priceMax") Long priceMax,
                        @Param("acreage") BigDecimal acreage,
                        @Param("customerId") Integer customerId,
                        @Param("startDate") Date startDate,
                        @Param("endDate") Date endDate,
                        @Param("censored") Integer censored,
                        Pageable pageable);

        @Query(nativeQuery = true, value = "SELECT * FROM realestate AS re "
                        + "LEFT JOIN province AS p ON re.province_id = p.id "
                        + "LEFT JOIN district AS d ON re.district_id = d.id "
                        + "LEFT JOIN ward AS w ON re.wards_id = w.id "
                        + "LEFT JOIN street AS s ON re.street_id = s.id "
                        + "LEFT JOIN customers AS c ON re.customer_id = c.id "
                        + "LEFT JOIN employees AS e ON re.create_by = e.EmployeeID "
                        + "WHERE 1 AND (re.create_by = :employeeId OR e.ReportsTo = :employeeId) "
                        + "AND IF(:acreage = '' OR :acreage IS NULL,re.acreage >= 0 OR re.acreage IS NULL,re.acreage >= :acreage) "
                        + "AND IF(:priceMin = '' OR :priceMin IS NULL,(re.price >= 0 OR re.price IS NULL) ,re.price >= :priceMin) "
                        + "AND IF(:priceMax = '' OR :priceMax IS NULL,(re.price <= (SELECT MAX(re.price) FROM realestate)) ,re.price <= :priceMax) "
                        + "AND IF(:customerId = '' OR :customerId IS NULL,re.customer_id >= 0 OR re.customer_id IS NULL,re.customer_id = :customerId) "
                        + "AND IF(:direction = '' OR :direction IS NULL,re.direction >=0 OR re.direction IS NULL,re.direction = :direction) "
                        + "AND IF(:request = '' OR :request IS NULL,re.request >=0 OR re.request IS NULL,re.request = :request) "
                        + "AND IF(:type = '' OR :type IS NULL,re.type >=0 OR re.type IS NULL,re.type = :type) "
                        + "AND (re.date_create >= IF(:startDate = '' OR :startDate IS NULL, 0, :startDate) AND re.date_create <= IF(:endDate = '' OR :endDate IS NULL, NOW(), :endDate)) "
                        + "AND IF(:censored = -1, re.censor >= 0 OR re.censor IS NULL, re.censor = :censored) "
                        + "AND (p._name LIKE %:location% OR d._name LIKE %:location% OR w._name LIKE %:location% OR s._name LIKE %:location%)", countQuery = "SELECT count(re.id) FROM realestate AS re "
                                        + "LEFT JOIN province AS p ON re.province_id = p.id "
                                        + "LEFT JOIN district AS d ON re.district_id = d.id "
                                        + "LEFT JOIN ward AS w ON re.wards_id = w.id "
                                        + "LEFT JOIN street AS s ON re.street_id = s.id "
                                        + "LEFT JOIN customers AS c ON re.customer_id = c.id "
                                        + "JOIN employees AS e ON re.create_by = e.EmployeeID "
                                        + "WHERE re.create_by = :employeeId OR e.ReportsTo = :employeeId "
                                        + "AND IF(:acreage = '' OR :acreage IS NULL,re.acreage >= 0 OR re.acreage IS NULL,re.acreage >= :acreage) "
                                        + "AND IF(:priceMin = '' OR :priceMin IS NULL,(re.price >= 0 OR re.price IS NULL) ,re.price >= :priceMin) "
                                        + "AND IF(:priceMax = '' OR :priceMax IS NULL,(re.price <= (SELECT MAX(re.price) FROM realestate)) ,re.price <= :priceMax) "
                                        + "AND IF(:customerId = '' OR :customerId IS NULL,re.customer_id >= 0 OR re.customer_id IS NULL,re.customer_id = :customerId) "
                                        + "AND IF(:direction = '' OR :direction IS NULL,re.direction >= 0 OR re.direction IS NULL,re.direction = :direction) "
                                        + "AND IF(:request = '' OR :request IS NULL,re.request >= 0 OR re.request IS NULL,re.request = :request) "
                                        + "AND IF(:type = '' OR :type IS NULL,re.type >= 0 OR re.type IS NULL,re.type = :type) "
                                        + "AND (re.date_create >= IF(:startDate = '' OR :startDate IS NULL, 0, :startDate) AND re.date_create <= IF(:endDate = '' OR :endDate IS NULL, NOW(), :endDate)) "
                                        + "AND IF(:censored = -1, re.censor >= 0 OR re.censor IS NULL, re.censor = :censored) "
                                        + "AND (p._name LIKE %:location% OR d._name LIKE %:location% OR w._name LIKE %:location% OR s._name LIKE %:location%)")
        Page<RealEstate> findEstateAdvancedSearchBySeller(@Param("employeeId") Integer employeeId,
                        @Param("location") String location,
                        @Param("request") Integer request,
                        @Param("type") Integer type,
                        @Param("direction") Integer direction,
                        @Param("priceMin") Long priceMin,
                        @Param("priceMax") Long priceMax,
                        @Param("acreage") BigDecimal acreage,
                        @Param("customerId") Integer customerId,
                        @Param("startDate") Date startDate,
                        @Param("endDate") Date endDate,
                        @Param("censored") Integer censored,
                        Pageable pageable);

        @Query(nativeQuery = true, value = "SELECT MAX(price) FROM realestate AS re WHERE re.censor = 1")
        Long maxPriceEsatble();
}
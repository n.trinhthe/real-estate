package com.devcamp.realestate.service;
import com.devcamp.realestate.entity.Employee;
import com.devcamp.realestate.security.UserPrincipal;

import org.springframework.stereotype.Service;

@Service
public interface EmployeeService {
    Employee createEmployee(Employee user);

    UserPrincipal findByUsername(String username);
}

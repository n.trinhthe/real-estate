package com.devcamp.realestate.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.engine.jdbc.StreamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;

import com.devcamp.realestate.service.ImageService;

@RestController
@CrossOrigin

public class ImageController {
    @Autowired
    ImageService imageService;

    private final ResourceLoader resourceLoader;

    public ImageController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    // Upload File To Local
    @PostMapping("/image/local")
    public ResponseEntity<?> uploadImageToLocal(@RequestParam("image") MultipartFile file) throws IOException {
        try {
            imageService.uploadImageToLocal(file);
            return ResponseEntity.status(HttpStatus.OK).body("Upload Image Successfully");
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Upload Fail");
        }
    }

    // Upload File To Database
    @PostMapping("/image/db")
    public ResponseEntity<?> uplaodImageToDB(@RequestParam("image") MultipartFile file) throws IOException {
        try {
            String uploadImage = imageService.uploadImageToDB(file);
            return ResponseEntity.status(HttpStatus.OK).body("Upload image successfully: " + uploadImage);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Upload Fail");
        }
    }

    //Lấy hình ảnh từ url
    @GetMapping("/images/{imageName}")
    @ResponseBody
    public void getImage(HttpServletResponse response, @PathVariable String imageName) throws IOException {
        Resource imageResource = resourceLoader.getResource("classpath:/static/images/" + imageName); //Thư mục gốc chưa file ảnh
        response.setContentType("image/jpeg");
        response.setContentLengthLong(imageResource.contentLength());
        StreamUtils.copy(imageResource.getInputStream(), response.getOutputStream());
    }

    // //Download image 
    // @GetMapping("/images/")
    // @ResponseBody
    // public void getImage(HttpServletResponse response, @PathVariable String imageName) throws IOException {
    //     Resource imageResource = resourceLoader.getResource("classpath:/static/images/" + imageName); //Thư mục gốc chưa file ảnh
    //     response.setContentType("image/jpeg");
    //     response.setContentLengthLong(imageResource.contentLength());
    //     StreamUtils.copy(imageResource.getInputStream(), response.getOutputStream());
    // }

}

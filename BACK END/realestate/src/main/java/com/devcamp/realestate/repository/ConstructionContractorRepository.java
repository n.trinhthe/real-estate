package com.devcamp.realestate.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.model.ConstructionContractor;

public interface ConstructionContractorRepository extends JpaRepository<ConstructionContractor, Integer>{
    Optional<ConstructionContractor> findById(Integer id);
}

package com.devcamp.realestate.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.model.Utility;

public interface UtilityRepository extends JpaRepository<Utility, Integer>{
    Optional<Utility> findById(Integer id);
}

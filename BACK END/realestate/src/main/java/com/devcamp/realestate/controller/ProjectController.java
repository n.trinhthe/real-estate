package com.devcamp.realestate.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.model.Project;
import com.devcamp.realestate.service.ProjectService;

@RestController
@CrossOrigin
public class ProjectController {
    @Autowired
    ProjectService projectService;

    @GetMapping("/projects")
    public ResponseEntity<List<Project>> getAllProjects() {
        try {
            return new ResponseEntity<>(projectService.getAllProjects(), HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getCause().getCause().getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/projects/{projectId}")
    public ResponseEntity<Project> getAllProjects(@PathVariable("projectId") Integer projectId) {
        Project findProject = projectService.getProjectById(projectId);
        if (findProject != null) {
            try {
                return new ResponseEntity<>(findProject, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/projects")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Object> createProject(@Valid @RequestBody Project pProject) {
        try {
            return new ResponseEntity<>(projectService.createProject(pProject), HttpStatus.CREATED);
        } catch (Exception ex) {
            return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/projects/{projectId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Object> updateProject(@Valid @RequestBody Project pProject,
            @PathVariable("projectId") Integer projectId) {
        Project updateProject = projectService.updateProjectById(pProject, projectId);
        if (updateProject != null) {
            try {
                return new ResponseEntity<>(updateProject, HttpStatus.OK);
            } catch (Exception ex) {
                return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/projects/{projectId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Project> deleteProjectById(@PathVariable("projectId") Integer projectId) {
        try {
            projectService.deleteProject(projectId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/{districtId}/projects")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Set<Project>> getProjectsOfDistrict(@PathVariable("districtId") Integer districtId) {
        Set<Project> projectList = projectService.getProjectsOfDistrict(districtId);
        if (projectList != null) {
            try {
                return new ResponseEntity<>(projectList, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/projects",params = "provinceId")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<List<Project>> getEstateByProvince(
            @RequestParam(value = "provinceId") Integer provinceId) {
        try {
            return new ResponseEntity<>(projectService.findProjectByProvince(provinceId), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/projects",params = "districtId")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<List<Project>> getEstateByDistrict(
            @RequestParam(value = "districtId") Integer districtId) {
        try {
            return new ResponseEntity<>(projectService.findProjectByDistrict(districtId), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/projects",params = "wardId")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<List<Project>> getEstateByWard(
            @RequestParam(value = "wardId") Integer wardId) {
        try {
            return new ResponseEntity<>(projectService.findProjectByWard(wardId), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

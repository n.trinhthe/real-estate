package com.devcamp.realestate.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.model.Investor;
import com.devcamp.realestate.service.InvestorService;

@RestController
@CrossOrigin
public class InvestorController {
    @Autowired
    InvestorService investorService;

    @GetMapping("/investors")
    public ResponseEntity<List<Investor>> getAllInvestors() {
        try {
            return new ResponseEntity<>(investorService.getAllInvestors(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/investors/{investorId}")
    public ResponseEntity<Investor> getAllInvestors(@PathVariable("investorId") Integer investorId) {
        Investor findInvestor = investorService.getInvestorById(investorId);
        if (findInvestor != null) {
            try {
                return new ResponseEntity<>(findInvestor, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/investors")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Object> createInvestor(@Valid @RequestBody Investor pInvestor) {
        try {
            return new ResponseEntity<>(investorService.createInvestor(pInvestor), HttpStatus.CREATED);
        } catch (Exception ex) {
            return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/investors/{investorId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Object> updateInvestor(@Valid @RequestBody Investor pInvestor,
            @PathVariable("investorId") Integer investorId) {
        Investor updateInvestor = investorService.updateInvestorById(pInvestor, investorId);
        if (updateInvestor != null) {
            try {
                return new ResponseEntity<>(updateInvestor, HttpStatus.OK);
            } catch (Exception ex) {
                return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/investors/{investorId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Investor> deleteInvestorById(@PathVariable("investorId") Integer investorId) {
        try {
            investorService.deleteInvestor(investorId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

package com.devcamp.realestate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.model.RegionLink;
import com.devcamp.realestate.repository.RegionLinkRepository;

@Service
public class RegionLinkService {
    @Autowired
    RegionLinkRepository regionlinkRepository;

    public List<RegionLink> getAllRegionLink() {
        ArrayList<RegionLink> regionlinkList = new ArrayList<>();
        regionlinkRepository.findAll().forEach(regionlinkList::add);
        return regionlinkList;
    }

    public RegionLink getRegionLinkById(Integer id) {
        Optional<RegionLink> regionlinkData = regionlinkRepository.findById(id);
        if (regionlinkData.isPresent()) {
            return regionlinkData.get();
        } else {
            return null;
        }
    }

    public RegionLink createRegionLink(RegionLink pRegionLink) {
        RegionLink newRegionLink = new RegionLink();
        newRegionLink.setName(pRegionLink.getName());
        newRegionLink.setDescription(pRegionLink.getDescription());
        newRegionLink.setPhoto(pRegionLink.getPhoto());
        newRegionLink.setAddress(pRegionLink.getAddress());
        newRegionLink.setLat(pRegionLink.getLat());
        newRegionLink.setLng(pRegionLink.getLng());
        RegionLink createdRegionLink = regionlinkRepository.save(newRegionLink);
        return createdRegionLink;
    }

    public RegionLink updateRegionLink(RegionLink pRegionLink, Integer id) {
        Optional<RegionLink> regionlinkData = regionlinkRepository.findById(id);
        if (regionlinkData.isPresent()) {
            RegionLink foundRegionLink = regionlinkData.get();
            foundRegionLink.setName(pRegionLink.getName());
            foundRegionLink.setDescription(pRegionLink.getDescription());
            foundRegionLink.setPhoto(pRegionLink.getPhoto());
            foundRegionLink.setAddress(pRegionLink.getAddress());
            foundRegionLink.setLat(pRegionLink.getLat());
            foundRegionLink.setLng(pRegionLink.getLng());
            RegionLink updatedRegionLink = regionlinkRepository.save(foundRegionLink);
            return updatedRegionLink;
        } else {
            return null;
        }
    }

    public void deleteRegionLink(Integer id) {
        regionlinkRepository.deleteById(id);
    }

    public void deleteAllRegionLink() {
        regionlinkRepository.deleteAll();
        ;
    }
}

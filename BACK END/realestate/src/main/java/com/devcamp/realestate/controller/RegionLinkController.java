package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.model.RegionLink;
import com.devcamp.realestate.service.RegionLinkService;

@RestController
@CrossOrigin
public class RegionLinkController {
    @Autowired
    RegionLinkService regionLinkService;

    @GetMapping("/regionlinks")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<List<RegionLink>> getAllRegionLink() {
        try {
            return new ResponseEntity<>(regionLinkService.getAllRegionLink(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/regionlinks/{id}")
    public ResponseEntity<RegionLink> getRegionLinkById(@PathVariable("id") Integer id) {
        RegionLink findRegionLink = regionLinkService.getRegionLinkById(id);
        if (findRegionLink != null) {
            try {
                return new ResponseEntity<>(findRegionLink, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/regionlinks")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<RegionLink> createRegionLink(@RequestBody RegionLink pRegionLink) {
        RegionLink newRegionLink = regionLinkService.createRegionLink(pRegionLink);
        try {
            return new ResponseEntity<>(newRegionLink, HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/regionlinks/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Object> updateRegionLink(@RequestBody RegionLink pRegionLink, @PathVariable("id") Integer id) {
        RegionLink updateRegionLink = regionLinkService.updateRegionLink(pRegionLink, id);
        if (updateRegionLink != null) {
            try {
                return new ResponseEntity<>(updateRegionLink, HttpStatus.OK);
            } catch (Exception ex) {
                return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/regionlinks/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Object> deleteRegionLink(@PathVariable("id") Integer id) {
        try {
            regionLinkService.deleteRegionLink(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/regionlinks/all")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Object> deleteAllRegionLink() {
        try {
            regionLinkService.deleteAllRegionLink();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}

package com.devcamp.realestate.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.realestate.model.Province;
import com.devcamp.realestate.model.custom.IProvinceCountEstate;

public interface ProvinceRepository extends JpaRepository<Province,Integer> {
    Optional<Province> findById(Integer id);
    Province getByCode(String code);

    @Query(value = "SELECT p.id AS id, p._name AS name, p._code AS code , COUNT(re.province_id) AS countEstate " 
    + "FROM province AS p "
    + "LEFT JOIN realestate AS re ON re.province_id = p.id "
    + "GROUP BY p._name "
    + "ORDER BY p.id "
    ,nativeQuery = true)
    List<IProvinceCountEstate> findAllProvinceCount();
}

package com.devcamp.realestate.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.model.DesignUnit;
import com.devcamp.realestate.service.DesignUnitService;

@RestController
@CrossOrigin
public class DesignUnitController {
    @Autowired
    DesignUnitService designunitService;

    @GetMapping("/designunits")
    public ResponseEntity<List<DesignUnit>> getAllDesignUnits() {
        try {
            return new ResponseEntity<>(designunitService.getAllDesignUnits(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/designunits/{designunitId}")
    public ResponseEntity<DesignUnit> getAllDesignUnits(@PathVariable("designunitId") Integer designunitId) {
        DesignUnit findDesignUnit = designunitService.getDesignUnitById(designunitId);
        if (findDesignUnit != null) {
            try {
                return new ResponseEntity<>(findDesignUnit, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/designunits")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Object> createDesignUnit(@Valid @RequestBody DesignUnit pDesignUnit) {
        try {
            return new ResponseEntity<>(designunitService.createDesignUnit(pDesignUnit), HttpStatus.CREATED);
        } catch (Exception ex) {
            return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/designunits/{designunitId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Object> updateDesignUnit(@Valid @RequestBody DesignUnit pDesignUnit,
            @PathVariable("designunitId") Integer designunitId) {
        DesignUnit updateDesignUnit = designunitService.updateDesignUnitById(pDesignUnit, designunitId);
        if (updateDesignUnit != null) {
            try {
                return new ResponseEntity<>(updateDesignUnit, HttpStatus.OK);
            } catch (Exception ex) {
                
                return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/designunits/{designunitId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<DesignUnit> deleteDesignUnitById(@PathVariable("designunitId") Integer designunitId) {
        try {
            designunitService.deleteDesignUnit(designunitId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/designunits/all")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<DesignUnit> deleteAllDesignUnit() {
        try {
            designunitService.deleteAllDesignUnit();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

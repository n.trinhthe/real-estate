package com.devcamp.realestate.repository;

import java.util.Optional;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.devcamp.realestate.model.AddressMap;

@EnableJpaRepositories
@ComponentScan(basePackages = { "com.devcamp.realestate.*" })
@EntityScan("com.devcamp.realestate.model.*")
public interface AddressMapRepository extends JpaRepository<AddressMap, Integer> {
    Optional<AddressMap> findById(Integer id);
}

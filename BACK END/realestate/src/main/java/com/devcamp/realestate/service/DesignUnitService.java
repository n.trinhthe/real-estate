package com.devcamp.realestate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.model.DesignUnit;
import com.devcamp.realestate.repository.DesignUnitRepository;

@Service
public class DesignUnitService {
    @Autowired
    DesignUnitRepository designunitRepository;

    public List<DesignUnit> getAllDesignUnits() {
        ArrayList<DesignUnit> designunitList = new ArrayList<>();
        designunitRepository.findAll().forEach(designunitList::add);
        return designunitList;
    }

    public DesignUnit getDesignUnitById(Integer id) {
        Optional<DesignUnit> designunitData = designunitRepository.findById(id);
        if (designunitData.isPresent()) {
            return designunitData.get();
        } else {
            return null;
        }
    }

    public DesignUnit createDesignUnit(DesignUnit pDesignUnit) {
        DesignUnit newDesignUnit = new DesignUnit();
        newDesignUnit.setName(pDesignUnit.getName());
        newDesignUnit.setDescription(pDesignUnit.getDescription());
        newDesignUnit.setAddress(pDesignUnit.getAddress());
        newDesignUnit.setProjects(pDesignUnit.getProjects());
        newDesignUnit.setPhone(pDesignUnit.getPhone());
        newDesignUnit.setPhone2(pDesignUnit.getPhone2());
        newDesignUnit.setFax(pDesignUnit.getFax());
        newDesignUnit.setEmail(pDesignUnit.getEmail());
        newDesignUnit.setWebsite(pDesignUnit.getWebsite());
        newDesignUnit.setNote(pDesignUnit.getNote());
        DesignUnit createdDesignUnit =  designunitRepository.save(newDesignUnit);
        return createdDesignUnit;
    }

    public DesignUnit updateDesignUnitById(DesignUnit pDesignUnit, Integer id) {
        Optional<DesignUnit> designunitData = designunitRepository.findById(id);
        if (designunitData.isPresent()) {
            DesignUnit foundDesignUnit = designunitData.get();
            foundDesignUnit.setName(pDesignUnit.getName());
            foundDesignUnit.setDescription(pDesignUnit.getDescription());
            foundDesignUnit.setProjects(pDesignUnit.getProjects());
            foundDesignUnit.setAddress(pDesignUnit.getAddress());
            foundDesignUnit.setPhone(pDesignUnit.getPhone());
            foundDesignUnit.setPhone2(pDesignUnit.getPhone2());
            foundDesignUnit.setFax(pDesignUnit.getFax());
            foundDesignUnit.setEmail(pDesignUnit.getEmail());
            foundDesignUnit.setWebsite(pDesignUnit.getWebsite());
            foundDesignUnit.setNote(pDesignUnit.getNote());
            DesignUnit updatedDesignUnit =  designunitRepository.save(foundDesignUnit);
            return updatedDesignUnit;
        } else {
            return null;
        }
    }

    public void deleteDesignUnit(Integer id) {
        designunitRepository.deleteById(id);
    }

    public void deleteAllDesignUnit() {
        designunitRepository.deleteAll();
    }
}

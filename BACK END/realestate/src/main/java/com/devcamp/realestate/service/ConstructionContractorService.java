package com.devcamp.realestate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.model.ConstructionContractor;
import com.devcamp.realestate.repository.ConstructionContractorRepository;

@Service
public class ConstructionContractorService {
    @Autowired
    ConstructionContractorRepository constructioncontractorRepository;
    
    public List<ConstructionContractor> getAllConstructionContractors() {
        ArrayList<ConstructionContractor> constructioncontractorList = new ArrayList<>();
        constructioncontractorRepository.findAll().forEach(constructioncontractorList::add);
        return constructioncontractorList;
    }

    public ConstructionContractor getConstructionContractorById(Integer id) {
        Optional<ConstructionContractor> constructioncontractorData = constructioncontractorRepository.findById(id);
        if (constructioncontractorData.isPresent()) {
            return constructioncontractorData.get();
        } else {
            return null;
        }
    }

    public ConstructionContractor createConstructionContractor(ConstructionContractor pConstructionContractor) {
        ConstructionContractor newConstructionContractor = new ConstructionContractor();
        newConstructionContractor.setName(pConstructionContractor.getName());
        newConstructionContractor.setDescription(pConstructionContractor.getDescription());
        newConstructionContractor.setAddress(pConstructionContractor.getAddress());
        newConstructionContractor.setPhone(pConstructionContractor.getPhone());
        newConstructionContractor.setPhone2(pConstructionContractor.getPhone2());
        newConstructionContractor.setFax(pConstructionContractor.getFax());
        newConstructionContractor.setEmail(pConstructionContractor.getEmail());
        newConstructionContractor.setWebsite(pConstructionContractor.getWebsite());
        newConstructionContractor.setNote(pConstructionContractor.getNote());
        ConstructionContractor createdConstructionContractor =  constructioncontractorRepository.save(newConstructionContractor);
        return createdConstructionContractor;
    }

    public ConstructionContractor updateConstructionContractorById(ConstructionContractor pConstructionContractor, Integer id) {
        Optional<ConstructionContractor> constructioncontractorData = constructioncontractorRepository.findById(id);
        if (constructioncontractorData.isPresent()) {
            ConstructionContractor foundConstructionContractor = constructioncontractorData.get();
            foundConstructionContractor.setName(pConstructionContractor.getName());
            foundConstructionContractor.setDescription(pConstructionContractor.getDescription());
            foundConstructionContractor.setAddress(pConstructionContractor.getAddress());
            foundConstructionContractor.setPhone(pConstructionContractor.getPhone());
            foundConstructionContractor.setPhone2(pConstructionContractor.getPhone2());
            foundConstructionContractor.setFax(pConstructionContractor.getFax());
            foundConstructionContractor.setEmail(pConstructionContractor.getEmail());
            foundConstructionContractor.setWebsite(pConstructionContractor.getWebsite());
            foundConstructionContractor.setNote(pConstructionContractor.getNote());
            ConstructionContractor updatedConstructionContractor =  constructioncontractorRepository.save(foundConstructionContractor);
            return updatedConstructionContractor;
        } else {
            return null;
        }
    }

    public void deleteConstructionContractor(Integer id) {
        constructioncontractorRepository.deleteById(id);
    }

    public void deleteAllConstructionContractor() {
        constructioncontractorRepository.deleteAll();;
    }
}


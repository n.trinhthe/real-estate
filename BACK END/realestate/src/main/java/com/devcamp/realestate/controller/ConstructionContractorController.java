package com.devcamp.realestate.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.model.ConstructionContractor;
import com.devcamp.realestate.service.ConstructionContractorService;

@RestController
@CrossOrigin
public class ConstructionContractorController {
    @Autowired
    ConstructionContractorService constructioncontractorService;

    @GetMapping("/contractors")
    public ResponseEntity<List<ConstructionContractor>> getAllConstructionContractors() {
        try {
            return new ResponseEntity<>(constructioncontractorService.getAllConstructionContractors(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/contractors/{contractorId}")
    public ResponseEntity<ConstructionContractor> getAllConstructionContractors(
            @PathVariable("contractorId") Integer constructioncontractorId) {
        ConstructionContractor findConstructionContractor = constructioncontractorService
                .getConstructionContractorById(constructioncontractorId);
        if (findConstructionContractor != null) {
            try {
                return new ResponseEntity<>(findConstructionContractor, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/contractors")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Object> createConstructionContractor(
            @Valid @RequestBody ConstructionContractor pConstructionContractor) {
        try {
            return new ResponseEntity<>(
                    constructioncontractorService.createConstructionContractor(pConstructionContractor),
                    HttpStatus.CREATED);
        } catch (Exception ex) {
            return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/contractors/{contractorId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Object> updateConstructionContractor(
            @Valid @RequestBody ConstructionContractor pConstructionContractor,
            @PathVariable("contractorId") Integer constructioncontractorId) {
        ConstructionContractor updateConstructionContractor = constructioncontractorService
                .updateConstructionContractorById(pConstructionContractor, constructioncontractorId);
        if (updateConstructionContractor != null) {
            try {
                return new ResponseEntity<>(updateConstructionContractor, HttpStatus.OK);
            } catch (Exception ex) {
                return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/contractors/{contractorId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<ConstructionContractor> deleteConstructionContractorById(
            @PathVariable("contractorId") Integer constructioncontractorId) {
        try {
            constructioncontractorService.deleteConstructionContractor(constructioncontractorId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/contractors/all")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<ConstructionContractor> deleteAllConstructionContractor() {
        try {
            constructioncontractorService.deleteAllConstructionContractor();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

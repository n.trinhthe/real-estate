package com.devcamp.realestate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.model.District;
import com.devcamp.realestate.model.Province;
import com.devcamp.realestate.model.custom.IDistrictCountEstate;
import com.devcamp.realestate.repository.DistrictRepository;
import com.devcamp.realestate.repository.ProvinceRepository;

@Service
public class DistrictService {
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    ProvinceRepository provinceRepository;

    public List<District> getAllDistrict(){
        ArrayList<District> districtList = new ArrayList<>();
        districtRepository.findAll().forEach(districtList::add);
        return districtList;
    }

    public List<IDistrictCountEstate> getDistrictCountEstate(){
        return districtRepository.findDistrictCountEstate();
    }
    
    public District getDistrictById(Integer id){
        Optional<District> districtData =  districtRepository.findById(id);
        if (districtData.isPresent()){
            return districtData.get();
        }
        else {
            return null;
        }
    }

    public District createDistrict(District pDistrict){
        District newDistrict = new District();
        newDistrict.setName(pDistrict.getName());
        newDistrict.setPrefix(pDistrict.getPrefix());
        newDistrict.setProvince(pDistrict.getProvince());
        districtRepository.save(newDistrict);
        return newDistrict;
    }

    public District updatedDistrict (District pDistrict, Integer id){
        Optional<District> districtData =  districtRepository.findById(id);
        if (districtData.isPresent()){
            District updatedDistrict = districtData.get();
            updatedDistrict.setName(pDistrict.getName());
            updatedDistrict.setPrefix(pDistrict.getPrefix());
            districtRepository.save(updatedDistrict);
            return updatedDistrict;
        }
        else {
            return null;
        }
    }

    public void delelteDistrict(Integer id){
        districtRepository.deleteById(id);
    }

    public Set<District> getDistrictOfProvinceId(Integer provinceId){
        Optional<Province> provinceData = provinceRepository.findById(provinceId);
        if (provinceData!=null){
            return provinceData.get().getDistricts();
        }
        else {
            return null;
        }
    }
}


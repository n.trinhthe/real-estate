package com.devcamp.realestate.model.custom;

public interface IWardCountEstate {
    Integer getId();
    String getName();
    String getPrefix();
    Integer getCountEstate();
    // Integer getCountProject();
}

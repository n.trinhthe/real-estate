package com.devcamp.realestate.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.realestate.model.Image;
import com.devcamp.realestate.repository.ImageRepository;
import com.devcamp.realestate.util.ImageUtility;

@Service
public class ImageService {
    @Autowired
    ImageRepository imageRepository;

    // Upload image thư mục của Backend
    private String uploadFolderPath = "../realestate/src/main/resources/static/images/";

    public void uploadImageToLocal(MultipartFile file) throws IOException {
        byte[] dataImage = file.getBytes();
        Path path = Paths.get(uploadFolderPath + file.getOriginalFilename());
        Files.write(path, dataImage);
    }

    // Upload Image vào database
    public String uploadImageToDB(MultipartFile file) throws IOException {
        // Get the original file name
        String originalFileName = file.getOriginalFilename();

        // Kiểm tra xem tên ảnh có trùng với tên ảnh trong DB ko?
        String filePath = originalFileName;
        int counter = 1;

        while (imageRepository.existsImageByName(filePath)) { // Nếu trùng sẽ thêm số vào sau tên của ảnh
            String newFileName = String.format("%s(%d).%s",
                    FilenameUtils.getBaseName(originalFileName), // Lấy tên file của ảnh trước Extension FilenameUtils
                                                                 // import từ IO từ thư viện maven
                    counter++,
                    FilenameUtils.getExtension(originalFileName)); // Lấy đuôi file
            filePath = newFileName;
        }

        // Save Image vào database
        Image imageData = imageRepository.save(Image.builder()
                .name(filePath)
                .type(file.getContentType())
                .image(ImageUtility.compressImage(file.getBytes())).build());
        if (imageData != null) {
            byte[] dataImage = file.getBytes();
            Path path = Paths.get(uploadFolderPath + filePath);
            Files.write(path, dataImage);
            return filePath;
        } else {
            return null;
        }
    }

    // //Download image
    // public byte[] downloadImageFromDB(String fileName){
    // Optional<Image> imageData = imageRepository.findByName(fileName);
    // String fileImage = imageData.get().getName();
    // byte[] image = Files.readAllBytes(new File(fileImage).toPath());
    // return image;
    // }

    // Delete Image
    public void deleteImageToLocal(String fileName) throws IOException {
        Path path = Paths.get(uploadFolderPath + fileName);
        Files.deleteIfExists(path);
    }
}

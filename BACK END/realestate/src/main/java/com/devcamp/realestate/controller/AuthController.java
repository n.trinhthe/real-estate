package com.devcamp.realestate.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Token;
import com.devcamp.realestate.entity.Employee;
import com.devcamp.realestate.security.JwtUtil;
import com.devcamp.realestate.security.UserPrincipal;
import com.devcamp.realestate.service.TokenService;
import com.devcamp.realestate.service.EmployeeService;

@RestController
@CrossOrigin
public class AuthController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private TokenService verificationTokenService;

    @PostMapping("/register")
    public Employee register(@RequestBody Employee employee) {
        employee.setPassword(new BCryptPasswordEncoder().encode(employee.getPassword()));
        return employeeService.createEmployee(employee);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody Employee employee) {
        UserPrincipal userPrincipal = employeeService.findByUsername(employee.getUsername());
        if (employee == null || !new BCryptPasswordEncoder().matches(employee.getPassword(), userPrincipal.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username Or Password Wrong");
        }
        Token token = new Token();
        token.setToken(jwtUtil.generateToken(userPrincipal));
        token.setTokenExpDate(jwtUtil.generateExpirationDate());
        token.setCreatedBy(userPrincipal.getUserId());
        tokenService.createToken(token);
        return new ResponseEntity<>(token.getToken(), HttpStatus.OK);
        // return ResponseEntity.ok();
    }
    
    @GetMapping("/users/info")

    public ResponseEntity<UserPrincipal> getUserDetail(@RequestHeader(name = "Authorization") String authUser) {
        UserPrincipal user = null;
        Token token = null;
        // System.out.println("Authorization:" + authUser);
        if (authUser.startsWith("Token ")) {
            String jwt = authUser.substring(6);
            user = jwtUtil.getUserFromToken(jwt);
            token = verificationTokenService.findByToken(jwt);
            
        }
        if (null != user && null != token && token.getTokenExpDate().after(new Date())
        ) {
            return new ResponseEntity<UserPrincipal>(user, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<UserPrincipal>(user,HttpStatus.BAD_REQUEST);
        }
    }
}

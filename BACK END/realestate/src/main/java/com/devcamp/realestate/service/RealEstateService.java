package com.devcamp.realestate.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.realestate.model.RealEstate;
import com.devcamp.realestate.repository.CustomerRepository;
import com.devcamp.realestate.repository.DistrictRepository;
import com.devcamp.realestate.repository.ImageRepository;
import com.devcamp.realestate.repository.ProjectRepository;
import com.devcamp.realestate.repository.ProvinceRepository;
import com.devcamp.realestate.repository.RealEstateRepository;
import com.devcamp.realestate.repository.StreetRepository;
import com.devcamp.realestate.repository.WardRepository;

@Service
public class RealEstateService {
    @Autowired
    RealEstateRepository realEstateRepository;
    @Autowired
    ProvinceRepository provinceRepository;
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    WardRepository wardRepository;
    @Autowired
    StreetRepository streetRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    ImageService imageService;
    @Autowired
    ImageRepository imageRepository;

    public Page<RealEstate> getAllRealEstatePageable(Integer page, Integer size) {
        Page<RealEstate> listEstate = realEstateRepository.findAll(
                PageRequest.of(page, size, Sort.by("date_create").descending()));
        return listEstate;
    }

    public Page<RealEstate> getAllRealEstate(Integer page, Integer size) {
        return realEstateRepository.findAllRealEstate(
                PageRequest.of(page, size, Sort.by("date_create").descending()));
    }

    public List<RealEstate> getRealEstateByCustomerId(Integer customerId) {
        return realEstateRepository.findByCustomerId(customerId);
    }

    public RealEstate findRealEstateById(Integer id) {
        Optional<RealEstate> realEstateData = realEstateRepository.findById(id);
        if (realEstateData.isPresent()) {
            return realEstateData.get();
        } else {
            return null;
        }
    }

    public RealEstate createEstate(RealEstate pEstate, MultipartFile file) throws IOException {
        RealEstate newRealEstate = new RealEstate();
        newRealEstate.setTitle(pEstate.getTitle());
        newRealEstate.setType(pEstate.getType());
        newRealEstate.setRequest(pEstate.getRequest());
        newRealEstate.setProvinceId(pEstate.getProvinceId());
        newRealEstate.setDistrictId(pEstate.getDistrictId());
        newRealEstate.setWardId(pEstate.getWardId());
        newRealEstate.setStreetId(pEstate.getStreetId());
        newRealEstate.setProjectId(pEstate.getProjectId());
        newRealEstate.setCustomerId(pEstate.getCustomerId());
        newRealEstate.setAddress(pEstate.getAddress());
        newRealEstate.setPrice(pEstate.getPrice());
        newRealEstate.setPriceMin(pEstate.getPriceMin());
        newRealEstate.setPriceTime(pEstate.getPriceTime());
        newRealEstate.setAcreage(pEstate.getAcreage());
        newRealEstate.setDirection(pEstate.getDirection());
        newRealEstate.setTotalFloors(pEstate.getTotalFloors());
        newRealEstate.setNumberFloors(pEstate.getNumberFloors());
        newRealEstate.setBath(pEstate.getBath());
        newRealEstate.setApartCode(pEstate.getApartCode());
        newRealEstate.setWallArea(pEstate.getWallArea());
        newRealEstate.setBedroom(pEstate.getBedroom());
        newRealEstate.setBalcony(pEstate.getBalcony());
        newRealEstate.setLandscapeView(pEstate.getLandscapeView());
        newRealEstate.setApartType(pEstate.getApartType());
        newRealEstate.setApartLoca(pEstate.getApartLoca());
        newRealEstate.setFurnitureType(pEstate.getFurnitureType());
        newRealEstate.setPriceRent(pEstate.getPriceRent());
        newRealEstate.setReturnRate(pEstate.getReturnRate());
        newRealEstate.setLegalDoc(pEstate.getLegalDoc());
        newRealEstate.setDescription(pEstate.getDescription());
        newRealEstate.setWidthY(pEstate.getWidthY());
        newRealEstate.setLongX(pEstate.getLongX());
        newRealEstate.setStreetHouse(pEstate.getStreetHouse());
        newRealEstate.setFSBO(pEstate.getFSBO());
        newRealEstate.setViewNum(pEstate.getViewNum());
        newRealEstate.setShape(pEstate.getShape());
        newRealEstate.setDistance2facade(pEstate.getDistance2facade());
        newRealEstate.setAdjacentFacadeNum(pEstate.getAdjacentFacadeNum());
        newRealEstate.setAdjacentRoad(pEstate.getAdjacentRoad());
        newRealEstate.setAdjacentAlleyMinWidth(pEstate.getAdjacentAlleyMinWidth());
        newRealEstate.setAlleyMinWidth(pEstate.getAlleyMinWidth());
        newRealEstate.setFactor(pEstate.getFactor());
        newRealEstate.setStructure(pEstate.getStructure());
        newRealEstate.setDTSXD(pEstate.getDTSXD());
        newRealEstate.setCLCL(pEstate.getCLCL());
        newRealEstate.setCTXDPrice(pEstate.getCTXDPrice());
        newRealEstate.setCTXDValue(pEstate.getCTXDValue());
        if (!pEstate.getPhoto().equals(null) && !pEstate.getPhoto().equals("")) {
            String photoPath = imageService.uploadImageToDB(file);
            newRealEstate.setPhoto(photoPath);
        }
        // newRealEstate.setPhoto(pEstate.getPhoto());
        newRealEstate.setLat(pEstate.getLat());
        newRealEstate.setLng(pEstate.getLng());
        RealEstate createdRealEstate = realEstateRepository.save(newRealEstate);
        return createdRealEstate;
    }

    @Transactional
    public RealEstate updateEstate(RealEstate pEstate, Integer id, List<String> auth, MultipartFile file)
            throws IOException {
        Optional<RealEstate> realEstateData = realEstateRepository.findById(id);
        if (realEstateData.isPresent()) {
            RealEstate foundRealEstate = realEstateData.get();
            foundRealEstate.setTitle(pEstate.getTitle());
            foundRealEstate.setType(pEstate.getType());
            foundRealEstate.setRequest(pEstate.getRequest());
            foundRealEstate.setProvinceId(pEstate.getProvinceId());
            foundRealEstate.setDistrictId(pEstate.getDistrictId());
            foundRealEstate.setWardId(pEstate.getWardId());
            foundRealEstate.setStreetId(pEstate.getStreetId());
            foundRealEstate.setProjectId(pEstate.getProjectId());
            foundRealEstate.setCustomerId(pEstate.getCustomerId());
            foundRealEstate.setAddress(pEstate.getAddress());
            foundRealEstate.setPrice(pEstate.getPrice());
            foundRealEstate.setPriceMin(pEstate.getPriceMin());
            foundRealEstate.setPriceTime(pEstate.getPriceTime());
            foundRealEstate.setAcreage(pEstate.getAcreage());
            foundRealEstate.setDirection(pEstate.getDirection());
            foundRealEstate.setTotalFloors(pEstate.getTotalFloors());
            foundRealEstate.setNumberFloors(pEstate.getNumberFloors());
            foundRealEstate.setBath(pEstate.getBath());
            foundRealEstate.setApartCode(pEstate.getApartCode());
            foundRealEstate.setWallArea(pEstate.getWallArea());
            foundRealEstate.setBedroom(pEstate.getBedroom());
            foundRealEstate.setBalcony(pEstate.getBalcony());
            foundRealEstate.setLandscapeView(pEstate.getLandscapeView());
            foundRealEstate.setApartType(pEstate.getApartType());
            foundRealEstate.setApartLoca(pEstate.getApartLoca());
            foundRealEstate.setFurnitureType(pEstate.getFurnitureType());
            foundRealEstate.setPriceRent(pEstate.getPriceRent());
            foundRealEstate.setReturnRate(pEstate.getReturnRate());
            foundRealEstate.setLegalDoc(pEstate.getLegalDoc());
            foundRealEstate.setDescription(pEstate.getDescription());
            foundRealEstate.setWidthY(pEstate.getWidthY());
            foundRealEstate.setLongX(pEstate.getLongX());
            foundRealEstate.setStreetHouse(pEstate.getStreetHouse());
            foundRealEstate.setFSBO(pEstate.getFSBO());
            foundRealEstate.setViewNum(pEstate.getViewNum());
            foundRealEstate.setShape(pEstate.getShape());
            foundRealEstate.setDistance2facade(pEstate.getDistance2facade());
            foundRealEstate.setAdjacentFacadeNum(pEstate.getAdjacentFacadeNum());
            foundRealEstate.setAdjacentRoad(pEstate.getAdjacentRoad());
            foundRealEstate.setAdjacentAlleyMinWidth(pEstate.getAdjacentAlleyMinWidth());
            foundRealEstate.setAlleyMinWidth(pEstate.getAlleyMinWidth());
            foundRealEstate.setFactor(pEstate.getFactor());
            foundRealEstate.setStructure(pEstate.getStructure());
            foundRealEstate.setDTSXD(pEstate.getDTSXD());
            foundRealEstate.setCLCL(pEstate.getCLCL());
            foundRealEstate.setCTXDPrice(pEstate.getCTXDPrice());
            foundRealEstate.setCTXDValue(pEstate.getCTXDValue());
            if (!pEstate.getPhoto().equals(null) && !pEstate.getPhoto().equals("")) {
                if(foundRealEstate.getPhoto() != null && !foundRealEstate.getPhoto().equals("")){
                    imageRepository.deleteImageByName(foundRealEstate.getPhoto());
                    imageService.deleteImageToLocal(foundRealEstate.getPhoto());
                }
                String photoPath = imageService.uploadImageToDB(file);
                foundRealEstate.setPhoto(photoPath);
            }
            foundRealEstate.setLat(pEstate.getLat());
            foundRealEstate.setLng(pEstate.getLng());
            foundRealEstate.setLng(pEstate.getLng());
            if (auth.size() != 0) {
                foundRealEstate.setCensorred(pEstate.getCensorred());
            }
            RealEstate updatedRealEstate = realEstateRepository.save(foundRealEstate);
            return updatedRealEstate;
        } else {
            return null;
        }
    }

    public void deleteRealEstateById(Integer id) {
        realEstateRepository.deleteById(id);
    }

    public void deleteAllRealEstate() {
        realEstateRepository.deleteAll();
    }

    public Page<RealEstate> findEstateByEmployee(Integer employeeId, Integer page, Integer size) {
        return realEstateRepository.findEstateByEmployee(employeeId, PageRequest.of(page, size, Sort.by("date_create").descending()));
    }

    public Page<RealEstate> findEstateBySeller(Integer employeeId, Integer page, Integer size) {
        return realEstateRepository.findEstateBySeller(employeeId, PageRequest.of(page, size, Sort.by("date_create").descending()));
    }

    public Page<RealEstate> getAllRealEstateCensorred(Integer page, Integer size) {
        Page<RealEstate> listEstate = realEstateRepository.findEstateCensorred(PageRequest.of(page, size,Sort.by("date_create").descending()));
        return listEstate;
    }

    public List<RealEstate> getRealEstateSearch(String title, Integer request, Integer type, Integer price,
            BigDecimal acreage) {
        List<RealEstate> listEstate = new ArrayList<>();
        listEstate = realEstateRepository.findEstateSearch(title, request, type, price, acreage);
        System.out.println(title);
        System.out.println(request);
        System.out.println(type);
        System.out.println(price);
        System.out.println(acreage);
        return listEstate;
    }

    public List<RealEstate> findTypeProperty(String type) {
        List<RealEstate> listEstate = new ArrayList<>();
        listEstate = realEstateRepository.findTypeProperty(type);
        return listEstate;
    }

    public List<RealEstate> searchProperty(String search) {
        List<RealEstate> listEstate = new ArrayList<>();
        listEstate = realEstateRepository.findEstateSearch(search);
        return listEstate;
    }

    public Page<RealEstate> findEstateByProvince(Integer provinceId, Integer page, Integer size) {
        Page<RealEstate> listEstate = realEstateRepository.findRealEstateByProvinceId(provinceId,
                PageRequest.of(page, size));
        return listEstate;
    }

    public Page<RealEstate> findEstateByDistrict(Integer districtId, Integer page, Integer size) {
        Page<RealEstate> listEstate = realEstateRepository.findRealEstateByDistrictId(districtId,
                PageRequest.of(page, size));
        return listEstate;
    }

    public Page<RealEstate> findEstateByWard(Integer wardId, Integer page, Integer size) {
        Page<RealEstate> listEstate = realEstateRepository.findRealEstateByWardId(wardId, PageRequest.of(page, size));
        return listEstate;
    }

    public Page<RealEstate> getRealEstateAdvancedSearch(String location, Integer request, Integer type,
            Integer direction, Long priceMin, Long priceMax, BigDecimal acreage, Integer customerId, Date startDate,
            Date endDate, Integer censored, Integer page, Integer size) {
        Page<RealEstate> listEstate = realEstateRepository.findEstateAdvancedSearch(location, request, type, direction,
                priceMin, priceMax, acreage, customerId, startDate, endDate, censored,
                PageRequest.of(page, size, Sort.by("date_create").descending()));
        return listEstate;
    }

    public Page<RealEstate> getRealEstateAdvancedSearchBySeller(Integer employeeId, String location, Integer request,
            Integer type, Integer direction, Long priceMin, Long priceMax, BigDecimal acreage, Integer customerId,
            Date startDate, Date endDate, Integer censored, Integer page, Integer size) {
        Page<RealEstate> listEstate = realEstateRepository.findEstateAdvancedSearchBySeller(employeeId, location,
                request, type, direction, priceMin, priceMax, acreage, customerId, startDate, endDate, censored,
                PageRequest.of(page, size, Sort.by("date_create").descending()));
        System.out.println(request);
        return listEstate;
    }

    public Page<RealEstate> getRealEstateAdvancedSearchByEmployee(Integer employeeId, String location, Integer request,
            Integer type, Integer direction, Long priceMin, Long priceMax, BigDecimal acreage, Integer customerId,
            Date startDate, Date endDate, Integer censored, Integer page, Integer size) {
        Page<RealEstate> listEstate = realEstateRepository.findEstateAdvancedSearchByEmployee(employeeId, location,
                request, type, direction, priceMin, priceMax, acreage, customerId, startDate, endDate, censored,
                PageRequest.of(page, size, Sort.by("date_create").descending()));
        return listEstate;
    }

    //Lấy giá trị price MAX
    public Long getPriceMax() {
        return realEstateRepository.maxPriceEsatble();
    }

}

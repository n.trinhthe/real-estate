package com.devcamp.realestate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.model.AddressMap;
import com.devcamp.realestate.repository.AddressMapRepository;

@Service
public class AddressMapService {
    @Autowired
    AddressMapRepository addressmapRepository;

    public List<AddressMap> getAllAddressMap(){
        ArrayList<AddressMap> addressmapList = new ArrayList<>();
        addressmapRepository.findAll().forEach(addressmapList::add);
        return addressmapList;
    }
    
    public AddressMap getAddressMapById(Integer id){
        Optional<AddressMap> addressmapData =  addressmapRepository.findById(id);
        if (addressmapData.isPresent()){
            return addressmapData.get();
        }
        else {
            return null;
        }
    }

    public AddressMap createAddressMap(AddressMap pAddressMap){
        AddressMap newAddressMap = new AddressMap();
        newAddressMap.setAddress(pAddressMap.getAddress());
        newAddressMap.setLat(pAddressMap.getLat());
        newAddressMap.setLng(pAddressMap.getLng());
        addressmapRepository.save(newAddressMap);
        return newAddressMap;
    }

    public AddressMap updatedAddressMap (AddressMap pAddressMap, Integer id){
        Optional<AddressMap> addressmapData =  addressmapRepository.findById(id);
        if (addressmapData.isPresent()){
            AddressMap updatedAddressMap = addressmapData.get();
            updatedAddressMap.setAddress(pAddressMap.getAddress());
            updatedAddressMap.setLat(pAddressMap.getLat());
            updatedAddressMap.setLng(pAddressMap.getLng());
            addressmapRepository.save(updatedAddressMap);
            return updatedAddressMap;
        }
        else {
            return null;
        }
    }

    public void delelteAddressMap(Integer id){
        addressmapRepository.deleteById(id);
    }
}


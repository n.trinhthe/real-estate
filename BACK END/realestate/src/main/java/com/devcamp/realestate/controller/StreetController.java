package com.devcamp.realestate.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.model.Street;
import com.devcamp.realestate.model.custom.IStreetCountEstate;
import com.devcamp.realestate.service.StreetService;

@RestController
@CrossOrigin
public class StreetController {
    @Autowired
    StreetService streetService;

    @GetMapping("/streets")
    public ResponseEntity<List<Street>> getAllStreets() {
        try {
            return new ResponseEntity<>(streetService.getAllStreets(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/streets/count")
    public ResponseEntity<List<IStreetCountEstate>> getAllStreetCount() {
        try {
            return new ResponseEntity<>(streetService.getAllStreetCount(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/streets/{streetId}")
    public ResponseEntity<Street> getAllStreets(@PathVariable("streetId") Integer streetId) {
        Street findStreet = streetService.getStreetById(streetId);
        if (findStreet != null) {
            try {
                return new ResponseEntity<>(findStreet, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/streets")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Street> createStreet(@RequestBody Street pStreet) {
        try {
            return new ResponseEntity<>(streetService.createStreet(pStreet), HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/streets/{streetId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Street> updateStreet(@RequestBody Street pStreet, @PathVariable("streetId") Integer streetId) {
        Street updateStreet = streetService.updateStreetById(pStreet,streetId);
        if (updateStreet != null) {
            try {
                return new ResponseEntity<>(updateStreet, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/streets/{streetId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Street> deleteStreetById(@PathVariable("streetId") Integer streetId) {
        try {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/{districtId}/streets")
    public ResponseEntity<Set<Street>> getStreetsOfDistrict(@PathVariable("districtId") Integer districtId) {
        Set<Street> streetList = streetService.getStreetsOfDistrict(districtId);
        if (streetList != null) {
            try {
                return new ResponseEntity<>(streetList, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}

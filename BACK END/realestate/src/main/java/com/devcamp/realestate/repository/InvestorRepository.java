package com.devcamp.realestate.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.model.Investor;

public interface InvestorRepository extends JpaRepository<Investor, Integer>{
    Optional<Investor> findById(Integer id);
}

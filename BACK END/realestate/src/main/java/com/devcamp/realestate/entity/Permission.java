package com.devcamp.realestate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "permission")
public class Permission extends BaseEntity {

	@Column(name = "permission_name")
    private String permissionName;
	@Column(name = "permission_key")
    private String permissionKey;

	/**
	 * @return the permissionName
	 */
	public String getPermissionName() {
		return permissionName;
	}

	/**
	 * @param permissionName the permissionName to set
	 */
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	/**
	 * @return the permissionKey
	 */
	public String getPermissionKey() {
		return permissionKey;
	}

	/**
	 * @param permissionKey the permissionKey to set
	 */
	public void setPermissionKey(String permissionKey) {
		this.permissionKey = permissionKey;
	}

}

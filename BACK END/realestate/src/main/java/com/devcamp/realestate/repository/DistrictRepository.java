package com.devcamp.realestate.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.realestate.model.District;
import com.devcamp.realestate.model.custom.IDistrictCountEstate;

public interface DistrictRepository extends JpaRepository<District,Integer> {
    Optional<District> findById(Integer id);

    @Query(value = "SELECT d.id AS id, d._name AS name, d._prefix AS prefix , COUNT(re.district_id) AS countEstate " 
    + "FROM district AS d "
    + "LEFT JOIN realestate AS re ON re.district_id = d.id "
    + "GROUP BY d._name "
    + "ORDER BY d.id "
    ,nativeQuery = true)
    List<IDistrictCountEstate> findDistrictCountEstate();
}

package com.devcamp.realestate.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.realestate.model.RealEstate;
import com.devcamp.realestate.service.RealEstateService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin
public class RealEstateController {
    @Autowired
    RealEstateService realEstateService;

    @GetMapping(value = "/realestates", params = { "page", "size" })
    public ResponseEntity<Page<RealEstate>> getAllRealEstatePageable(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "24") Integer size) {
        try {
            return new ResponseEntity<>(realEstateService.getAllRealEstatePageable(page, size), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestates")
    // @PreAuthorize("hasRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Page<RealEstate>> getAllRealEstate(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "24") Integer size) {
        try {
            return new ResponseEntity<>(realEstateService.getAllRealEstate(page, size), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customers/{customerId}/realestates")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<RealEstate>> getRealEstateByCustomerId(@PathVariable("customerId") Integer customerId) {
        try {
            return new ResponseEntity<>(realEstateService.getRealEstateByCustomerId(customerId),
                    HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestates/{id}")
    // @PreAuthorize("isAuthenticated()")
    // @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER','ROLE_CUSTOMER','ROLE_HOMESELLER')")
    public ResponseEntity<RealEstate> getRealEstateById(@PathVariable("id") Integer id) {
        RealEstate foundRealEstate = realEstateService.findRealEstateById(id);
        if (foundRealEstate != null) {
            try {
                return new ResponseEntity<>(foundRealEstate, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/realestates")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Object> createRealEstate(@Valid @RequestParam("realEstate") String pRealEstate,
            @Valid @RequestParam("image") MultipartFile file) throws IOException {
        // Chuyển Object Real Estate từ JSON
        ObjectMapper mapper = new ObjectMapper();
        RealEstate realEstate = mapper.readValue(pRealEstate, RealEstate.class);
        try {
            return new ResponseEntity<>(realEstateService.createEstate(realEstate, file), HttpStatus.CREATED);
        } catch (Exception ex) {
            return ResponseEntity.unprocessableEntity().body("Failed");
        }
    }

    @PutMapping("/realestates/{id}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Object> updateRealEstate(@Valid @RequestParam("realEstate") String pRealEstate,
            @Valid @PathVariable(value = "id") Integer id,
            @Valid @RequestParam("image") MultipartFile file) throws IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<String> authList = Arrays.asList("ROLE_ADMIN", "ROLE_MANAGER", "ROLE_HOMESELLER");
        List<String> auth = authList.stream().filter(e -> authentication.getAuthorities().toString().contains(e))
                .collect(Collectors.toList());
        System.out.println(auth);
        // Chuyển Object Real Estate từ JSON
        ObjectMapper mapper = new ObjectMapper();
        RealEstate realEstate = mapper.readValue(pRealEstate, RealEstate.class);
        RealEstate updatedRealEstate = realEstateService.updateEstate(realEstate, id, auth, file);
        if (updatedRealEstate != null) {
            try {
                return new ResponseEntity<>(updatedRealEstate, HttpStatus.OK);
            } catch (Exception ex) {
                return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/realestates/{id}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Integer> deleteRealEstate(@PathVariable("id") Integer id) {
        try {
            realEstateService.deleteRealEstateById(id);
            return new ResponseEntity<>(id, HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/realestates/all")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Integer> deleteRealEstate() {
        try {
            realEstateService.deleteAllRealEstate();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/employees/{employeeId}/realestates", params = { "page", "size" })
    @PreAuthorize("isAuthenticated()")
    // @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Page<RealEstate>> getRealEstateByEmployee(
            @PathVariable("employeeId") Integer employeeId,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "24") Integer size) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        try {
            if (authentication.getAuthorities().toString().contains("ROLE_ADMIN")
                    || authentication.getAuthorities().toString().contains("ROLE_MANAGER")) {
                return new ResponseEntity<>(realEstateService.getAllRealEstate(page, size), HttpStatus.OK);
            } else if (authentication.getAuthorities().toString().contains("ROLE_HOMESELLER")) {
                return new ResponseEntity<>(realEstateService.findEstateBySeller(employeeId, page, size),
                        HttpStatus.OK);
            } else {
                return new ResponseEntity<>(realEstateService.findEstateByEmployee(employeeId, page, size),
                        HttpStatus.OK);
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // public
    // ResponseEntity<Page<RealEstate>>getRealEstateCensorred(@PathVariable("employeeId")
    // Integer employeeId) {
    // Authentication authentication =
    // SecurityContextHolder.getContext().getAuthentication();
    // try {
    // if (authentication.getAuthorities().toString().contains("ROLE_ADMIN")
    // || authentication.getAuthorities().toString().contains("ROLE_MANAGER")) {
    // return new ResponseEntity<>(realEstateService.getAllRealEstate(),
    // HttpStatus.OK);
    // } else if
    // (authentication.getAuthorities().toString().contains("ROLE_HOMESELLER")) {
    // return new ResponseEntity<>(realEstateService.findEstateBySeller(employeeId),
    // HttpStatus.OK);
    // } else {
    // return new
    // ResponseEntity<>(realEstateService.findEstateByEmployee(employeeId),
    // HttpStatus.OK);
    // }
    // } catch (Exception ex) {
    // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    // }

    @GetMapping(value = "/realestates/censorred", params = { "page", "size" })
    public ResponseEntity<Page<RealEstate>> getRealEstateSearch(
            @RequestParam(value = "page", defaultValue = "") Integer page,
            @RequestParam(value = "size", defaultValue = "") Integer size) {
        try {
            return new ResponseEntity<>(realEstateService.getAllRealEstateCensorred(page, size), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/realestates/search", params = { "title", "request", "type", "acreage", "price" })
    public ResponseEntity<List<RealEstate>> getRealEstateSearch(
            @RequestParam(value = "price", defaultValue = "5000") Integer price,
            @RequestParam(value = "title", defaultValue = "") String title,
            @RequestParam(value = "request", defaultValue = "0") Integer request,
            @RequestParam(value = "type", defaultValue = "0") Integer type,
            @RequestParam(value = "acreage", defaultValue = "0") BigDecimal acreage) {
        try {
            System.out.println(price);
            System.out.println(request);
            System.out.println(type);
            return new ResponseEntity<>(realEstateService.getRealEstateSearch(title, request, type, price, acreage),
                    HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/realestates", params = "type")
    public ResponseEntity<List<RealEstate>> getTypeProperty(
            @RequestParam(value = "type") String type) {
        try {
            return new ResponseEntity<>(realEstateService.findTypeProperty(type), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/realestates", params = "search")
    public ResponseEntity<List<RealEstate>> searchEstateTable(
            @RequestParam(value = "search") String search) {
        try {
            return new ResponseEntity<>(realEstateService.searchProperty(search), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/realestates", params = { "provinceId", "page", "size" })
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Page<RealEstate>> getEstateByProvince(
            @RequestParam(value = "provinceId") Integer provinceId,
            @RequestParam(value = "page", defaultValue = "") Integer page,
            @RequestParam(value = "size", defaultValue = "") Integer size) {
        try {
            return new ResponseEntity<>(realEstateService.findEstateByProvince(provinceId, page, size), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/realestates", params = { "districtId", "page", "size" })
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Page<RealEstate>> getEstateByDistrict(
            @RequestParam(value = "districtId") Integer districtId,
            @RequestParam(value = "page", defaultValue = "") Integer page,
            @RequestParam(value = "size", defaultValue = "") Integer size) {
        try {
            return new ResponseEntity<>(realEstateService.findEstateByDistrict(districtId, page, size), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/realestates", params = { "wardId", "page", "size" })
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Page<RealEstate>> getEstateByWard(
            @RequestParam(value = "wardId") Integer wardId,
            @RequestParam(value = "page", defaultValue = "") Integer page,
            @RequestParam(value = "size", defaultValue = "") Integer size) {
        try {
            return new ResponseEntity<>(realEstateService.findEstateByWard(wardId, page, size), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/employees/{employeeId}/realestates/advancedsearch", params = { "location", "request", "type",
            "direction", "acreage", "priceMin", "priceMax", "customerId", "startDate", "endDate", "censored", "page",
            "size" })
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Page<RealEstate>> getRealEstateAdvencedSearch(
            @PathVariable(value = "employeeId") Integer employeeId,
            @RequestParam(value = "location", defaultValue = "") String location,
            @RequestParam(value = "request", defaultValue = "") Integer request,
            @RequestParam(value = "type", defaultValue = "") Integer type,
            @RequestParam(value = "direction", defaultValue = "") Integer direction,
            @RequestParam(value = "priceMin", defaultValue = "") Long priceMin,
            @RequestParam(value = "priceMax", defaultValue = "") Long priceMax,
            @RequestParam(value = "acreage", defaultValue = "") BigDecimal acreage,
            @RequestParam(value = "customerId", defaultValue = "") Integer customerId,
            @RequestParam(value = "startDate", defaultValue = "0000-00-00T00:00") String startDateString,
            @RequestParam(value = "endDate", defaultValue = "9999-00-00T00:00") String endDateString,
            @RequestParam(value = "censored", defaultValue = "-1") Integer censored,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
            Date startDate = formatterDate.parse(startDateString);
            System.out.println(startDate);
            Date endDate = formatterDate.parse(endDateString);
            System.out.println(endDate);
            if (authentication.getAuthorities().toString().contains("ROLE_ADMIN")
                    || authentication.getAuthorities().toString().contains("ROLE_MANAGER")) {
                return new ResponseEntity<>(realEstateService.getRealEstateAdvancedSearch(location, request, type,
                        direction, priceMin, priceMax, acreage, customerId, startDate, endDate, censored, page, size),
                        HttpStatus.OK);
            } else if (authentication.getAuthorities().toString().contains("ROLE_HOMESELLER")) {
                return new ResponseEntity<>(realEstateService.getRealEstateAdvancedSearchBySeller(employeeId, location,
                        request, type, direction, priceMin, priceMax, acreage, customerId, startDate, endDate, censored,
                        page, size), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(realEstateService.getRealEstateAdvancedSearchByEmployee(employeeId,
                        location, request, type, direction, priceMin, priceMax, acreage, customerId, startDate, endDate,
                        censored, page, size),
                        HttpStatus.OK);
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/realestates/pricemax")
    public ResponseEntity<Long> getPriceMaxEntity() {
        try {
            return new ResponseEntity<>(realEstateService.getPriceMax(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

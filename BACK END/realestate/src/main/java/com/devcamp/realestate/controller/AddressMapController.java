package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.model.AddressMap;
import com.devcamp.realestate.service.AddressMapService;

@RestController
@CrossOrigin
public class AddressMapController {
    @Autowired
    AddressMapService addressmapService;

    @GetMapping("/addressmaps")
    public ResponseEntity<List<AddressMap>> getAllAddressMap(){
        try {
            return new ResponseEntity<>(addressmapService.getAllAddressMap(),HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/addressmaps/{id}")
    public ResponseEntity<AddressMap> getAddressMapById(@PathVariable ("id") Integer id){
        AddressMap findAddressMap = addressmapService.getAddressMapById(id);
        if (findAddressMap!=null){
            try {
                return new ResponseEntity<>(findAddressMap,HttpStatus.OK);
            }
            catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/addressmaps")
    public ResponseEntity<AddressMap> createAddressMap(@RequestBody AddressMap pAddressMap){
        try {
            return new ResponseEntity<>(addressmapService.createAddressMap(pAddressMap),HttpStatus.CREATED);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    @PutMapping("/addressmaps/{id}")
    public ResponseEntity<AddressMap> updateAddressMap(@RequestBody AddressMap pAddressMap, @PathVariable ("id") Integer id){
        AddressMap updatedAddressMap = addressmapService.updatedAddressMap(pAddressMap,id);
        if (updatedAddressMap!=null){
            try {
                return new ResponseEntity<>(updatedAddressMap,HttpStatus.OK);
            }
            catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/addressmaps/{id}")
    public ResponseEntity<AddressMap> deleteDistrcit(@PathVariable ("id") Integer id){
            try {
                addressmapService.delelteAddressMap(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
    } 
} 


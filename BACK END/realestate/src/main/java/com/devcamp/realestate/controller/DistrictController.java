package com.devcamp.realestate.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.model.District;
import com.devcamp.realestate.model.custom.IDistrictCountEstate;
import com.devcamp.realestate.service.DistrictService;

@RestController
@CrossOrigin
public class DistrictController {
    @Autowired
    DistrictService districtService;

    @GetMapping("/districts")
    public ResponseEntity<List<District>> getAllDistrict(){
        try {
            return new ResponseEntity<>(districtService.getAllDistrict(),HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/count")
    public ResponseEntity<List<IDistrictCountEstate>> getAllDistrictCount(){
        try {
            return new ResponseEntity<>(districtService.getDistrictCountEstate(),HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/{id}")
    public ResponseEntity<District> getDistrictById(@PathVariable ("id") Integer id){
        District findDistrict = districtService.getDistrictById(id);
        if (findDistrict!=null){
            try {
                return new ResponseEntity<>(findDistrict,HttpStatus.OK);
            }
            catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/districts")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<District> createDistrict(@RequestBody District pDistrict){
        try {
            return new ResponseEntity<>(districtService.createDistrict(pDistrict),HttpStatus.CREATED);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    @PutMapping("/districts/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<District> updateDistrict(@RequestBody District pDistrict, @PathVariable ("id") Integer id){
        District updatedDistrict = districtService.updatedDistrict(pDistrict,id);
        if (updatedDistrict!=null){
            try {
                return new ResponseEntity<>(updatedDistrict,HttpStatus.OK);
            }
            catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/districts/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<District> deleteDistrict(@PathVariable ("id") Integer id){
            try {
                districtService.delelteDistrict(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
    } 

    @GetMapping("/provinces/{provinceId}/districts")
    public ResponseEntity<Set<District>> getDistrictOfProvinceId(@PathVariable ("provinceId") Integer provinceId){
        Set<District> districtList = districtService.getDistrictOfProvinceId(provinceId);
        if (districtList!=null){
            try {
                return new ResponseEntity<>(districtList, HttpStatus.OK);
            }
            catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
} 
}

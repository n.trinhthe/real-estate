package com.devcamp.realestate.repository;

import java.util.List;
import java.util.Optional;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.realestate.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer>{
    Optional<Customer> findById(Integer id);

    // @Query(nativeQuery = true, value = "SELECT * FROM customers WHERE "
    // + " WHEN (SELECT er.role_id FROM employee_role AS er WHERE er.employee_id = :employeeId) = 1 OR 2 THEN 1 "
    // +       "WHEN SELECT er.role_id FROM employee_role AS er WHERE er.employee_id = :employeeId) = 3 THEN "
    // +           "(SELECT e.EmployeeID FROM employees AS e WHERE e.ReportsTo = :employeeId ) = :employeeId"
    // +       "THEN customers.create_by = :employeeId"
    // + "ELSE SELECT * FROM customers WHERE customers.create_by = :employeeId END")
    // List<Customer> findCustomerByEmployee(Integer employeeId);

    @Query(nativeQuery = true, value = "SELECT * FROM customers WHERE customers.create_by = :employeeId")
    List<Customer> findCustomerByEmployee(Integer employeeId);

    @Query(nativeQuery = true, value = "SELECT * FROM customers AS c " 
    + "JOIN employees AS e ON c.create_by = e.EmployeeID "
    + "WHERE c.create_by = :employeeId  OR e.ReportsTo = :employeeId")
    List<Customer> findCustomerBySeller(Integer employeeId);
}

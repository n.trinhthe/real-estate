package com.devcamp.realestate.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.realestate.model.Ward;
import com.devcamp.realestate.model.custom.IWardCountEstate;

public interface WardRepository extends JpaRepository<Ward, Integer> {
    Optional<Ward> findById(Integer id);

    @Query(value = "SELECT w.id AS id, w._name AS name, w._prefix AS prefix , COUNT(re.wards_id) AS countEstate, COUNT(pr._ward_id) AS countProject " 
    + "FROM ward AS w "
    + "LEFT JOIN realestate AS re ON re.wards_id = w.id "
    + "LEFT JOIN project AS pr ON re.project_id = pr.id "
    + "GROUP BY w._name "
    + "ORDER BY w.id "
    ,nativeQuery = true)
    List<IWardCountEstate> findAllWard();
}

package com.devcamp.realestate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.model.Utility;
import com.devcamp.realestate.repository.UtilityRepository;

@Service
public class UtilityService {
    @Autowired
    UtilityRepository utilityRepository;

    public List<Utility> getAllUtility() {
        ArrayList<Utility> utilityList = new ArrayList<>();
        utilityRepository.findAll().forEach(utilityList::add);
        return utilityList;
    }

    public Utility getUtilityById(Integer id) {
        Optional<Utility> utilityData = utilityRepository.findById(id);
        if (utilityData.isPresent()) {
            return utilityData.get();
        } else {
            return null;
        }
    }

    public Utility createUtility(Utility pUtility) {
        Utility newUtility = new Utility();
        newUtility.setName(pUtility.getName());
        newUtility.setDescription(pUtility.getDescription());
        newUtility.setPhoto(pUtility.getPhoto());
        Utility createdUtility = utilityRepository.save(newUtility);
        return createdUtility;
    }

    public Utility updateUtility(Utility pUtility, Integer id) {
        Optional<Utility> utilityData = utilityRepository.findById(id);
        if (utilityData.isPresent()) {
            Utility foundUtility = utilityData.get();
            foundUtility.setName(pUtility.getName());
            foundUtility.setDescription(pUtility.getDescription());
            foundUtility.setPhoto(pUtility.getPhoto());
            Utility updatedUtility = utilityRepository.save(foundUtility);
            return updatedUtility;
        } else {
            return null;
        }
    }

    public void deleteUtility(Integer id) {
        utilityRepository.deleteById(id);
    }

    public void deleteAllUtility(){
        utilityRepository.deleteAll();;
    }
}

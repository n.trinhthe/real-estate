package com.devcamp.realestate.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.model.DesignUnit;

public interface DesignUnitRepository extends JpaRepository<DesignUnit, Integer>{
    Optional<DesignUnit> findById(Integer id);
}

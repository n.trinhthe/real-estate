package com.devcamp.realestate.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.model.RegionLink;

public interface RegionLinkRepository extends JpaRepository<RegionLink, Integer> {
    Optional<RegionLink> findById(Integer id);
}

package com.devcamp.realestate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.model.Investor;
import com.devcamp.realestate.repository.InvestorRepository;

@Service
public class InvestorService {
    @Autowired
    InvestorRepository investorRepository;
    
    public List<Investor> getAllInvestors() {
        ArrayList<Investor> investorList = new ArrayList<>();
        investorRepository.findAll().forEach(investorList::add);
        return investorList;
    }

    public Investor getInvestorById(Integer id) {
        Optional<Investor> investorData = investorRepository.findById(id);
        if (investorData.isPresent()) {
            return investorData.get();
        } else {
            return null;
        }
    }

    public Investor createInvestor(Investor pInvestor) {
        Investor newInvestor = new Investor();
        newInvestor.setName(pInvestor.getName());
        newInvestor.setDescription(pInvestor.getDescription());
        newInvestor.setAddress(pInvestor.getAddress());
        newInvestor.setPhone(pInvestor.getPhone());
        newInvestor.setPhone2(pInvestor.getPhone2());
        newInvestor.setFax(pInvestor.getFax());
        newInvestor.setEmail(pInvestor.getEmail());
        newInvestor.setWebsite(pInvestor.getWebsite());
        newInvestor.setNote(pInvestor.getNote());
        Investor createdInvestor =  investorRepository.save(newInvestor);
        return createdInvestor;
    }

    public Investor updateInvestorById(Investor pInvestor, Integer id) {
        Optional<Investor> investorData = investorRepository.findById(id);
        if (investorData.isPresent()) {
            Investor foundInvestor = investorData.get();
            foundInvestor.setName(pInvestor.getName());
            foundInvestor.setDescription(pInvestor.getDescription());
            foundInvestor.setAddress(pInvestor.getAddress());
            foundInvestor.setPhone(pInvestor.getPhone());
            foundInvestor.setPhone2(pInvestor.getPhone2());
            foundInvestor.setFax(pInvestor.getFax());
            foundInvestor.setEmail(pInvestor.getEmail());
            foundInvestor.setWebsite(pInvestor.getWebsite());
            foundInvestor.setNote(pInvestor.getNote());
            Investor updatedInvestor =  investorRepository.save(foundInvestor);
            return updatedInvestor;
        } else {
            return null;
        }
    }

    public void deleteInvestor(Integer id) {
        investorRepository.deleteById(id);
    }
}

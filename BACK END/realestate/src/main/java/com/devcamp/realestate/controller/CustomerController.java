package com.devcamp.realestate.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.model.Customer;
import com.devcamp.realestate.service.CustomerService;

@RestController
@CrossOrigin
public class CustomerController {
    @Autowired
    CustomerService customerService;

    // @GetMapping("/customers")
    // public ResponseEntity<List<Customer>> getAllCustomer(){
    //     try {
    //         return new ResponseEntity<>(customerService.getAllCustomer(), HttpStatus.OK);
    //     }
    //     catch (Exception ex){
    //         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    //     }
    // } 

    @PostMapping("/customers")
    public ResponseEntity<Object> createCustomer(@Valid @RequestBody Customer customer){
        Customer createdCustomer = customerService.createCustomer(customer);
        try {
            return new ResponseEntity<>(createdCustomer, HttpStatus.CREATED);
        }
        catch (Exception ex){
            return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
        }
    }
    @GetMapping("/customers/{id}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable("id") Integer id){
        Customer foundCustomer = customerService.findCustomerById(id);
        if (foundCustomer != null){
            try {
                return new ResponseEntity<Customer>(foundCustomer, HttpStatus.OK);
            }
            catch (Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/customers/{id}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Object> updateCustomer(@Valid @RequestBody Customer pCustomer, @PathVariable("id") Integer id){
        Customer updatedCustomer = customerService.updateCustomer(pCustomer,id);
        if (updatedCustomer != null){
            try {
                return new ResponseEntity<Object>(updatedCustomer, HttpStatus.OK);
            }
            catch (Exception ex) {
                return ResponseEntity.unprocessableEntity().body(ex.getCause().getCause().getMessage());
            }
        }
        else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/customers/{id}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Customer> deleteCustomer(@PathVariable("id") Integer id){
        try {
            customerService.deleteCutomer(id);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/customers/all")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Customer> deleteAllCustomer(){
        try {
            customerService.deleteAllCutomer();
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/employees/{employeeId}/customers")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<Customer>> getCustomerByEmployee(@PathVariable("employeeId") Integer employeeId){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            if (auth.getAuthorities().toString().contains("ADMIN") || auth.getAuthorities().toString().contains("ROLE_MANAGER")) {
                return new ResponseEntity<>(customerService.getAllCustomer(), HttpStatus.OK);
            } else if (auth.getAuthorities().toString().contains("ROLE_HOMESELLER")) {
                return new ResponseEntity<>(customerService.findCustomerBySeller(employeeId), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(customerService.findCustomerByEmployee(employeeId), HttpStatus.OK);
            }
        }
        catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

}

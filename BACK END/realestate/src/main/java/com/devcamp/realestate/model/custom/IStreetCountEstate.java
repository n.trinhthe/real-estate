package com.devcamp.realestate.model.custom;

public interface IStreetCountEstate {
    Integer getId();
    String getName();
    String getPrefix();
    Integer getCountEstate();
    // Integer getCountProject();
}

package com.devcamp.realestate.model;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "_name")
    private String name;

    @Column(name = "_province_id")
    private Integer province;

    @Column(name = "_district_id")
    private Integer district;

    @Column(name = "_ward_id")
    private Integer ward;

    @Column(name = "_street_id")
    private Integer street;

    private String address;

    @Column(name = "slogan", columnDefinition = "MEDIUMTEXT")
    private String slogan;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    private BigDecimal acreage;

    @Column(name = "construct_area")
    private BigDecimal constructArea;

    @Column(name = "num_block")
    private Short numBlock;

    @Column(name = "num_floors")
    private String numFloors;

    @Column(name = "num_apartment", nullable = false)
    private Integer numApartment;

    @Column(name = "apartmentt_area")
    private String apartmenttArea;

    @Column(name = "investor", nullable = false)
    private Integer investor;

    @Column(name = "construction_contractor")
    private Integer constructionContractor;

    @Column(name = "design_unit")
    private Integer designUnit;

    @Column(name = "utilities", nullable = false)
    private String utilities;

    @Column(name = "region_link", nullable = false)
    private String regionLink;

    @OneToMany (mappedBy = "projectId", fetch = FetchType.LAZY)
    private Set<RealEstate> realEstates;

    private String photo;
    private double lat;
    private double lng;

    public Project() {
    }

    public Project(String name, Integer province, Integer district, Integer ward, Integer street, String address, String slogan,
            String description, BigDecimal acreage, BigDecimal constructArea, Short numBlock, String numFloors,
            Integer numApartment, String apartmenttArea, Integer investor, Integer constructionContractor, Integer designUnit,
            String utilities, String regionLink, String photo, double lat, double lng) {
        this.name = name;
        this.province = province;
        this.district = district;
        this.ward = ward;
        this.street = street;
        this.address = address;
        this.slogan = slogan;
        this.description = description;
        this.acreage = acreage;
        this.constructArea = constructArea;
        this.numBlock = numBlock;
        this.numFloors = numFloors;
        this.numApartment = numApartment;
        this.apartmenttArea = apartmenttArea;
        this.investor = investor;
        this.constructionContractor = constructionContractor;
        this.designUnit = designUnit;
        this.utilities = utilities;
        this.regionLink = regionLink;
        this.photo = photo;
        this.lat = lat;
        this.lng = lng;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public BigDecimal getContructArea() {
        return constructArea;
    }

    public void setContructArea(BigDecimal constructArea) {
        this.constructArea = constructArea;
    }

    public Short getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(Short numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloors() {
        return numFloors;
    }

    public void setNumFloors(String numFloors) {
        this.numFloors = numFloors;
    }

    public Integer getNumApartment() {
        return numApartment;
    }

    public void setNumApartment(Integer numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmenttArea() {
        return apartmenttArea;
    }

    public void setApartmenttArea(String apartmenttArea) {
        this.apartmenttArea = apartmenttArea;
    }

    public BigDecimal getConstructArea() {
        return constructArea;
    }

    public void setConstructArea(BigDecimal constructArea) {
        this.constructArea = constructArea;
    }

    public String getUtilities() {
        return utilities;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getRegionLink() {
        return regionLink;
    }

    public void setRegionLink(String regionLink) {
        this.regionLink = regionLink;
    }

    public Integer getProvince() {
        return province;
    }

    public void setProvince(Integer province) {
        this.province = province;
    }

    public Integer getDistrict() {
        return district;
    }

    public void setDistrict(Integer district) {
        this.district = district;
    }
    public Integer getWard() {
        return ward;
    }
    public void setWard(Integer ward) {
        this.ward = ward;
    }
    public Integer getStreet() {
        return street;
    }
    public void setStreet(Integer street) {
        this.street = street;
    }
    public Integer getInvestor() {
        return investor;
    }
    public void setInvestor(Integer investor) {
        this.investor = investor;
    }
    public Integer getConstructionContractor() {
        return constructionContractor;
    }
    public void setConstructionContractor(Integer constructionContractor) {
        this.constructionContractor = constructionContractor;
    }
    public Integer getDesignUnit() {
        return designUnit;
    }
    public void setDesignUnit(Integer designUnit) {
        this.designUnit = designUnit;
    }

    public Set<RealEstate> getRealEstates() {
        return realEstates;
    }

    public void setRealEstates(Set<RealEstate> realEstates) {
        this.realEstates = realEstates;
    }  

}

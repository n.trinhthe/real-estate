package com.devcamp.realestate.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.model.Ward;
import com.devcamp.realestate.model.custom.IWardCountEstate;
import com.devcamp.realestate.service.WardService;

@RestController
@CrossOrigin
public class WardController {
    @Autowired
    WardService wardService;

    @GetMapping("/wards")
    public ResponseEntity<List<Ward>> getAllWards() {
        try {
            return new ResponseEntity<>(wardService.getAllWards(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/wards/count")
    public ResponseEntity<List<IWardCountEstate>> getAllWardCount() {
        try {
            return new ResponseEntity<>(wardService.getAllWardCount(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/wards/{wardId}")
    public ResponseEntity<Ward> getAllWards(@PathVariable("wardId") Integer wardId) {
        Ward findWard = wardService.getWardById(wardId);
        if (findWard != null) {
            try {
                return new ResponseEntity<>(findWard, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/wards")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Ward> createWard(@RequestBody Ward pWard) {
        try {
            return new ResponseEntity<>(wardService.createWard(pWard), HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/wards/{wardId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public ResponseEntity<Ward> updateWard(@RequestBody Ward pWard, @PathVariable("wardId") Integer wardId) {
        Ward updateWard = wardService.updateWardById(pWard,wardId);
        if (updateWard != null) {
            try {
                return new ResponseEntity<>(updateWard, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/wards/{wardId}")
    public ResponseEntity<Ward> deleteWardById(@PathVariable("wardId") Integer wardId) {
        try {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/{districtId}/wards")

    public ResponseEntity<Set<Ward>> getWardsOfDistrict(@PathVariable("districtId") Integer districtId) {
        Set<Ward> wardList = wardService.getWardsOfDistrict(districtId);
        if (wardList != null) {
            try {
                return new ResponseEntity<>(wardList, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}

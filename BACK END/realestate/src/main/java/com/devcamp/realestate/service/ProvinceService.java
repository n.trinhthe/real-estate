package com.devcamp.realestate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.model.Province;
import com.devcamp.realestate.model.custom.IProvinceCountEstate;
import com.devcamp.realestate.repository.ProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    ProvinceRepository provinceRepository;
    public List<Province> getAllProvince(){
        ArrayList<Province> provinceList = new ArrayList<>();
        provinceRepository.findAll().forEach(provinceList::add);
        return provinceList;
    }
    public List<IProvinceCountEstate> getAllProvinceCount(){
        return provinceRepository.findAllProvinceCount();
    }

    public Province getProvinceById(Integer id){
        Optional<Province> provinceData = provinceRepository.findById(id);
        if (provinceData.isPresent()){
            return provinceData.get();
        }
        else {
            return null;
        }
    }

    public Province createProvince(Province pProvince){
        Province newProvince = new Province();
        newProvince.setCode(pProvince.getCode());
        newProvince.setName(pProvince.getName());
        provinceRepository.save(newProvince);
        return newProvince;
    }

    public Province updateProvince(Province pProvince, Integer id){
        Optional<Province> provinceData = provinceRepository.findById(id);
        if (provinceData.isPresent()){
            Province updateProvince = provinceData.get();
            updateProvince.setCode(pProvince.getCode());
            updateProvince.setName(pProvince.getName());
            provinceRepository.save(updateProvince);
            return updateProvince;
        }
        else {
            return null;
        }
    }

    public void deleteProvince(Integer id){
        provinceRepository.deleteById(id);
    }

    public Province getProvinceByCode(String code){
        Province province = provinceRepository.getByCode(code);
        if (province!=null){
            return province;
        }
        else {
            return null;
        }
    }
}

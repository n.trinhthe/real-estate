package com.devcamp.realestate.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.realestate.entity.Employee;
import com.devcamp.realestate.entity.Role;
import com.devcamp.realestate.repository.EmployeeRepository;
import com.devcamp.realestate.repository.ImageRepository;
import com.devcamp.realestate.repository.RoleRepository;
import com.devcamp.realestate.security.UserPrincipal;
import com.devcamp.realestate.service.EmployeeService;
import com.devcamp.realestate.service.ImageService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    ImageRepository imageRepository;
    @Autowired
    ImageService imageService;

    @Override
    public Employee createEmployee(Employee employee) {
        return employeeRepository.saveAndFlush(employee);
    }

    @Override
    public UserPrincipal findByUsername(String username) {
        Employee employee = employeeRepository.findByUsername(username);
        UserPrincipal userPrincipal = new UserPrincipal();
        if (null != employee) {
            Set<String> authorities = new HashSet<>();
            if (null != employee.getRoles())
                employee.getRoles().forEach(r -> {
                    authorities.add(r.getRoleKey());
                    r.getPermissions().forEach(p -> authorities.add(p.getPermissionKey()));
                });

            userPrincipal.setUserId(employee.getId());
            userPrincipal.setUsername(employee.getUsername());
            userPrincipal.setPassword(employee.getPassword());
            userPrincipal.setFirstname(employee.getFirstname());
            userPrincipal.setLastname(employee.getLastname());
            userPrincipal.setAuthorities(authorities);
        }
        return userPrincipal;
    }

    public List<Employee> getAllEmployee() {
        ArrayList<Employee> employeeList = new ArrayList<>();
        employeeRepository.findAll().forEach(employeeList::add);
        return employeeList;
    }

    public Employee findEmployeeById(Integer id) {
        Optional<Employee> employeeData = employeeRepository.findById(id);
        if (employeeData.isPresent()) {
            return employeeData.get();
        } else {
            return null;
        }
    }

    public Employee createEmployeeByAdmin(Employee pEmployee) {
        Employee newEmployee = new Employee();
        newEmployee.setLastname(pEmployee.getLastname());
        newEmployee.setFirstname(pEmployee.getFirstname());
        newEmployee.setTitle(pEmployee.getTitle());
        newEmployee.setTitleOfCourtesy(pEmployee.getTitleOfCourtesy());
        newEmployee.setBirthDate(pEmployee.getBirthDate());
        newEmployee.setHireDate(pEmployee.getBirthDate());
        newEmployee.setAddress(pEmployee.getAddress());
        newEmployee.setNotes(pEmployee.getNotes());
        newEmployee.setCity(pEmployee.getCity());
        newEmployee.setRegion(pEmployee.getRegion());
        newEmployee.setPostalCode(pEmployee.getPostalCode());
        newEmployee.setCountry(pEmployee.getCountry());
        newEmployee.setHomePhone(pEmployee.getHomePhone());
        newEmployee.setExtension(pEmployee.getExtension());
        newEmployee.setPhoto(pEmployee.getPhoto());
        newEmployee.setReportsTo(pEmployee.getReportsTo());
        newEmployee.setUsername(pEmployee.getUsername());
        newEmployee.setPassword(pEmployee.getPassword());
        newEmployee.setEmail(pEmployee.getEmail());
        newEmployee.setActivated(pEmployee.getActivated());
        newEmployee.setProfile(pEmployee.getProfile());
        newEmployee.setUserLevel(pEmployee.getUserLevel());
        Employee createdEmployee = employeeRepository.save(newEmployee);
        return createdEmployee;
    }

    @Transactional
    public Employee updateEmployee(Employee pEmployee, Integer id, MultipartFile file) throws IOException {
        Optional<Employee> employeeData = employeeRepository.findById(id);
        if (employeeData.isPresent()) {
            Employee foundEmployee = employeeData.get();
            foundEmployee.setLastname(pEmployee.getLastname());
            foundEmployee.setFirstname(pEmployee.getFirstname());
            foundEmployee.setTitle(pEmployee.getTitle());
            foundEmployee.setTitleOfCourtesy(pEmployee.getTitleOfCourtesy());
            foundEmployee.setBirthDate(pEmployee.getBirthDate());
            foundEmployee.setHireDate(pEmployee.getBirthDate());
            foundEmployee.setAddress(pEmployee.getAddress());
            foundEmployee.setNotes(pEmployee.getNotes());
            foundEmployee.setCity(pEmployee.getCity());
            foundEmployee.setRegion(pEmployee.getRegion());
            foundEmployee.setPostalCode(pEmployee.getPostalCode());
            foundEmployee.setCountry(pEmployee.getCountry());
            foundEmployee.setHomePhone(pEmployee.getHomePhone());
            System.out.println(foundEmployee.getPhoto());
            if (pEmployee.getPhoto() != null && !pEmployee.getPhoto().equals("")) {
                if (foundEmployee.getPhoto() != null && !foundEmployee.getPhoto().equals("")) {
                    imageRepository.deleteImageByName(foundEmployee.getPhoto());
                    imageService.deleteImageToLocal(foundEmployee.getPhoto());
                }
                String photoPath = imageService.uploadImageToDB(file);
                foundEmployee.setPhoto(photoPath);
            }
            foundEmployee.setReportsTo(pEmployee.getReportsTo());
            foundEmployee.setUsername(pEmployee.getUsername());
            foundEmployee.setPassword(pEmployee.getPassword());
            foundEmployee.setEmail(pEmployee.getEmail());
            foundEmployee.setActivated(pEmployee.getActivated());
            foundEmployee.setProfile(pEmployee.getProfile());
            foundEmployee.setUserLevel(pEmployee.getUserLevel());
            Optional<Role> roleData = roleRepository.findById(pEmployee.getUserLevel());
            Set<Role> roles = new HashSet<>();
            if (roleData.isPresent()) {
                roles.add(roleData.get());
                foundEmployee.setRoles(roles);
            }
            Employee updatedEmployee = employeeRepository.save(foundEmployee);
            return updatedEmployee;
        } else {
            return null;
        }
    }

    public Employee updateEmployeeProfile(Employee pEmployee, Integer id) {
        Optional<Employee> employeeData = employeeRepository.findById(id);
        if (employeeData.isPresent()) {
            Employee foundEmployee = employeeData.get();
            foundEmployee.setLastname(pEmployee.getLastname());
            foundEmployee.setFirstname(pEmployee.getFirstname());
            foundEmployee.setTitleOfCourtesy(pEmployee.getTitleOfCourtesy());
            foundEmployee.setBirthDate(pEmployee.getBirthDate());
            foundEmployee.setAddress(pEmployee.getAddress());
            foundEmployee.setCity(pEmployee.getCity());
            foundEmployee.setRegion(pEmployee.getRegion());
            foundEmployee.setPostalCode(pEmployee.getPostalCode());
            foundEmployee.setCountry(pEmployee.getCountry());
            foundEmployee.setHomePhone(pEmployee.getHomePhone());
            foundEmployee.setExtension(pEmployee.getExtension());
            if (pEmployee.getPhoto() != null || pEmployee.getPhoto() != "") {
                foundEmployee.setPhoto(pEmployee.getPhoto());
            }
            foundEmployee.setEmail(pEmployee.getEmail());
            Employee updatedEmployee = employeeRepository.save(foundEmployee);
            return updatedEmployee;
        } else {
            return null;
        }
    }

    public void deleteEmployee(Integer id) {
        employeeRepository.deleteById(id);
    }

    public void deleteAllEmployee() {
        employeeRepository.deleteAll();
        ;
    }

}

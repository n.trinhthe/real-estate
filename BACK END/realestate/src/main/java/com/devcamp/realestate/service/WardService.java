package com.devcamp.realestate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.model.District;
import com.devcamp.realestate.model.Ward;
import com.devcamp.realestate.model.custom.IWardCountEstate;
import com.devcamp.realestate.repository.DistrictRepository;
import com.devcamp.realestate.repository.WardRepository;

@Service
public class WardService {
    @Autowired
    WardRepository wardRepository;
    @Autowired
    DistrictRepository districtRepository;

    public List<Ward> getAllWards() {
        ArrayList<Ward> wardList = new ArrayList<>();
        wardRepository.findAll().forEach(wardList::add);
        return wardList;
    }

    public List<IWardCountEstate> getAllWardCount() {
        return wardRepository.findAllWard();
    }

    public Ward getWardById(Integer id) {
        Optional<Ward> wardData = wardRepository.findById(id);
        if (wardData.isPresent()) {
            return wardData.get();
        } else {
            return null;
        }
    }

    public Ward createWard(Ward pWard){
        Ward newWard = new Ward();
        newWard.setName(pWard.getName());
        newWard.setPrefix(pWard.getPrefix());
        newWard.setDistrict(pWard.getDistrict());
        wardRepository.save(newWard);
        return newWard;
    }

    public Ward updateWardById(Ward pWard, Integer id) {
        Optional<Ward> wardData = wardRepository.findById(id);
        if (wardData.isPresent()) {
            Ward updatedWard = wardData.get();
            updatedWard.setName(pWard.getName());
            updatedWard.setPrefix(pWard.getPrefix());
            updatedWard.setDistrict(pWard.getDistrict());
            wardRepository.save(updatedWard);
            return updatedWard;
        } else {
            return null;
        }
    }

    public void deleteWard(Integer id){
        wardRepository.deleteById(id);
    }

    public Set<Ward> getWardsOfDistrict(Integer districtId) {
        Optional<District> districtData = districtRepository.findById(districtId);
        if (districtData.isPresent()) {
            return districtData.get().getWards();
        } else {
            return null;
        }
    }
}

package com.devcamp.realestate.model.custom;

public interface IProvinceCountEstate {
    Integer getId();
    String getName();
    String getCode();
    Integer getCountEstate();
}

"use strict"
var gNameCol = ["id","countEstate", "name", "code", "action"];
var gProvinceList = [];
var gProvinceId = "";
const gCOL_ID = 0;
const gCOL_REAL_ESTATE = 1;
// const gCOL_PROJECT = 2;
const gCOL_NAME = 2;
const gCOL_CODE = 3;
const gCOL_ACTION = 4;
var gTableProvince = $("#table-province").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_REAL_ESTATE] },
    { data: gNameCol[gCOL_NAME] },
    { data: gNameCol[gCOL_CODE] },
    { data: gNameCol[gCOL_ACTION] }
  ],
  columnDefs: [
    {
      targets: gCOL_ACTION,
      defaultContent: `<i class="far fa-edit btn-edit text-primary fa-xl"></i> &nbsp <i class="fas fa-trash-alt btn-delete text-danger fa-xl"></i>`,
    },
    {
      targets: gCOL_REAL_ESTATE,
      render: function (data) {
        let vHTML = `
          <a class="btn btn-primary btn-estate">Real Estate <span class="badge badge-light">${data}</span></a> 
          `
        return vHTML;
      }
    },
  ],
  autoWidth: false
})
$(document).ready(function () {
  getAllProvince();
})
$("#btn-add-province").on("click", function () {
  $("#add-province-modal").modal("show");
})
$("#btn-confirm-add-province").on("click", function () {
  onBtnConfirmAddProvince()
})
$("#table-province tbody").on("click", ".btn-edit", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableProvince.row(vRowClick).data();
  // console.log(vRowData);
  gProvinceId = vRowData.id;
  $("#edit-province-modal").modal("show");
  getProvinceById(gProvinceId);
})
$("#btn-confirm-edit-province").on("click", function () {
  onBtnConfirmEditProvince()
})
$("#table-province tbody").on("click", ".btn-delete", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableProvince.row(vRowClick).data();
  console.log(vRowData);
  gProvinceId = vRowData.id;
  $("#delete-province-modal").modal("show");
})
$("#btn-confirm-delete-province").on("click", function () {
  onBtnConfirmDeleteProvince()
})
$(document).on("click",".btn-estate", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableProvince.row(vRowClick).data();
  gProvinceId = vRowData.id;
  window.location.href = "RealEstateRegion.html?provinceId=" + gProvinceId;
})

//Hàm xử lý ajax lấy dữ liệu province đã đếm dự án
function getAllProvince() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/provinces/count",
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      gProvinceList = res;
      loadDataToTable(gProvinceList);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
function loadDataToTable(pProvinceList) {
  "use strict"
  gTableProvince.clear();
  gTableProvince.rows.add(pProvinceList);
  gTableProvince.draw();
}
//Hàm xư lý nút xác nhận thêm Province
function onBtnConfirmAddProvince() {
  let vProvinceObj = {
    name: "",
    code: ""
  }
  getDataByFormAddModal(vProvinceObj);
  var vCheck = validateData(vProvinceObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/provinces",
      type: "POST",
      data: JSON.stringify(vProvinceObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      headers: gHeader,
      success: function (res) {
        console.log("Thêm Province thành công");
        getAllProvince();
        loadDataToTable(gProvinceList);
        $("#add-province-modal").modal("hide");
      },
      error: function (err) {
        alert("Thêm Province thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận chỉnh sửa Province
function onBtnConfirmEditProvince() {
  let vProvinceObj = {
    name: "",
    code: ""
  }
  getDataByFormEditModal(vProvinceObj);
  var vCheck = validateData(vProvinceObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/provinces/" + gProvinceId,
      type: "PUT",
      data: JSON.stringify(vProvinceObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      headers: gHeader,
      success: function (res) {
        console.log("Chỉnh sửa Province thành công");
        getAllProvince();
        loadDataToTable(gProvinceList);
        $("#edit-province-modal").modal("hide");
      },
      error: function (err) {
        alert("Chỉnh sửa Province thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận delete Province
function onBtnConfirmDeleteProvince() {
  $.ajax({
    url: gBASE_URL + "/provinces/" + gProvinceId,
    type: "DELETE",
    headers: gHeader,
    success: function () {
      console.log("Xóa Province thành công");
      getAllProvince();
      loadDataToTable(gProvinceList);
      $("#delete-province-modal").modal("hide");
    },
    error: function (err) {
      alert("Xóa Province thất bại" + err.responseText);
    }
  })
}
//Hàm truy xuất dữ liệu form Edit Modal
function getDataByFormEditModal(paramObj) {
  paramObj.name = $("#inp-name-edit-modal").val();
  paramObj.code = $("#inp-code-edit-modal").val();
}
//Hàm truy xuất dữ liệu form Add Modal
function getDataByFormAddModal(paramObj) {
  paramObj.name = $("#inp-name-add-modal").val();
  paramObj.code = $("#inp-code-add-modal").val();
}

function validateData(paramObj) {
  let vResult = true;
  if (paramObj.name == "") {
    alert("Chưa nhập tên tỉnh thành!");
    vResult = false;
  }
  else if (paramObj.code == "") {
    alert("Chưa nhập mã tỉnh thành!");
    vResult = false;
  }
  return vResult;
}
function getProvinceById(paramId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/provinces/" + paramId,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      $("#inp-name-edit-modal").val(res.name);
      $("#inp-code-edit-modal").val(res.code);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
"use strict"
const gBASE_URL = "http://localhost:8080/streets"
var gNameCol = ["id", "countEstate", "name", "prefix", "action"];
var gStreetList = [];
var gStreetId = "";
const gCOL_ID = 0;
const gCOL_REAL_ESTATE = 1;
const gCOL_NAME = 2;
const gCOL_PREFIX = 3;
const gCOL_ACTION = 4;
var gTableStreet = $("#table-street").DataTable({
    columns: [
        { data: gNameCol[gCOL_ID] },
        { data: gNameCol[gCOL_REAL_ESTATE] },
        { data: gNameCol[gCOL_NAME] },
        { data: gNameCol[gCOL_PREFIX] },
        { data: gNameCol[gCOL_ACTION] }
    ],
    columnDefs: [
        {
            targets: gCOL_ACTION,
            defaultContent: `<i class="far fa-edit btn-edit text-primary fa-xl"></i> &nbsp <i class="fas fa-trash-alt btn-delete text-danger fa-xl"></i>`,
        },
        {
            targets: gCOL_REAL_ESTATE,
            render: function (data) {
                let vHTML = `
                <a class="btn btn-primary btn-estate">Real Estate <span class="badge badge-light">${data}</span></a> 
               `
                return vHTML;
            }
        },
    ],
    autoWidth: false
})
$(document).ready(function () {
    getAllStreet();
})
$("#btn-add-street").on("click", function () {
    $("#add-street-modal").modal("show");
})
$("#btn-confirm-add-street").on("click", function () {
    onBtnConfirmAddStreet()
})
$("#table-street tbody").on("click", ".btn-edit", function () {
    let vRowClick = $(this).closest("tr");
    let vRowData = gTableStreet.row(vRowClick).data();
    // console.log(vRowData);
    gStreetId = vRowData.id;
    $("#edit-street-modal").modal("show");
    getStreetById(gStreetId);
})
$("#btn-confirm-edit-street").on("click", function () {
    onBtnConfirmEditStreet()
})
$("#table-street tbody").on("click", ".btn-delete", function () {
    let vRowClick = $(this).closest("tr");
    let vRowData = gTableStreet.row(vRowClick).data();
    console.log(vRowData);
    gStreetId = vRowData.id;
    $("#delete-street-modal").modal("show");
})
$("#btn-confirm-delete-street").on("click", function () {
    onBtnConfirmDeleteStreet()
})
$(document).on("click", ".btn-estate", function () {
    let vRowClick = $(this).closest("tr");
    let vRowData = gTableStreet.row(vRowClick).data();
    gStreetId = vRowData.id;
    window.location.href = "RealEstateRegion.html?streetId=" + gStreetId;
})
$(document).on("click", ".btn-project", function () {
    let vRowClick = $(this).closest("tr");
    let vRowData = gTableStreet.row(vRowClick).data();
    gStreetId = vRowData.id;
    window.location.href = "RealEstateRegion.html?streetId=" + gStreetId;
})
function getAllStreet() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/count",
        dataType: "json",
        type: "GET",
        success: function (res) {
            console.log(res);
            gStreetList = res;
            loadDataToTable(gStreetList);
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
function loadDataToTable(pStreetList) {
    "use strict"
    gTableStreet.clear();
    gTableStreet.rows.add(pStreetList);
    gTableStreet.draw();
}
//Hàm xư lý nút xác nhận thêm Street
function onBtnConfirmAddStreet() {
    let vStreetObj = {
        name: "",
        prefix: ""
    }
    getDataByFormAddModal(vStreetObj);
    var vCheck = validateData(vStreetObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            data: JSON.stringify(vStreetObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log("Thêm Street thành công");
                getAllStreet();
                loadDataToTable(gStreetList);
                $("#add-street-modal").modal("hide");
            },
            error: function (err) {
                alert("Thêm Street thất bại" + err.responseText);
            }
        })
    }
}
//Hàm xử lý nút xác nhận chỉnh sửa Street
function onBtnConfirmEditStreet() {
    let vStreetObj = {
        name: "",
        prefix: ""
    }
    getDataByFormEditModal(vStreetObj);
    var vCheck = validateData(vStreetObj);
    if (vCheck = true) {
        $.ajax({
            url: gBASE_URL + "/" + gStreetId,
            type: "PUT",
            data: JSON.stringify(vStreetObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log("Chỉnh sửa Street thành công");
                getAllStreet();
                loadDataToTable(gStreetList);
                $("#edit-street-modal").modal("hide");
            },
            error: function (err) {
                alert("Chỉnh sửa Street thất bại" + err.responseText);
            }
        })
    }
}
//Hàm xử lý nút xác nhận delete Street
function onBtnConfirmDeleteStreet() {
    $.ajax({
        url: gBASE_URL + "/" + gStreetId,
        type: "DELETE",
        headers: gHeader,
        success: function () {
            console.log("Xóa Street thành công");
            getAllStreet();
            loadDataToTable(gStreetList);
            $("#delete-street-modal").modal("hide");
            Toast.fire({
                icon: 'success',
                title: 'Delete Street Success!!!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: 'Delete Street Failed!!!'
            })
        }
    })
}
//Hàm truy xuất dữ liệu form Edit Modal
function getDataByFormEditModal(paramObj) {
    paramObj.name = $("#inp-name-edit-modal").val();
    paramObj.prefix = $("#select-prefix-edit-modal").val();
}
//Hàm truy xuất dữ liệu form Add Modal
function getDataByFormAddModal(paramObj) {
    paramObj.name = $("#inp-name-add-modal").val();
    paramObj.prefix = $("#select-prefix-add-modal").val();
}

function validateData(paramObj) {
    let vResult = true;
    if (paramObj.name == "") {
        alert("Chưa nhập tên phường xã!");
        vResult = false;
    }
    else if (paramObj.prefix == "") {
        alert("Chưa nhập prefix phường xã!");
        vResult = false;
    }
    return vResult;
}
function getStreetById(paramId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/" + paramId,
        dataType: "json",
        type: "GET",
        success: function (res) {
            console.log(res);
            $("#inp-name-edit-modal").val(res.name);
            $("#select-prefix-edit-modal").val(res.prefix);
            Toast.fire({
                icon: 'success',
                title: 'Get Street Success!!!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: 'Get Street Failed!!!'
            })
        }
    })
}
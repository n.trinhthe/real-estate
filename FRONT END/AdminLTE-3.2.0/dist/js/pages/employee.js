"use strict"
const gBASE_URL = "http://localhost:8080";
var gEmployeeList = [];
var gRealEstateList = [];
var gSTT = 1;
var gEmployeeId = "";
//Định nghĩa bảng
var gNameCol = ["id", "firstname", "lastname", "title", "titleOfCourtesy", "birthDate", "hireDate", "address", "city", "region", "postalCode", "country", "homePhone", "extension", "email", "photo", "reportsTo", "notes", 'username', "password", "activated", "profile", "userLevel"];
const gCOL_ID = 0;
const gCOL_FIRSTNAME = 1;
const gCOL_LASTNAME = 2;
const gCOL_TITLE = 3;
const gCOL_TITLE_COURTESY = 4;
const gCOL_BIRTHDATE = 5
const gCOL_HIRE_DATE = 6;
const gCOL_ADDRESS = 7;
const gCOL_CITY = 8;
const gCOL_REGION = 9;
const gCOL_POSTAL_CODE = 10;
const gCOL_COUNTRY = 11;
const gCOL_HOME_PHONE = 12;
const gCOL_EXTENSION = 13;
const gCOL_EMAIL = 14;
const gCOL_PHOTO = 15;
const gCOL_REPORTS_TO = 16;
const gCOL_NOTES = 17;
const gCOL_USERNAME = 18;
const gCOL_PASSWORD = 19;
const gCOL_ACTIVATED = 20;
const gCOL_PROFILE = 21;
const gCOL_USER_LEVEL = 22;
var gTableEmployee = $("#table-employee").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_FIRSTNAME] },
    { data: gNameCol[gCOL_LASTNAME] },
    { data: gNameCol[gCOL_TITLE] },
    { data: gNameCol[gCOL_TITLE_COURTESY] },
    { data: gNameCol[gCOL_BIRTHDATE] },
    { data: gNameCol[gCOL_HIRE_DATE] },
    { data: gNameCol[gCOL_ADDRESS] },
    { data: gNameCol[gCOL_CITY] },
    { data: gNameCol[gCOL_REGION] },
    { data: gNameCol[gCOL_POSTAL_CODE] },
    { data: gNameCol[gCOL_COUNTRY] },
    { data: gNameCol[gCOL_HOME_PHONE] },
    { data: gNameCol[gCOL_EXTENSION] },
    { data: gNameCol[gCOL_EMAIL] },
    { data: gNameCol[gCOL_PHOTO] },
    { data: gNameCol[gCOL_REPORTS_TO] },
    { data: gNameCol[gCOL_NOTES] },
    { data: gNameCol[gCOL_USERNAME] },
    { data: gNameCol[gCOL_PASSWORD] },
    { data: gNameCol[gCOL_ACTIVATED] },
    { data: gNameCol[gCOL_PROFILE] },
    { data: gNameCol[gCOL_USER_LEVEL] },
  ],
  columnDefs: [
    {
      targets: gCOL_ID,
      render: function (data) {
        return `
          <div class="text-center">
              ${gSTT++}
              <i data-id="${data}" class="far fa-edit text-primary btn-edit" style="cursor: pointer"></i>
              <i data-id="${data}" class="fas fa-trash text-primary btn-delete" style="cursor: pointer"></i>
          </div>
          `
      },
      width: "5px"
    },
    {
      targets: gCOL_PHOTO,
      render: function (data) {
        if (data != null && data != "") {
          return `<img src="${gBASE_URL}/images/${data}" style="height: 100px; width: 100px"></img>`;
        }
        else {
          return ``;
        }
      }
    },
    {
      targets: gCOL_USER_LEVEL,
      render: function (data) {
        if (data == 1) {
          return `Adminstrater`;
        }
        else if (data == 2) {
          return `Customer`;
        }
        else if (data == 3) {
          return `Manager`;
        }
        else if (data == 4) {
          return `Home Seller`;
        }
        else {
          return ``;
        }
      }
    },
  ],
  autoWidth: false,
  buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
  stateSave: true,
  stateLoadCallback: function (settings) {
    return JSON.parse(localStorage.getItem("DataTables_table-estate_/Employee.html"));
  }
})

$(document).ready(function () {
  getAllEmployee();
  loadDataToTable(gEmployeeList);
  loadDataToSelect(gEmployeeList);
})
$("#btn-add-employee").on("click", function () { //Nút hiện thị Add Employee Modal
  $("#add-employee-modal").modal("show");
})

$("#btn-confirm-add").on("click", function () { //Nút Confirm Add Employee
  onBtnAddEmployeeClick();
})

$("#table-employee tbody").on("click", "tr.odd,tr.even", function () { //Click dòng
  changeColorRow(this);
})

$(document).on("click", ".btn-edit", function () { //Nút hiện thị edit modal employee
  gEmployeeId = $(this).data("id");
  $("#edit-employee-modal").modal("show");
  getEmployeeById(gEmployeeId);
})

$("#form-update-employee").submit(function (event) { //Nút Update Employee
  event.preventDefault();
  onBtnUpdateEmployeeClick(this);
})

$(document).on("click", ".btn-delete", function () { //Nút hiện thị delete modal employee
  gEmployeeId = $(this).data("id");
  $("#delete-employee-modal").modal("show");
})

$("#btn-confirm-delete-employee").on("click", function () { //Nút Xác Nhận Delete Employee
  onBtnConfirmDeleteClick();
  $("#delete-employee-modal").modal("hide");
})

//Hàm yêu cầu lấy dữ liệu All Employee từ server
function getAllEmployee() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/employees",
    dataType: "json",
    type: "GET",
    async: false,
    headers: gHeader,
    success: function (res) {
      console.log(res);
      gEmployeeList = res;
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}

//Hàm load dữ liệu vào Bảng
function loadDataToTable(paramEmployee) {
  "use strict"
  gTableEmployee.clear();
  gTableEmployee.rows.add(paramEmployee);
  gTableEmployee.draw();
  gTableEmployee.buttons().container().prependTo('.table-tool');
}

//Hàm load dữ liệu vào Select
function loadDataToSelect(paramEmployee) {
  "use strict"
  console.log(paramEmployee);
  paramEmployee.forEach(function (e) {
    $("#select-reportsto-edit-modal").append($("<option>", {
      value: e.id,
      text: e.firstname + " " + e.lastname + " - " + e.title
    }));
  });
}

//Hàm xử lý nút Add Employee
function onBtnAddEmployeeClick() {
  "use strict"
  let vEmployeeObj = {
    firstname: "",
    lastname: "",
    title: "",
    titleOfCourtesy: "",
    birthDate: "",
    hireDate: "",
    address: "",
    city: "",
    region: "",
    postalCode: "",
    country: "",
    homePhone: "",
    extension: "",
    photo: "",
    notes: "",
    reportsTo: "",
    username: "",
    password: "",
    email: "",
    activated: "",
    profile: "",
    userLevel: ""
  }
  getDataByFormAddModal(vEmployeeObj);
  let vCheck = validateEmployeeData(vEmployeeObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/employees",
      type: "POST",
      data: JSON.stringify(vEmployeeObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      headers: gHeader,
      success: function (res) {
        console.log(res);
        alert("Add Employee Success!");
        gSTT = 1;
        getAllEmployee();
        loadDataToTable(gEmployeeList);
        refeshForm();
        Toast.fire({
          icon: 'success',
          title: 'Add Employee Success!!!'
        })
      },
      error: function (err) {
        Toast.fire({
          icon: 'error',
          title: 'Add Employee Failed: ' + err.responseJSON.error
        })
      }
    })
  }
}
//Hàm Truy xuất dữ liệu từ form
function getDataByFormAddModal(paramEmployeeObj) {
  "use strict"
  paramEmployeeObj.firstname = $("#inp-firstname-add-modal").val().trim();
  paramEmployeeObj.lastname = $("#inp-lastname-add-modal").val().trim();
  paramEmployeeObj.title = $("#inp-title-add-modal").val().trim();
  paramEmployeeObj.titleOfCourtesy = $("#inp-courtesy-add-modal").val().trim();
  paramEmployeeObj.birthDate = $("#inp-birthdate-add-modal").val().trim();
  paramEmployeeObj.hireDate = $("#inp-hiredate-add-modal").val().trim();
  paramEmployeeObj.address = $("#inp-address-add-modal").val().trim();
  paramEmployeeObj.city = $("#inp-city-add-modal").val().trim();
  paramEmployeeObj.region = $("#inp-region-add-modal").val().trim();
  paramEmployeeObj.postalCode = $("#inp-postalcode-add-modal").val().trim();
  paramEmployeeObj.country = $("#inp-country-add-modal").val().trim();
  paramEmployeeObj.homePhone = $("#inp-homephone-add-modal").val().trim();
  paramEmployeeObj.extension = $("#inp-extension-add-modal").val().trim();
  paramEmployeeObj.photo = $("#inp-photo-add-modal").val().split('\\').pop();
  paramEmployeeObj.notes = $("#textarea-notes-add-modal").val().trim();
  paramEmployeeObj.reportsTo = $("#select-reportsto-add-modal").val();
  paramEmployeeObj.username = $("#inp-username-add-modal").val().trim();
  paramEmployeeObj.password = $("#inp-password-add-modal").val().trim();
  paramEmployeeObj.email = $("#inp-email-add-modal").val().trim();
  paramEmployeeObj.activated = $("#select-activated-add-modal").val();
  paramEmployeeObj.profile = $("#textarea-profiles-add-modal").val().trim();
  paramEmployeeObj.userLevel = $("#select-user-level-add-modal").val();
}
function getDataByFormEditModal(paramEmployeeObj) {
  "use strict"
  paramEmployeeObj.firstname = $("#inp-firstname-edit-modal").val().trim();
  paramEmployeeObj.lastname = $("#inp-lastname-edit-modal").val().trim();
  paramEmployeeObj.title = $("#inp-title-edit-modal").val().trim();
  paramEmployeeObj.titleOfCourtesy = $("#inp-courtesy-edit-modal").val().trim();
  paramEmployeeObj.birthDate = $("#inp-birthdate-edit-modal").val().trim();
  paramEmployeeObj.hireDate = $("#inp-hiredate-edit-modal").val().trim();
  paramEmployeeObj.address = $("#inp-address-edit-modal").val().trim();
  paramEmployeeObj.city = $("#inp-city-edit-modal").val().trim();
  paramEmployeeObj.region = $("#inp-region-edit-modal").val().trim();
  paramEmployeeObj.postalCode = $("#inp-postalcode-edit-modal").val().trim();
  paramEmployeeObj.country = $("#inp-country-edit-modal").val().trim();
  paramEmployeeObj.homePhone = $("#inp-homephone-edit-modal").val().trim();
  paramEmployeeObj.extension = $("#inp-extension-edit-modal").val().trim();
  paramEmployeeObj.photo = $("#inp-photo-edit-modal").val().split('\\').pop();
  paramEmployeeObj.notes = $("#textarea-notes-edit-modal").val().trim();
  paramEmployeeObj.reportsTo = $("#select-reportsto-edit-modal").val();
  paramEmployeeObj.username = $("#inp-username-edit-modal").val().trim();
  paramEmployeeObj.password = $("#inp-password-edit-modal").val().trim();
  paramEmployeeObj.email = $("#inp-email-edit-modal").val().trim();
  paramEmployeeObj.activated = $("#select-activated-edit-modal").val();
  paramEmployeeObj.profile = $("#textarea-profiles-edit-modal").val().trim();
  paramEmployeeObj.userLevel = $("#select-user-level-edit-modal").val();
}
//Hàm kiểm tra dữ liệu Employee
function validateEmployeeData(paramEmployeeObj) {
  let vResult = true;
  if (paramEmployeeObj.firstname == "") {
    alert("First Name chưa được nhập!");
    vResult = false;
  }
  else if (paramEmployeeObj.lastname == "") {
    alert("Last Name chưa được nhập!");
    vResult = false;
  }
  else if (paramEmployeeObj.title == "") {
    alert("Title chưa được nhập!");
    vResult = false;
  }
  else if (paramEmployeeObj.titleOfCourtesy == "") {
    alert("Title of Courtesy chưa được nhập!");
    vResult = false;
  }
  else if (paramEmployeeObj.birthDate == "") {
    alert("Birth Date chưa được nhập!");
    vResult = false;
  }
  else if (paramEmployeeObj.hireDate == "") {
    alert("Hire Date chưa được nhập!");
    vResult = false;
  }
  else if (paramEmployeeObj.region == "") {
    alert("Region chưa được nhập!");
    vResult = false;
  }
  else if (paramEmployeeObj.country == "") {
    alert("Country chưa được nhập!");
    vResult = false;
  }
  else if (paramEmployeeObj.username == "") {
    alert("Username chưa được nhập!");
    vResult = false;
  }
  else if (paramEmployeeObj.password == "") {
    alert("Password chưa được nhập!");
    vResult = false;
  }
  else if (paramEmployeeObj.email == "") {
    alert("Email chưa được nhập!");
    vResult = false;
  }
  else if (paramEmployeeObj.userLevel == -1) {
    alert("User Level chưa được nhập!");
    vResult = false;
  }
  return vResult;
}
//Hàm yêu câu Get Employee By Id từ server
function getEmployeeById(paramEmployeeId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/employees/" + paramEmployeeId,
    dataType: "json",
    type: "GET",
    headers: gHeader,
    success: function (res) {
      console.log(res);
      loadDataEmployeeToForm(res);
    },
    error: function (err) {
      Toast.fire({
        icon: 'error',
        title: 'Get Employee Failed: ' + err.responseJSON.error
      })
    }
  })
}
//Hàm load dữ liệu lên Form
function loadDataEmployeeToForm(paramEmployeeData) {
  "use strict"
  $("#inp-firstname-edit-modal").val(paramEmployeeData.firstname);
  $("#inp-lastname-edit-modal").val(paramEmployeeData.lastname);
  $("#inp-title-edit-modal").val(paramEmployeeData.title);
  $("#inp-courtesy-edit-modal").val(paramEmployeeData.titleOfCourtesy);
  $("#inp-birthdate-edit-modal").val(paramEmployeeData.birthDate);
  $("#inp-hiredate-edit-modal").val(paramEmployeeData.hireDate);
  $("#inp-address-edit-modal").val(paramEmployeeData.address);
  $("#inp-city-edit-modal").val(paramEmployeeData.city);
  $("#inp-region-edit-modal").val(paramEmployeeData.region);
  $("#inp-postalcode-edit-modal").val(paramEmployeeData.postalCode);
  $("#inp-country-edit-modal").val(paramEmployeeData.country);
  $("#inp-homephone-edit-modal").val(paramEmployeeData.homePhone);
  $("#inp-extension-edit-modal").val(paramEmployeeData.extension);
  // $("#inp-photo").attr("value",paramEmployeeData.photo);
  $("#textarea-notes-edit-modal").val(paramEmployeeData.notes);
  $("#select-reportsto-edit-modal").val(paramEmployeeData.reportsTo).change();
  $("#inp-username-edit-modal").val(paramEmployeeData.username);
  $("#inp-password-edit-modal").val(paramEmployeeData.password);
  $("#inp-email-edit-modal").val(paramEmployeeData.email);
  $("#select-activated-edit-modal").val(paramEmployeeData.activated).change();
  $("#textarea-profiles-edit-modal").val(paramEmployeeData.profile);
  $("#select-user-level-edit-modal").val(paramEmployeeData.userLevel).change();
}
//Hàm thay đổi màu hàng
function changeColorRow(paramRow) {
  $("#table-employee tbody tr").removeClass("table-primary");
  $(paramRow).addClass("table-primary");

}
//Hàm xử lý nút Update Employee
function onBtnUpdateEmployeeClick(event) {
  "use strict"
  console.log("Nút Update được ấn")
  let vEmployeeObj = {
    firstname: "",
    lastname: "",
    title: "",
    titleOfCourtesy: "",
    birthDate: "",
    hireDate: "",
    address: "",
    city: "",
    region: "",
    postalCode: "",
    country: "",
    homePhone: "",
    extension: "",
    photo: "",
    notes: "",
    reportsTo: "",
    username: "",
    password: "",
    email: "",
    activated: "",
    profile: "",
    userLevel: ""
  }
  var formData = new FormData(event);
  getDataByFormEditModal(vEmployeeObj);
  formData.append('employee', JSON.stringify(vEmployeeObj))
  if ($('#inp-photo-edit-modal')[0].files[0]) {
    formData.append('image', $('#inp-photo-edit-modal')[0].files[0]);
  } else {
    const defaultImage = new Blob([''], { type: 'image/jpeg' }); //Chuyển 
    formData.append('image', defaultImage);
  }
  let vCheck = validateEmployeeData(vEmployeeObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/employees/" + gEmployeeId,
      type: "PUT",
      data: formData,
      processData: false,
      contentType: false,
      headers: gHeader,
      success: function (res) {
        console.log(res);
        gSTT = 1;
        getAllEmployee();
        loadDataToTable(gEmployeeList);
        gEmployeeId = "";
        Toast.fire({
          icon: 'success',
          title: 'Update Employee Success!!!'
        })
        $("#edit-employee-modal").modal("hide");
      },
      error: function (err) {
        Toast.fire({
          icon: 'error',
          title: 'Update Employee Failed: ' + err.responseJSON.error
        })
      }
    })
  }
}
//Hàm xử lý nút Xác nhận xóa Employee
function onBtnConfirmDeleteClick() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/employees/" + gEmployeeId,
    type: "DELETE",
    headers: gHeader,
    success: function (res) {
      gSTT = 1;
      getAllEmployee();
      loadDataToTable(gEmployeeList);
      gEmployeeId = "";
      Toast.fire({
        icon: 'success',
        title: 'Delete Employee Success!!!'
      })
    },
    error: function (err) {
      Toast.fire({
        icon: 'error',
        title: 'Delete Employee Failed: ' + err.responseJSON.error
      })
    }
  })
  refeshForm();
}
//Clear Form
function refeshForm() {
  $(document).find("#add-employee-modal input").val("");
  $(document).find("#add-employee-modal textarea").val("");
  $(document).find("#add-employee-modal select").val(-1).change();
}





















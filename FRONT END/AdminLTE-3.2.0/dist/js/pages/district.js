"use strict"
const gBASE_URL = "http://localhost:8080"
var gNameCol = ["id","countEstate", "name", "prefix", "action"];
var gDistrictList = [];
var gDistrictId = "";
const gCOL_ID = 0;
const gCOL_REAL_ESTATE = 1;
const gCOL_NAME = 2;
const gCOL_PREFIX = 3;
const gCOL_ACTION = 4;
var gTableDistrict = $("#table-district").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_REAL_ESTATE] },
    { data: gNameCol[gCOL_NAME] },
    { data: gNameCol[gCOL_PREFIX] },
    { data: gNameCol[gCOL_ACTION] }
  ],
  columnDefs: [
    {
      targets: gCOL_ACTION,
      defaultContent: `<i class="far fa-edit btn-edit text-primary fa-xl"></i> &nbsp <i class="fas fa-trash-alt btn-delete text-danger fa-xl"></i>`,
    },
    {
      targets: gCOL_REAL_ESTATE,
      render: function (data) {
        let vHTML = `
          <a class="btn btn-primary btn-estate">Real Estate <span class="badge badge-light">${data}</span></a> 
          `
        return vHTML;
      }
    },
  ],
  autoWidth: false
})
$(document).ready(function () {
  getAllDistrict();
  getAllProvince();
})
$("#btn-add-district").on("click", function () {
  $("#add-district-modal").modal("show");
})
$("#btn-confirm-add-district").on("click", function () {
  onBtnConfirmAddDistrict()
})
$("#table-district tbody").on("click", ".btn-edit", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableDistrict.row(vRowClick).data();
  // console.log(vRowData);
  gDistrictId = vRowData.id;
  $("#edit-district-modal").modal("show");
  getDistrictById(gDistrictId);
})
$("#btn-confirm-edit-district").on("click", function () {
  onBtnConfirmEditDistrict()
})
$("#table-district tbody").on("click", ".btn-delete", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableDistrict.row(vRowClick).data();
  console.log(vRowData);
  gDistrictId = vRowData.id;
  $("#delete-district-modal").modal("show");
})
$("#btn-confirm-delete-district").on("click", function () {
  onBtnConfirmDeleteDistrict()
})
$(document).on("click",".btn-estate", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableDistrict.row(vRowClick).data();
  gDistrictId = vRowData.id;
  window.location.href = "RealEstateRegion.html?districtId=" + gDistrictId;
})
function getAllDistrict() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/districts/count",
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      gDistrictList = res;
      loadDataToTable(gDistrictList);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
function loadDataToTable(pDistrictList) {
  "use strict"
  gTableDistrict.clear();
  gTableDistrict.rows.add(pDistrictList);
  gTableDistrict.draw();
}
//Hàm xư lý nút xác nhận thêm District
function onBtnConfirmAddDistrict() {
  let vDistrictObj = {
    name: "",
    prefix: ""
  }
  getDataByFormAddModal(vDistrictObj);
  var vCheck = validateData(vDistrictObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/districts",
      type: "POST",
      data: JSON.stringify(vDistrictObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      headers: gHeader,
      success: function (res) {
        console.log("Thêm District thành công");
        getAllDistrict();
        loadDataToTable(gDistrictList);
        $("#add-district-modal").modal("hide");
      },
      error: function (err) {
        alert("Thêm District thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận chỉnh sửa District
function onBtnConfirmEditDistrict() {
  let vDistrictObj = {
    name: "",
    prefix: ""
  }
  getDataByFormEditModal(vDistrictObj);
  var vCheck = validateData(vDistrictObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/" + gDistrictId,
      type: "PUT",
      data: JSON.stringify(vDistrictObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      headers: gHeader,
      success: function (res) {
        console.log("Chỉnh sửa District thành công");
        getAllDistrict();
        loadDataToTable(gDistrictList);
        $("#edit-district-modal").modal("hide");
      },
      error: function (err) {
        alert("Chỉnh sửa District thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận delete District
function onBtnConfirmDeleteDistrict() {
  $.ajax({
    url: gBASE_URL + "/districts/" + gDistrictId,
    type: "DELETE",
    headers: gHeader,
    success: function () {
      console.log("Xóa District thành công");
      getAllDistrict();
      loadDataToTable(gDistrictList);
      $("#delete-district-modal").modal("hide");
    },
    error: function (err) {
      alert("Xóa District thất bại" + err.responseText);
    }
  })
}
//Hàm truy xuất dữ liệu form Edit Modal
function getDataByFormEditModal(paramObj) {
  paramObj.name = $("#inp-name-edit-modal").val();
  paramObj.prefix = $("#select-prefix-edit-modal").val();
}
//Hàm truy xuất dữ liệu form Add Modal
function getDataByFormAddModal(paramObj) {
  paramObj.name = $("#inp-name-add-modal").val();
  paramObj.prefix = $("#select-prefix-add-modal").val();
}

function validateData(paramObj) {
  let vResult = true;
  if (paramObj.name == "") {
    alert("Chưa nhập tên quận huyện!");
    vResult = false;
  }
  else if (paramObj.prefix == "") {
    alert("Chưa nhập prefix quận huyện!");
    vResult = false;
  }
  else if (paramObj.province == "") {
    alert("Chưa chọn Province!");
    vResult = false;
  }
  return vResult;
}
//Hàm yêu cầu ajax get District by ID
function getDistrictById(paramId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/districts/" + paramId,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      $("#inp-name-edit-modal").val(res.name);
      $("#select-prefix-edit-modal").val(res.prefix).change();
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
//Hàm ajax lấy dữ liệu Province
function getAllProvince() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/provinces",
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      res.forEach(element => {
        $("#select-province-add-modal").append($("<option>", {
          value: element.code,
          text: element.name,
        }));
        $("#select-province-edit-modal").append($("<option>", {
          value: element.code,
          text: element.name,
        }));
      });
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
// function getProvinceByCode(paramCode) {
//   "use strict"
//   $.ajax({
//     url: gBASE_URL + "/provinces?code=" + paramCode,
//     dataType: "json",
//     type: "GET",
//     success: function (res) {
//       console.log(res);
//       return res;
//     },
//     error: function (err) {
//       alert(err.responseText);
//     }
//   });
// }
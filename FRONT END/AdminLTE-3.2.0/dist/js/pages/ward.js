"use strict"
const gBASE_URL = "http://localhost:8080/wards"
var gNameCol = ["id", "countEstate", "name", "prefix", "action"];
var gWardList = [];
var gWardId = "";
const gCOL_ID = 0;
const gCOL_REAL_ESTATE = 1;
const gCOL_NAME = 2;
const gCOL_PREFIX = 3;
const gCOL_ACTION = 4;
var gTableWard = $("#table-ward").DataTable({
    columns: [
        { data: gNameCol[gCOL_ID] },
        { data: gNameCol[gCOL_REAL_ESTATE] },
        { data: gNameCol[gCOL_NAME] },
        { data: gNameCol[gCOL_PREFIX] },
        { data: gNameCol[gCOL_ACTION] }
    ],
    columnDefs: [
        {
            targets: gCOL_ACTION,
            defaultContent: `<i class="far fa-edit btn-edit text-primary fa-xl"></i> &nbsp <i class="fas fa-trash-alt btn-delete text-danger fa-xl"></i>`,
        },
        {
            targets: gCOL_REAL_ESTATE,
            render: function (data) {
                let vHTML = `
                <a class="btn btn-primary btn-estate">Real Estate <span class="badge badge-light">${data}</span></a> 
               `
                return vHTML;
            }
        },
    ],
    autoWidth: false
})
$(document).ready(function () {
    getAllWard();
})
$("#btn-add-ward").on("click", function () {
    $("#add-ward-modal").modal("show");
})
$("#btn-confirm-add-ward").on("click", function () {
    onBtnConfirmAddWard()
})
$("#table-ward tbody").on("click", ".btn-edit", function () {
    let vRowClick = $(this).closest("tr");
    let vRowData = gTableWard.row(vRowClick).data();
    // console.log(vRowData);
    gWardId = vRowData.id;
    $("#edit-ward-modal").modal("show");
    getWardById(gWardId);
})
$("#btn-confirm-edit-ward").on("click", function () {
    onBtnConfirmEditWard()
})
$("#table-ward tbody").on("click", ".btn-delete", function () {
    let vRowClick = $(this).closest("tr");
    let vRowData = gTableWard.row(vRowClick).data();
    console.log(vRowData);
    gWardId = vRowData.id;
    $("#delete-ward-modal").modal("show");
})
$("#btn-confirm-delete-ward").on("click", function () {
    onBtnConfirmDeleteWard()
})
$(document).on("click", ".btn-estate", function () {
    let vRowClick = $(this).closest("tr");
    let vRowData = gTableWard.row(vRowClick).data();
    gWardId = vRowData.id;
    window.location.href = "RealEstateRegion.html?wardId=" + gWardId;
})
$(document).on("click", ".btn-project", function () {
    let vRowClick = $(this).closest("tr");
    let vRowData = gTableWard.row(vRowClick).data();
    gWardId = vRowData.id;
    window.location.href = "RealEstateRegion.html?wardId=" + gWardId;
})
function getAllWard() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/count",
        dataType: "json",
        type: "GET",
        success: function (res) {
            console.log(res);
            gWardList = res;
            loadDataToTable(gWardList);
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
function loadDataToTable(pWardList) {
    "use strict"
    gTableWard.clear();
    gTableWard.rows.add(pWardList);
    gTableWard.draw();
}
//Hàm xư lý nút xác nhận thêm Ward
function onBtnConfirmAddWard() {
    let vWardObj = {
        name: "",
        prefix: ""
    }
    getDataByFormAddModal(vWardObj);
    var vCheck = validateData(vWardObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            data: JSON.stringify(vWardObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log("Thêm Ward thành công");
                getAllWard();
                loadDataToTable(gWardList);
                $("#add-ward-modal").modal("hide");
            },
            error: function (err) {
                alert("Thêm Ward thất bại" + err.responseText);
            }
        })
    }
}
//Hàm xử lý nút xác nhận chỉnh sửa Ward
function onBtnConfirmEditWard() {
    let vWardObj = {
        name: "",
        prefix: ""
    }
    getDataByFormEditModal(vWardObj);
    var vCheck = validateData(vWardObj);
    if (vCheck = true) {
        $.ajax({
            url: gBASE_URL + "/" + gWardId,
            type: "PUT",
            data: JSON.stringify(vWardObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log("Chỉnh sửa Ward thành công");
                getAllWard();
                loadDataToTable(gWardList);
                $("#edit-ward-modal").modal("hide");
            },
            error: function (err) {
                alert("Chỉnh sửa Ward thất bại" + err.responseText);
            }
        })
    }
}
//Hàm xử lý nút xác nhận delete Ward
function onBtnConfirmDeleteWard() {
    $.ajax({
        url: gBASE_URL + "/" + gWardId,
        type: "DELETE",
        success: function () {
            console.log("Xóa Ward thành công");
            getAllWard();
            loadDataToTable(gWardList);
            $("#delete-ward-modal").modal("hide");
            Toast.fire({
                icon: 'success',
                title: 'Delete Ward Success!!!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: 'Delete Ward Failed!!!'
            })
        }
    })
}
//Hàm truy xuất dữ liệu form Edit Modal
function getDataByFormEditModal(paramObj) {
    paramObj.name = $("#inp-name-edit-modal").val();
    paramObj.prefix = $("#select-prefix-edit-modal").val();
}
//Hàm truy xuất dữ liệu form Add Modal
function getDataByFormAddModal(paramObj) {
    paramObj.name = $("#inp-name-add-modal").val();
    paramObj.prefix = $("#select-prefix-add-modal").val();
}

function validateData(paramObj) {
    let vResult = true;
    if (paramObj.name == "") {
        alert("Chưa nhập tên phường xã!");
        vResult = false;
    }
    else if (paramObj.prefix == "") {
        alert("Chưa nhập prefix phường xã!");
        vResult = false;
    }
    return vResult;
}
function getWardById(paramId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/" + paramId,
        dataType: "json",
        type: "GET",
        success: function (res) {
            console.log(res);
            $("#inp-name-edit-modal").val(res.name);
            $("#select-prefix-edit-modal").val(res.prefix);
            Toast.fire({
                icon: 'success',
                title: 'Get Ward Success!!!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: 'Get Ward Failed!!!'
            })
        }
    })
}
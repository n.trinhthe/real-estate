"use strict"
var gEstateList = "";
var gProvinceList = "";
var gDistrictList = "";
var gWardList = "";
var gStreetList = "";
var gCustomerList = "";
var gProjectList = "";
var gProvinceId = "";
var gDistrictId = "";
var gWardId = "";
var gStreetId = "";
var gTotalPages = "";
// var gDesignUnitList = "";
var gSTT = 1;
var gEstateId = -1;
//Định nghĩa bảng
var gNameCol = ["id", "censorred", "title", "type", "request", "provinceId", "districtId", "wardId", "streetId", "address",
    "projectId", "customerId", "price", "priceMin", "priceTime", "dateCreate", "acreage", "direction", "totalFloors",
    "numberFloors", "bath", "bedroom", "balcony", "landscapeView", "apartCode", "apartType", "apartLoca", "wallArea",
    "furnitureType", "priceRent", "returnRate", "legalDoc", "description", "widthY", "longX", "streetHouse", "fsbo", "viewNum",
    "createBy", "updateBy", "shape", "distance2facade", "adjacentFacadeNum", "adjacentRoad", "alleyMinWidth",
    "adjacentAlleyMinWidth", "factor", "structure", "dtsxd", "clcl", "ctxdprice", "ctxdvalue", "photo", "lat", "lng"];
const gCOL_ID = 0;
const gCOL_CENSORRED = 1;
const gCOL_TITLE = 2;
const gCOL_TYPE = 3;
const gCOL_REQUEST = 4;
const gCOL_PROVINCE = 5;
const gCOL_DISTRICT = 6;
const gCOL_WARD = 7;
const gCOL_STREET = 8;
const gCOL_ADDRESS = 9;
const gCOL_PROJECT = 10;
const gCOL_CUSTOMER = 11;
const gCOL_PRICE = 12;
const gCOL_PRICE_MIN = 13;
const gCOL_PRICE_TIME = 14;
const gCOL_DATE_CREATE = 15;
const gCOL_ACREAGE = 16;
const gCOL_DIRECTION = 17;
const gCOL_TOTAL_FLOORS = 18;
const gCOL_NUMBER_FLOORS = 19;
const gCOL_BATH = 20;
const gCOL_BEDROOM = 21;
const gCOL_BALCONY = 22;
const gCOL_LANSCAPVIEW = 23;
const gCOL_APART_CODE = 24;
const gCOL_APART_TYPE = 25;
const gCOL_APART_LOCA = 26;
const gCOL_WALL_AREA = 27;
const gCOL_FURNITURE = 28;
const gCOL_PRICE_RENT = 29;
const gCOL_RETURN_RATE = 30;
const gCOL_LEGAL_DOC = 31
const gCOL_DESCRIPTION = 32;
const gCOL_WIDTH = 33;
const gCOL_LONG = 34;
const gCOL_STREET_HOUSE = 35;
const gCOL_FSBO = 36;
const gCOL_VIEW_NUM = 37;
const gCOL_CREATE_BY = 38;
const gCOL_UPDATE_BY = 39;
const gCOL_SHAPE = 40;
const gCOL_DISTANCE_FACADE = 41;
const gCOL_ADJACENT_FACADE = 42;
const gCOL_ADJACENT_ROAD = 43;
const gCOL_ALLEY = 44;
const gCOL_ADJACENT_ALLEY = 45;
const gCOL_FACTOR = 46;
const gCOL_STRUCTURE = 47;
const gCOL_DTSXD = 48;
const gCOL_CLCL = 49;
const gCOL_CTXD_PRICE = 50;
const gCOL_CTXD_VALUE = 51;
const gCOL_PHOTO = 52;
const gCOL_LAT = 53;
const gCOL_LNG = 54;
const gCOL_ACTION = 55;

var gTableEstate = $("#table-estate").DataTable({
    columns: [
        { data: gNameCol[gCOL_ID] },
        { data: gNameCol[gCOL_CENSORRED] },
        { data: gNameCol[gCOL_TITLE] },
        { data: gNameCol[gCOL_TYPE] },
        { data: gNameCol[gCOL_REQUEST] },
        { data: gNameCol[gCOL_PROVINCE] },
        { data: gNameCol[gCOL_DISTRICT] },
        { data: gNameCol[gCOL_WARD] },
        { data: gNameCol[gCOL_STREET] },
        { data: gNameCol[gCOL_ADDRESS] },
        { data: gNameCol[gCOL_PROJECT] },
        { data: gNameCol[gCOL_CUSTOMER] },
        { data: gNameCol[gCOL_PRICE] },
        { data: gNameCol[gCOL_PRICE_MIN] },
        { data: gNameCol[gCOL_PRICE_TIME] },
        { data: gNameCol[gCOL_DATE_CREATE] },
        { data: gNameCol[gCOL_ACREAGE] },
        { data: gNameCol[gCOL_DIRECTION] },
        { data: gNameCol[gCOL_TOTAL_FLOORS] },
        { data: gNameCol[gCOL_NUMBER_FLOORS] },
        { data: gNameCol[gCOL_BATH] },
        { data: gNameCol[gCOL_BEDROOM] },
        { data: gNameCol[gCOL_BALCONY] },
        { data: gNameCol[gCOL_LANSCAPVIEW] },
        { data: gNameCol[gCOL_APART_CODE] },
        { data: gNameCol[gCOL_APART_TYPE] },
        { data: gNameCol[gCOL_APART_LOCA] },
        { data: gNameCol[gCOL_WALL_AREA] },
        { data: gNameCol[gCOL_FURNITURE] },
        { data: gNameCol[gCOL_PRICE_RENT] },
        { data: gNameCol[gCOL_RETURN_RATE] },
        { data: gNameCol[gCOL_LEGAL_DOC] },
        { data: gNameCol[gCOL_DESCRIPTION] },
        { data: gNameCol[gCOL_WIDTH] },
        { data: gNameCol[gCOL_LONG] },
        { data: gNameCol[gCOL_STREET_HOUSE] },
        { data: gNameCol[gCOL_FSBO] },
        { data: gNameCol[gCOL_VIEW_NUM] },
        { data: gNameCol[gCOL_CREATE_BY] },
        { data: gNameCol[gCOL_UPDATE_BY] },
        { data: gNameCol[gCOL_SHAPE] },
        { data: gNameCol[gCOL_DISTANCE_FACADE] },
        { data: gNameCol[gCOL_ADJACENT_FACADE] },
        { data: gNameCol[gCOL_ADJACENT_ROAD] },
        { data: gNameCol[gCOL_ALLEY] },
        { data: gNameCol[gCOL_ADJACENT_ALLEY] },
        { data: gNameCol[gCOL_FACTOR] },
        { data: gNameCol[gCOL_STRUCTURE] },
        { data: gNameCol[gCOL_DTSXD] },
        { data: gNameCol[gCOL_CLCL] },
        { data: gNameCol[gCOL_CTXD_PRICE] },
        { data: gNameCol[gCOL_CTXD_VALUE] },
        { data: gNameCol[gCOL_PHOTO] },
        { data: gNameCol[gCOL_LAT] },
        { data: gNameCol[gCOL_LNG] },
        { data: gNameCol[gCOL_ACTION] },
    ],
    columnDefs: [
        {
            targets: gCOL_ID,
            render: function (data) {
                return `
                <div class="text-center">
                    ${data}<br>
                    <i data-id="${data}" class="fas fa-search text-primary btn-detail " style="cursor: pointer"></i>
                    <i data-id="${data}" class="far fa-edit text-primary btn-edit" style="cursor: pointer"></i>
                    <i data-id="${data}" class="fas fa-trash text-primary btn-delete" style="cursor: pointer"></i>
                </div>
                `
            },
            width: "5px"
        },
        {
            targets: gCOL_TYPE,
            render: function (data) {
                let vTypeName = new String();
                if (data == 1) {
                    vTypeName = "Nhà ở";
                }
                else if (data == 2) {
                    vTypeName = "Căn hộ/ Chung cư";
                }
                else if (data == 3) {
                    vTypeName = "Văn phòng";
                }
                else if (data == 4) {
                    vTypeName = "Kinh doanh";
                }
                else if (data == 5) {
                    vTypeName = "Phòng trọ";
                }
                else if (data == 6) {
                    vTypeName = "Đất nền";
                }
                return vTypeName;
            }
        },
        {
            targets: gCOL_REQUEST,
            render: function (data) {
                let requestName = "";
                if (data == 1) {
                    requestName = "Bán";
                }
                else if (data == 2) {
                    requestName = "Mua";
                }
                else if (data == 3) {
                    requestName = "Cho thuê";
                }
                else if (data == 4) {
                    requestName = "Thuê";
                }
                return requestName;
            }
        },
        {
            targets: gCOL_PROVINCE,
            render: function (data) {
                if (data == null || data < 1) {
                    return "";
                }
                else {
                    let vProvince = gProvinceList.filter(e => e.id == data)
                    return vProvince[0].name;
                }
            }
        },
        {
            targets: gCOL_WARD,
            render: function (data) {
                if (data == null || data < 0) {
                    return ""
                }
                else {
                    let vWard = gWardList.filter(e => e.id == data)
                    return vWard[0].name;
                }
            }
        },
        {
            targets: gCOL_STREET,
            render: function (data) {
                if (data == null || data < 0) {
                    return ""
                }
                else {
                    let vStreet = gStreetList.filter(e => e.id == data)
                    return vStreet[0].name;
                }
            }
        },
        {
            targets: gCOL_DIRECTION,
            render: function (data) {
                let vDirection = "";
                switch (data) {
                    case 1:
                        vDirection = "West";
                        break;
                    case 2:
                        vDirection = "North";
                        break;
                    case 3:
                        vDirection = "East";
                        break;
                    case 4:
                        vDirection = "South";
                        break;
                    case 5:
                        vDirection = "South-West";
                        break;
                    case 6:
                        vDirection = "North-East";
                        break;
                    case 7:
                        vDirection = "East-South";
                        break;
                    case 8:
                        vDirection = "North-West";
                        break;
                }
                return vDirection;
            }
        },
        {
            targets: gCOL_DISTRICT,
            render: function (data) {
                if (data == null || data < 0) {
                    return ""
                }
                else {
                    let vDistrict = gDistrictList.filter(e => e.id == data)
                    return vDistrict[0].name;
                }
            }
        },
        {
            targets: gCOL_CUSTOMER,
            render: function (data) {
                let vCustomer = gCustomerList.filter(e => e.id == data)
                return vCustomer == "" ? "" : vCustomer[0].contactName + " - " + vCustomer[0].mobile;
            }
        },
    {
        targets: gCOL_PROJECT,
        render: function (data) {
            if (data == null || data < 0) {
                return ""
            }
            else {
                let vProject = gProjectList.filter(e => e.id == data)
                console.log()
                return vProject[0].name;
            }
        }
    },
    {
        targets: gCOL_PHOTO,
        render: function (data) {
            return (data != null && data != "") ? `<img src="${gBASE_URL}/images/${data}" style="height: 100px; width: 100px"></img>` : `<img src="dist/picture/placeholder.jpg" style="height: 100px; width: 100px"></img>`;

        }
    },
    {
        targets: gCOL_CENSORRED,
        render: function (data) {
            if (data == true) {
                return `<i class="fas fa-check text-success"></i>`
            }
            else {
                return `<i class="fas fa-times text-danger"></i>`
            }
        },
        className: "dt-center"
    },
    {
        className: 'dtr-control',
        orderable: false,
        targets: -1
    },
    {
        targets: gCOL_ACTION,
        defaultContent: ``
    },
    ],
// dom: 'rtip',
responsive: {
    details: {
        type: 'column',
            target: -1
    }
},
buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    stateSave: true,
        stateLoadCallback: function (settings) {
            return JSON.parse(localStorage.getItem("DataTables_table-estate_/RealEstateRegion.html"));
        },
searching: false,
    autoWidth: false,
        paging: false,
            info: false,
})

$(document).ready(function () {
    getAllEstateByRegion()
    getCustomerByEmployee();
    getAllProject();
    getAllProvince();
    getAllDistrict();
    getAllWard();
    getAllStreet();
    loadDataToTable(gEstateList);
})
$("#select-province").on("change", function () { //Thay đổi Tỉnh thành
    onChangeProvince($(this).val());
})

$("#select-district").on("change", function () { //Thay đổi quận huyện
    onChangeDistrict($(this).val());
})
$("#table-estate tbody").on("click", "tr.odd,tr.even", function () { //Click dòng
    changeColorRow(this);
})
$(document).on("click", ".btn-detail", function () { //Nút hiện thị Detail Modal
    gEstateId = $(this).data("id");
    getEstateById(gEstateId);
    $("#edit-detail-estate-modal").modal("show");
    $("#edit-detail-estate-modal .btn-confirm").hide();
})
$(document).on("click", ".btn-edit", function () { //Nút hiện thị Edit Modal
    gEstateId = $(this).data("id");
    getEstateById(gEstateId);
    $("#edit-detail-estate-modal .btn-confirm").show();
    $("#edit-detail-estate-modal").modal("show");
})
$("#form-update-realestate").submit(function (event) { //Nút Update Estate
    event.preventDefault();
    onBtnUpdateEstateClick(this);
})
$(document).on("click", ".btn-delete", function () { //Nút hiện thị Delete Modal
    gEstateId = $(this).data("id");
    getEstateById(gEstateId);
    $("#delete-estate-modal").modal("show");
})
$("#delete-estate-modal .btn-confirm").on("click", function () { //Nút Xác Nhận Delete Estate
    onBtnConfirmDeleteClick();
    $("#delete-estate-modal").modal("hide");
})
$("#btn-customer-modal").on("click", function () { //Nút hiện thị modal add customer
    $("#add-customer-modal").modal("show");
})
$("#btn-add-customer").on("click", function () { //Nút xác nhận add customer
    onBtnAddCustomerClick()
})
//Hàm lấy dữ liệu theo Region URL
function getAllEstateByRegion(){
    let vURLString = window.location.href;
    console.log(vURLString);
    if (vURLString.indexOf("provinceId") != -1) {
        let vProvinceId = new URL(vURLString).searchParams.get("provinceId")
        gProvinceId = vProvinceId;
        console.log(vProvinceId);
        getEstateByProvinceId(vProvinceId, 0, 10);
    }
    if (vURLString.indexOf("districtId") != -1) {
        let vDistrictId = new URL(vURLString).searchParams.get("districtId")
        gDistrictId = vDistrictId;
        console.log(vDistrictId)
        getEstateByDistrictId(gDistrictId, 0, 10);
    }
    if (vURLString.indexOf("wardId") != -1) {
        let vWardId = new URL(vURLString).searchParams.get("wardId")
        gWardId = vWardId;
        console.log(vWardId)
        getEstateByWardId(vWardId, 0, 10);
    }
}
//Hàm yêu cầu lấy dữ liệu All Estate By Province từ server
function getEstateByProvinceId(pProvinceId, pPage, pSize) {
    "use strict"
    if (pProvinceId != null & pProvinceId != "") {
        $.ajax({
            url: gBASE_URL + "/realestates?provinceId=" + pProvinceId
                + "&page=" + pPage + "&size=" + pSize,
            dataType: "json",
            type: "GET",
            async: false,
            headers: gHeader,
            async: false,
            success: function (res) {
                gEstateList = res.content;
                gTotalPages = res.totalPages;
                pagination(pPage + 1, gTotalPages);
            },
            error: function (err) {
                alert(err.responseText);
            }
        })
    }
}
//Hàm yêu cầu lấy dữ liệu All Estate By District từ server
function getEstateByDistrictId(pDistrictId, pPage, pSize) {
    "use strict"
    if (pDistrictId != null & pDistrictId != "") {
        $.ajax({
            url: gBASE_URL + "/realestates?districtId=" + pDistrictId
                + "&page=" + pPage + "&size=" + pSize,
            dataType: "json",
            type: "GET",
            async: false,
            headers: gHeader,
            async: false,
            success: function (res) {
                gEstateList = res.content;
                gTotalPages = res.totalPages;
                pagination(pPage + 1, gTotalPages);
            },
            error: function (err) {
                alert(err.responseText);
            }
        })
    }
}
//Hàm yêu cầu lấy dữ liệu All Estate By District từ server
function getEstateByWardId(pWardId, pPage, pSize) {
    "use strict"
    if (pWardId != null & pWardId != "") {
        $.ajax({
            url: gBASE_URL + "/realestates?wardId=" + pWardId
                + "&page=" + pPage + "&size=" + pSize,
            dataType: "json",
            type: "GET",
            async: false,
            headers: gHeader,
            async: false,
            success: function (res) {
                gEstateList = res.content;
                gTotalPages = res.totalPages;
                pagination(pPage + 1, gTotalPages);
                console.log(gEstateList);
            },
            error: function (err) {
                alert(err.responseText);
            }
        })
    }
}
//Hàm ajax lấy dữ liệu Investor
function getCustomerByEmployee() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/employees/" + gInfo.userId + "/customers",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        async: false,
        success: function (res) {
            console.log(res);
            gCustomerList = res;
            res.forEach(element => {
                $("#select-customer").append($("<option>", {
                    value: element.id,
                    text: element.contactName + " - " + element.mobile,
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu Project
function getAllProject() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/projects",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        async: false,
        success: function (res) {
            console.log(res);
            gProjectList = res;
            res.forEach(element => {
                $("#select-project").append($("<option>", {
                    value: element.id,
                    text: element.name,
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu Street
function getAllStreet() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/streets",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        async: false,
        success: function (res) {
            gStreetList = res;
            console.log(res);
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm yêu câu Get Estate By Id từ server
function getEstateById(paramEstateId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/realestates/" + paramEstateId,
        dataType: "json",
        type: "GET",
        headers: gHeader,
        async: false,
        success: function (res) {
            console.log(res);
            loadDataEstateToForm(res);
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm load dữ liệu lên Form
function loadDataEstateToForm(paramEstateObj) {
    "use strict"
    $("#inp-title").val(paramEstateObj.title);
    $("#select-type").val(paramEstateObj.type).change();
    $("#select-request").val(paramEstateObj.request).change();
    $("#select-province").val(paramEstateObj.provinceId).change();
    $("#select-district").val(paramEstateObj.districtId).change();
    $("#select-ward").val(paramEstateObj.wardId).change();
    $("#select-street").val(paramEstateObj.streetId).change();
    $("#inp-address").val(paramEstateObj.address);
    $("#select-project").val(paramEstateObj.projectId).change();
    $("#select-customer").val(paramEstateObj.customerId).change();
    $("#inp-price").val(paramEstateObj.price);
    $("#inp-price-min").val(paramEstateObj.priceMin);
    $("#inp-price-time").val(paramEstateObj.priceTime);
    $("#inp-num-floors").val(paramEstateObj.dateCreate);
    $("#inp-acreage").val(paramEstateObj.acreage);
    $("#select-direction").val(paramEstateObj.direction);
    $("#inp-total-floors").val(paramEstateObj.totalFloors);
    $("#inp-bath").val(paramEstateObj.bath);
    $("#inp-bedroom").val(paramEstateObj.bedroom);
    $("#inp-balcony").val(paramEstateObj.balcony);
    $("#inp-landscape").val(paramEstateObj.landscapeView);
    $("#inp-apartment-code").val(paramEstateObj.apartCode);
    $("#inp-apartment-type").val(paramEstateObj.apartType);
    $("#inp-apartment-location").val(paramEstateObj.apartLoca);
    $("#inp-inp-wallarea").val(paramEstateObj.wallArea);//
    $("#select-furniture").val(paramEstateObj.furnitureType).change();
    $("#inp-price-rent").val(paramEstateObj.priceRent);
    $("#inp-return-rate").val(paramEstateObj.returnRate);
    $("#inp-legaldoc").val(paramEstateObj.legalDoc);
    $("#inp-region-link").val(paramEstateObj.description);//
    $("#inp-width").val(paramEstateObj.widthY);
    $("#inp-long").val(paramEstateObj.longX);
    $("#inp-street-house").val(paramEstateObj.streetHouse);
    $("#inp-fsbo").val(paramEstateObj.fsbo);
    $("#inp-view-num").val(paramEstateObj.viewNum);
    $("#inp-description").val(paramEstateObj.description);
    $("#inp-region-link").val(paramEstateObj.createBy);//
    $("#inp-region-link").val(paramEstateObj.updateBy);//
    $("#inp-distance-facade").val(paramEstateObj.distance2facade);
    $("#inp-adjacent-facade").val(paramEstateObj.adjacentFacadeNum);
    $("#inp-adjacent-road").val(paramEstateObj.adjacentRoad);
    $("#inp-alley").val(paramEstateObj.alleyMinWidth);
    $("#inp-adjacent-alley").val(paramEstateObj.adjacentAlleyMinWidth);
    $("#inp-factor").val(paramEstateObj.factor);
    $("#inp-structure").val(paramEstateObj.structure);
    $("#inp-dtsxd").val(paramEstateObj.dtsxd);
    $("#inp-clcl").val(paramEstateObj.clcl);
    $("#inp-ctxd-price").val(paramEstateObj.ctxdprice);
    $("#inp-ctxd-value").val(paramEstateObj.ctxdvalue);
    $("#inp-lat").val(paramEstateObj.lat);
    $("#inp-lng").val(paramEstateObj.lng);
    paramEstateObj.censorred ? $("#checkbox-censor").prop('checked', true) : $("#checkbox-censor").prop('checked', false);
}
//Hàm xử lý nút Add Estate
function onBtnAddEstateClick() {
    "use strict"
    let vEstateObj = {
        title: "",
        type: "",
        request: "",
        provinceId: "",
        districtId: "",
        wardId: "",
        streetId: "",
        projectId: "",
        customerId: "",
        address: "",
        price: "",
        priceMin: "",
        priceTime: "",
        acreage: "",
        direction: "",
        totalFloors: "",
        numberFloors: "",
        bath: "",
        apartCode: "",
        wallArea: "",
        bedroom: "",
        balcony: "",
        landscapeView: "",
        apartLoca: "",
        apartType: "",
        furnitureType: "",
        priceRent: "",
        returnRate: "",
        legalDoc: "",
        description: "",
        widthY: "",
        longX: "",
        streetHouse: "",
        viewNum: "",
        createBy: "",
        updateBy: "",
        shape: "",
        distance2facade: "",
        adjacentFacadeNum: "",
        adjacentRoad: "",
        alleyMinWidth: "",
        adjacentAlleyMinWidth: "",
        factor: "",
        structure: "",
        photo: "",
        lat: "",
        lng: "",
        clcl: "",
        ctxdprice: "",
        ctxdvalue: "",
        dtsxd: "",
        fsbo: "",
        censorred: false
    }
    getDataByForm(vEstateObj);
    console.log(vEstateObj);
    let vCheck = validateEstateData(vEstateObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/realestates",
            type: "POST",
            data: JSON.stringify(vEstateObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log(res);
                alert("Add Estate Success!");
                gSTT = 1;
                getAllEstateByRegion();
                loadDataToTable(gEstateList);
                refeshForm();
            },
            error: function (err) {
                alert("Add Estate Failed: " + err.responseText);
            }
        })
    }
}
//Hàm Truy xuất dữ liệu từ form
function getDataByForm(paramEstateObj) {
    "use strict"
    paramEstateObj.title = $("#inp-title").val().trim();
    paramEstateObj.type = $("#select-type").val();
    paramEstateObj.request = $("#select-request").val();
    paramEstateObj.provinceId = $("#select-province").val();
    paramEstateObj.districtId = $("#select-district").val();
    paramEstateObj.wardId = $("#select-ward").val();
    paramEstateObj.streetId = $("#select-street").val();
    paramEstateObj.address = $("#inp-address").val().trim();
    paramEstateObj.projectId = $("#select-project").val();
    paramEstateObj.customerId = $("#select-customer").val();
    paramEstateObj.price = $("#inp-price").val().trim();
    paramEstateObj.priceMin = $("#inp-price-min").val().trim();
    paramEstateObj.priceTime = $("#inp-price-time").val().trim();
    paramEstateObj.acreage = $("#inp-acreage").val().trim();
    paramEstateObj.direction = $("#select-direction").val();
    paramEstateObj.totalFloors = $("#inp-total-floors").val().trim();
    paramEstateObj.bath = $("#inp-bath").val().trim();
    paramEstateObj.bedroom = $("#inp-bedroom").val().trim();
    paramEstateObj.balcony = $("#inp-balcony").val().trim();
    paramEstateObj.landscapeView = $("#inp-landscape").val().trim();
    paramEstateObj.apartCode = $("#inp-apartment-code").val().trim();
    paramEstateObj.apartType = $("#inp-apartment-type").val().trim();
    paramEstateObj.apartLoca = $("#inp-apartment-location").val().trim();
    paramEstateObj.wallArea = $("#inp-wall-area").val().trim();
    paramEstateObj.furnitureType = $("#select-furniture").val();
    paramEstateObj.priceRent = $("#inp-price-rent").val().trim();
    paramEstateObj.returnRate = $("#inp-return-rate").val().trim();
    paramEstateObj.legalDoc = $("#inp-legaldoc").val().trim();
    paramEstateObj.widthY = $("#inp-width").val().trim();
    paramEstateObj.longX = $("#inp-long").val().trim();
    paramEstateObj.streetHouse = $("#inp-street-house").val().trim();
    paramEstateObj.fsbo = $("#inp-fsbo").val().trim();
    paramEstateObj.viewNum = $("#inp-view-num").val().trim();
    paramEstateObj.description = $("#inp-description").val().trim();
    paramEstateObj.distance2facade = $("#inp-distance-facade").val().trim();
    paramEstateObj.adjacentFacadeNum = $("#inp-adjacent-facade").val().trim();
    paramEstateObj.adjacentRoad = $("#inp-adjacent-road").val().trim();
    paramEstateObj.alleyMinWidth = $("#inp-alley").val().trim();
    paramEstateObj.adjacentAlleyMinWidth = $("#inp-adjacent-alley").val().trim();
    paramEstateObj.factor = $("#inp-factor").val().trim();
    paramEstateObj.structure = $("#inp-structure").val().trim();
    paramEstateObj.dtsxd = $("#inp-dtsxd").val().trim();
    paramEstateObj.clcl = $("#inp-clcl").val().trim();
    paramEstateObj.ctxdprice = $("#inp-ctxd-price").val().trim();
    paramEstateObj.ctxdvalue = $("#inp-ctxd-value").val().trim();
    paramEstateObj.photo = $("#inp-photo").val().split("\\").pop();
    paramEstateObj.lat = $("#inp-lat").val().trim();
    paramEstateObj.lng = $("#inp-lng").val().trim();
    paramEstateObj.censorred = $("#checkbox-censor:checked").val();
}
//Hàm kiểm tra dữ liệu Estate
function validateEstateData(paramEstateObj) {
    let vResult = true;
    if (paramEstateObj.title == "") {
        alert("Title Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.provinceId == -1) {
        alert("Province Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.price == "") {
        alert("Price Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.acreage == "") {
        alert("Acreage Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.request == -1) {
        alert("Request Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.type == -1) {
        alert("Type Request Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.address == "") {
        alert("Address Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.address == "") {
        alert("Street Not Empty");
        vResult = false;
    }
    return vResult;
}
//Hàm xử lý nút Update Estate
function onBtnUpdateEstateClick(event) {
    "use strict"
    console.log("Nút Update được ấn")
    let vEstateObj = {
        title: "",
        type: "",
        request: "",
        provinceId: "",
        districtId: "",
        wardId: "",
        streetId: "",
        projectId: "",
        customerId: "",
        address: "",
        price: "",
        priceMin: "",
        priceTime: "",
        acreage: "",
        direction: "",
        totalFloors: "",
        numberFloors: "",
        bath: "",
        apartCode: "",
        wallArea: "",
        bedroom: "",
        balcony: "",
        landscapeView: "",
        apartLoca: "",
        apartType: "",
        furnitureType: "",
        priceRent: "",
        returnRate: "",
        legalDoc: "",
        description: "",
        widthY: "",
        longX: "",
        streetHouse: "",
        viewNum: "",
        createBy: "",
        updateBy: "",
        shape: "",
        distance2facade: "",
        adjacentFacadeNum: "",
        adjacentRoad: "",
        alleyMinWidth: "",
        adjacentAlleyMinWidth: "",
        factor: "",
        structure: "",
        photo: "",
        lat: "",
        lng: "",
        clcl: "",
        ctxdprice: "",
        ctxdvalue: "",
        dtsxd: "",
        fsbo: "",
        censorred: false
    }
    var formData = new FormData(event);
    getDataByForm(vEstateObj);
    formData.append('realEstate', JSON.stringify(vEstateObj));
    if($('#inp-photo')[0].files[0]){
        formData.append('image',$('#inp-photo')[0].files[0]);
    }else {
        const defaultImage = new Blob([''], { type: 'image/jpeg' }); //Chuyển file rỗng thành file image
        formData.append('image',defaultImage,);
    }
    let vCheck = validateEstateData(vEstateObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/realestates/" + gEstateId,
            type: "PUT",
            data: formData,
            processData: false,
            contentType: false,
            headers: gHeader,
            success: function (res) {
                console.log(res);
                gSTT = 1;
                getAllEstateByRegion();
                loadDataToTable(gEstateList);
                gEstateId = -1;
                $("#edit-detail-estate-modal").modal("hide");
                Toast.fire({
                    icon: 'success',
                    title: 'Update Estate Success!'
                })
            },
            error: function (err) {
                Toast.fire({
                    icon: 'error',
                    title: 'Update Estate Failed:' + err.responseJSON.error
                })
            }
        })
    }
}
//Hàm load dữ liệu vào Bảng
function loadDataToTable(paramEstate) {
    "use strict"
    gTableEstate.clear();
    gTableEstate.rows.add(paramEstate);
    gTableEstate.draw();
    gTableEstate.buttons().container().prependTo('.table-tool');
}

//Hàm thay đổi màu nút
function changeColorRow(paramRow) {
    $("#table-estate tbody tr").removeClass("table-primary");
    $(paramRow).addClass("table-primary");

}
//Hàm xử lý nút Xác nhận xóa Estate
function onBtnConfirmDeleteClick() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/realestates/" + gEstateId,
        type: "DELETE",
        headers: gHeader,
        success: function (res) {
            alert("Delete Estate Success!");
            gSTT = 1;
            getEstateByCustomerId(gCustomerId);
            loadDataToTable(gEstateList);
            Toast.fire({
                icon: 'success',
                title: 'Delete Estate Success!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: 'Delete Estate Failed:' + err.responseJSON.error
            })
        }
    })
}
// -----------------------------------------------------------------------------------------------------------------------------------------//
//Hàm xử lý on change Select Province
function onChangeProvince(pProvinceId) {
    $("#select-district").find("option").remove().end().append('<option selected value="">Select District</option>');
    $("#select-ward").find("option").remove().end().append('<option selected value="">Select Ward</option>');
    $("#select-street").find("option").remove().end().append('<option selected value="">Select Street</option>');
    if (pProvinceId != "") {
        getDistrictsOfProvince(pProvinceId);
    }
}

//Hàm xử lý thay đổi District
function onChangeDistrict(pDistrictId) {
    $("#select-ward").find("option").remove().end().append('<option selected value="">Select Ward</option>');
    $("#select-street").find("option").remove().end().append('<option selected value="">Select Street</option>');
    if (pDistrictId != "") {
        getWardsOfDistrict(pDistrictId);
        getStreetsOfDistrict(pDistrictId);
    }
}
//Hàm ajax lấy dữ liệu Province
function getAllProvince() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/provinces",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            gProvinceList = res
            console.log(res);
            res.forEach(element => {
                $("#select-province").append($("<option>", {
                    value: element.id,
                    text: element.name,
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu District
function getAllDistrict() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/districts",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            gDistrictList = res
            console.log(res);
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu Ward
function getAllWard() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/wards",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            gWardList = res
            console.log(res);
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}

//Hàm yêu cầu ajax lấy Ward của District
function getWardsOfDistrict(pDistrictId) {
    "use strict"
    if (pDistrictId != null) {
        $.ajax({
            url: gBASE_URL + "/districts/" + pDistrictId + "/wards",
            dataType: "json",
            type: "GET",
            async: false,
            headers: gHeader,
            success: function (res) {
                res.sort((a, b) => a.name - b.name);
                res.forEach(element => {
                    $("#select-ward").append($("<option>", {
                        value: element.id,
                        text: [element.prefix, element.name].join(" "),
                    }));
                });
            },
            error: function (err) {
                alert(err.responseText);
            }
        })
    }
}
//Hàm yêu cầu ajax get all District
function getDistrictsOfProvince(pProvinceId) {
    "use strict"
    if (pProvinceId != null) {
        $.ajax({
            url: gBASE_URL + "/provinces/" + pProvinceId + "/districts",
            dataType: "json",
            type: "GET",
            async: false,
            headers: gHeader,
            success: function (res) {
                console.log(res);
                res.forEach(element => {
                    $("#select-district").append($("<option>", {
                        value: element.id,
                        text: [element.prefix, element.name].join(" ")
                    }));
                });
            },
            error: function (err) {
                alert(err.responseText);
            }
        })
    }
}
//Hàm yêu cầu ajax lấy Street của District
function getStreetsOfDistrict(pDistrictId) {
    "use strict"
    if (pDistrictId != null) {
        $.ajax({
            url: gBASE_URL + "/districts/" + pDistrictId + "/streets",
            dataType: "json",
            type: "GET",
            async: false,
            success: function (res) {
                res.sort((a, b) => a.name - b.name);
                res.forEach(element => {
                    $("#select-street").append($("<option>", {
                        value: element.id,
                        text: [element.prefix, element.name].join(" "),
                    }));
                });
            },
            error: function (err) {
                alert(err.responseText);
            }
        })
    }
}
//Clear Form
function refeshForm() {
    $(document).find(".content-info input").val("");
    $(document).find(".content-info textarea").val("");
    $(document).find(".content-info select").val("").change();
    $(document).find("#checkbox-censor").prop('checked', false)
    $(document).find("#checkbox-censor").val("true");
}

//Hàm load dữ liệu lên Form
function loadDataCustomerToForm(paramCustomerData) {
    "use strict"
    $("#inp-name-customer").val(paramCustomerData.contactName);
    $("#inp-mobile-customer").val(paramCustomerData.mobile);
    $("#inp-address-customer").val(paramCustomerData.address);
    $("#inp-email-customer").val(paramCustomerData.email);
}
//Hàm xử lý nút Add Customer
function onBtnAddCustomerClick() {
    "use strict"
    let vCustomerObj = {
        contactTitle: "",
        note: "",
        contactName: "",
        mobile: "",
        email: "",
        address: ""
    }
    console.log(gHeader);
    getDataByFormModal(vCustomerObj);
    let vCheck = validateCustomerData(vCustomerObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/customers",
            type: "POST",
            data: JSON.stringify(vCustomerObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log(res);
                getAllCustomer();
                $("#add-customer-modal").find("input, textarea").val("");
                $("#add-customer-modal").modal("hide");
                Toast.fire({
                    icon: 'success',
                    title: "Add Customer Success!"
                })
            },
            error: function (err) {
                Toast.fire({
                    icon: 'error',
                    title: 'Add Customer Failed:' + err.responseJSON.error
                })
            }
        })
    }
}
//Hàm xử lý nút hiện thi ảnh trong modal
$("#btn-display-image").on("click", () => {
    $("#img-estate-modal").modal("show");
});

var uploadImage = "";
$("#inp-photo").on("change", function (e) {
    console.log(e)
    const READER = new FileReader();
    READER.onload = () => {
        uploadImage = READER.result;
        console.log(uploadImage);
        $("#display-image").css("background-image", `url(${uploadImage})`)
            .css("background-size", "cover")
    }
    READER.readAsDataURL(this.files[0]);
})
pagination(1, 100)
//Hàm xử lý phân trang
$(document).on("click", "li .page-link", function () {
    let vPageIndex = $(this).data("index");
    console.log(vPageIndex)
    if (gProvinceId != "") {
        getEstateByProvinceId(gProvinceId, vPageIndex - 1, 10);
        loadDataToTable(gEstateList);
    }
    if (gDistrictId != "") {
        getEstateByDistrictId(gDistrictId, vPageIndex - 1, 10);
        loadDataToTable(gEstateList);
    }
    if (gWardId != "") {
        getEstateByWardId(gWardId, vPageIndex - 1, 10);
        loadDataToTable(gEstateList);
    }
})


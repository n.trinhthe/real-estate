"use strict"
const gBASE_URL = "http://localhost:8080"
var gContractorList = "";
var gContractorList = "";
var gDesignUnitList = "";
var gSTT = 1;
var gContractorId = -1;
//Định nghĩa bảng
var gNameCol = ["id", "name", "address", "phone", "phone2", "fax", "email", "website", "projects",
    "description", "note", "action"];
const gCOL_ID = 0;
const gCOL_NAME = 1;
const gCOL_ADDRESS = 2;
const gCOL_PHONE = 3;
const gCOL_PHONE2 = 4;
const gCOL_FAX = 5;
const gCOL_EMAIL = 6;
const gCOL_WEBSITE = 7;
const gCOL_PROJECTS = 8;
const gCOL_DESCRIPTION = 9;
const gCOL_NOTE = 10;
const gCOL_ACTION = 11;
var gTableContractor = $("#table-contractor").DataTable({
    columns: [
        { data: gNameCol[gCOL_ID] },
        { data: gNameCol[gCOL_NAME] },
        { data: gNameCol[gCOL_ADDRESS] },
        { data: gNameCol[gCOL_PHONE] },
        { data: gNameCol[gCOL_PHONE2] },
        { data: gNameCol[gCOL_FAX] },
        { data: gNameCol[gCOL_EMAIL] },
        { data: gNameCol[gCOL_WEBSITE] },
        { data: gNameCol[gCOL_PROJECTS] },
        { data: gNameCol[gCOL_DESCRIPTION] },
        { data: gNameCol[gCOL_NOTE] },
        { data: gNameCol[gCOL_ACTION] },
    ],
    columnDefs: [
        {
            targets: gCOL_ID,
            render: function (data) {
                return `
                <div class="text-center">
                    ${gSTT++}
                    <i data-id="${data}" class="far fa-edit text-primary btn-edit" style="cursor: pointer"></i>
                    <i data-id="${data}" class="fas fa-trash text-primary btn-delete" style="cursor: pointer"></i>
                </div>
                `
            },

        },
        {
            className: 'dtr-control',
            orderable: false,
            targets: -1
        },
        {
            targets: gCOL_ACTION,
            defaultContent: ``
        },
    ],
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    autoWidth: false,
    responsive: {
        details: {
            type: 'column',
            target: -1
        }
    },
    stateSave: true,
    searching: false,
})

$(document).ready(function () {
    getAllContractor();
    loadDataToTable(gContractorList);
})

$("#table-contractor tbody").on("click", "tr.odd,tr.even", function () { //Click dòng
    changeColorRow(this);
})
$(document).on("click", ".btn-edit", function () { //Nút hiện thị Update Contractor Modal
    gContractorId = $(this).data("id");
    $("#edit-contractor-modal").modal("show")
    getContractorById(gContractorId);
})
$("#btn-confirm-update").on("click", function(){ //Nút confirm update
    onBtnUpdateContractorClick();
})
$("#btn-add-contractor").on("click", function () { //Nút hiện thị Add Contractor Modal
    $("#add-contractor-modal").modal("show");
})
$("#btn-confirm-add").on("click", function () { //Nút Add Contractor
    onBtnAddContractorClick();
})
$(document).on("click", ".btn-delete", function () { //Nút hiện thị Update Contractor Modal
    gContractorId = $(this).data("id");
    $("#delete-contractor-modal").modal("show")
    getContractorById(gContractorId);
})
$("#btn-confirm-delete-contractor").on("click", function () { //Nút Xác Nhận Delete Contractor
    onBtnConfirmDeleteClick();
    $("#delete-contractor-modal").modal("hide");
})

//Hàm yêu cầu lấy dữ liệu All Contractor từ server
function getAllContractor() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/contractors",
        dataType: "json",
        type: "GET",
        async: false,
        success: function (res) {
            console.log(res);
            gContractorList = res;
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: "Get All Contractor Failed: " + err.responseJSON.error
            })
        }
    })
}
//Hàm load dữ liệu vào Bảng
function loadDataToTable(paramContractor) {
    "use strict"
    gTableContractor.clear();
    gTableContractor.rows.add(paramContractor);
    gTableContractor.draw();
    gTableContractor.buttons().container().prependTo('.table-tool');
}
//Hàm xử lý nút Add Contractor
function onBtnAddContractorClick() {
    "use strict"
    let vContractorObj = {
        name: "",
        address: "",
        description: "",
        note: "",
        fax: "",
        phone: "",
        phone2: "",
        email: "",
        website: "",
        projects: ""
    }
    getDataByFormAddModal(vContractorObj);
    let vCheck = validateContractorData(vContractorObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/contractors",
            type: "POST",
            data: JSON.stringify(vContractorObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log(res);
                gSTT = 1;
                getAllContractor();
                loadDataToTable(gContractorList);
                refeshForm();
                Toast.fire({
                    icon: 'success',
                    title: "Add Contractor Success!"
                })
            },
            error: function (err) {
                Toast.fire({
                    icon: 'error',
                    title: "Add Contractor Failed: " + err.responseJSON.error
                })
            }
        })
    }
}

//Hàm kiểm tra dữ liệu Contractor
function validateContractorData(paramContractorObj) {
    let vResult = true;
    if (paramContractorObj.name == "") {
        alert("Name Is Empty");
        vResult = false;
    }
    else if (paramContractorObj.phone == "") {
        alert("Phone Is Empty");
        vResult = false;
    }
    else if (paramContractorObj.address == "") {
        alert("Address Is Empty");
        vResult = false;
    }
    return vResult;
}
//Hàm Truy xuất dữ liệu từ form Edit Modal
function getDataByFormEditModal(paramContractorObj) {
    "use strict"
    paramContractorObj.name = $("#inp-name-edit-modal").val().trim();
    paramContractorObj.address = $("#inp-address-edit-modal").val().trim();
    paramContractorObj.description = $("#textarea-description-edit-modal").val().trim();
    paramContractorObj.note = $("#textarea-note-edit-modal").val().trim();
    paramContractorObj.fax = $("#inp-fax-edit-modal").val().trim();
    paramContractorObj.phone = $("#inp-phone-edit-modal").val().trim();
    paramContractorObj.phone2 = $("#inp-phone2-edit-modal").val().trim();
    paramContractorObj.email = $("#inp-email-edit-modal").val().trim();
    paramContractorObj.website = $("#inp-website-edit-modal").val().trim();
    paramContractorObj.projects = $("#textarea-project-edit-modal").val().trim();
}
//Hàm Truy xuất dữ liệu từ form Edit Modal
function getDataByFormAddModal(paramContractorObj) {
    "use strict"
    paramContractorObj.name = $("#inp-name-add-modal").val().trim();
    paramContractorObj.address = $("#inp-address-add-modal").val().trim();
    paramContractorObj.description = $("#textarea-description-add-modal").val().trim();
    paramContractorObj.note = $("#textarea-note-add-modal").val().trim();
    paramContractorObj.fax = $("#inp-fax-add-modal").val().trim();
    paramContractorObj.phone = $("#inp-phone-add-modal").val().trim();
    paramContractorObj.phone2 = $("#inp-phone2-add-modal").val().trim();
    paramContractorObj.email = $("#inp-email-add-modal").val().trim();
    paramContractorObj.website = $("#inp-website-add-modal").val().trim();
    paramContractorObj.projects = $("#textarea-project-add-modal").val().trim();
}
//Hàm thay đổi màu nút
function changeColorRow(paramRow) {
    $("#table-contractor tbody tr").removeClass("table-primary");
    $(paramRow).addClass("table-primary");

}
//Hàm load dữ liệu lên Form
function loadDataContractorToForm(paramContractorObj) {
    "use strict"
    $("#inp-name-edit-modal").val(paramContractorObj.name);
    $("#inp-address-edit-modal").val(paramContractorObj.address);
    $("#textarea-description-edit-modal").val(paramContractorObj.description);
    $("#textarea-note-edit-modal").val(paramContractorObj.note);
    $("#inp-fax-edit-modal").val(paramContractorObj.fax);
    $("#inp-phone-edit-modal").val(paramContractorObj.phone);
    $("#inp-phone2-edit-modal").val(paramContractorObj.phone2);
    $("#inp-email-edit-modal").val(paramContractorObj.email);
    $("#inp-website-edit-modal").val(paramContractorObj.website);
    $("#textarea-project-edit-modal").val(paramContractorObj.projects);
}
//Hàm xử lý nút Update Contractor
function onBtnUpdateContractorClick() {
    "use strict"
    console.log("Nút Update được ấn")
    let vContractorObj = {
        name: "",
        address: "",
        description: "",
        note: "",
        fax: "",
        phone: "",
        phone2: "",
        email: "",
        website: "",
        projects: ""
    }
    getDataByFormEditModal(vContractorObj);
    let vCheck = validateContractorData(vContractorObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/contractors/" + gContractorId,
            type: "PUT",
            data: JSON.stringify(vContractorObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log(res);
                gSTT = 1;
                getAllContractor();
                loadDataToTable(gContractorList);
                Toast.fire({
                    icon: 'success',
                    title: "Update Contractor Success!!!" 
                })
            },
            error: function (err) {
                Toast.fire({
                    icon: 'error',
                    title: "Update Contractor Failed: " + err.responseJSON.error
                })
            }
        })
    }
}
//Hàm yêu câu Get Contractor By Id từ server
function getContractorById(paramContractorId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/contractors/" + paramContractorId,
        dataType: "json",
        type: "GET",
        async: false,
        success: function (res) {
            console.log(res);
            loadDataContractorToForm(res);
            Toast.fire({
                icon: 'success',
                title: 'Get Contractor Success!!!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: 'Get Contractor Failed: ' + err.responseJSON.error
            })
        }
    })
}

//Hàm xử lý nút Xác nhận xóa Contractor
function onBtnConfirmDeleteClick() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/contractors/" + gContractorId,
        type: "DELETE",
        headers: gHeader,
        success: function (res) {
            gSTT = 1;
            getAllContractor();
            loadDataToTable(gContractorList);
            Toast.fire({
                icon: 'success',
                title: 'Delete Contractor Success!!!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: "Delete Contractor Failed: " + err.responseJSON.error
            })
        }
    })
}
//Clear Form
function refeshForm() {
    $(document).find("#add-contractor-modal input").val("");
    $(document).find("#add-contractor-modal textarea").val("");
}

"use strict"
const gBASE_URL = "http://localhost:8080"
var gNameCol = ["id", "name", "prefix", "action"];
var gDistrictList = [];
var gDistrictId = "";
$(document).ready(function () {
  getAllProvince();
})

$("#select-province").on("change", function () { //Thay đổi Tỉnh thành
  onChangeProvince($(this).val());
})

$("#select-district").on("change", function () { //Thay đổi quận huyện
  onChangeDistrict($(this).val());
})

//Hàm xử lý on change Select Province
function onChangeProvince(paramProvinceCode) {
  $("#select-district").find("option").remove().end().append('<option selected value="">Chọn Quận/Huyện</option>');
  $("#select-ward").find("option").remove().end().append('<option selected value="">Chọn Phường/Xã</option>');
  if (paramProvinceCode != "") {
    getDistrictsOfProvince(paramProvinceCode);
  }
}

//Hàm xử lý thay đổi District
function onChangeDistrict(paramDistrictId) {
  $("#select-ward").find("option").remove().end().append('<option selected value="">Chọn Phường/Xã</option>');
  if (paramDistrictId != "") {
    getWardsOfDistrict(paramDistrictId);
  }
}
//Hàm ajax lấy dữ liệu Province
function getAllProvince() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/provinces",
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      res.forEach(element => {
        $("#select-province").append($("<option>", {
          value: element.id,
          text: element.name,
        }));
      });
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
//Hàm yêu cầu ajax lấy Ward của District
function getWardsOfDistrict(paramDistrictId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/districts/" + paramDistrictId + "/wards",
    dataType: "json",
    type: "GET",
    success: function (res) {
      res.sort((a, b) => a.name - b.name);

      res.forEach(element => {
        $("#select-ward").append($("<option>", {
          value: element.id,
          text: [element.prefix, element.name].join(" "),
        }));
      });
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
//Hàm yêu cầu ajax get all District
function getDistrictsOfProvince(paramProvinceCode) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/provinces/" + paramProvinceCode + "/districts",
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      res.forEach(element => {
        $("#select-district").append($("<option>", {
          value: element.id,
          text: [element.prefix, element.name].join(" ")
        }));
      });
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
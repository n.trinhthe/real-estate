"use strict"
const gBASE_URL = "http://localhost:8080"
var gDesignUnitList = "";
var gSTT = 1;
var gDesignUnitId = -1;
//Định nghĩa bảng
var gNameCol = ["id", "name", "address", "phone", "phone2", "fax", "email", "website", "projects",
    "description", "note", "action"];
const gCOL_ID = 0;
const gCOL_NAME = 1;
const gCOL_ADDRESS = 2;
const gCOL_PHONE = 3;
const gCOL_PHONE2 = 4;
const gCOL_FAX = 5;
const gCOL_EMAIL = 6;
const gCOL_WEBSITE = 7;
const gCOL_PROJECTS = 8;
const gCOL_DESCRIPTION = 9;
const gCOL_NOTE = 10;
const gCOL_ACTION = 11;
var gTableDesignUnit = $("#table-designunit").DataTable({
    columns: [
        { data: gNameCol[gCOL_ID] },
        { data: gNameCol[gCOL_NAME] },
        { data: gNameCol[gCOL_ADDRESS] },
        { data: gNameCol[gCOL_PHONE] },
        { data: gNameCol[gCOL_PHONE2] },
        { data: gNameCol[gCOL_FAX] },
        { data: gNameCol[gCOL_EMAIL] },
        { data: gNameCol[gCOL_WEBSITE] },
        { data: gNameCol[gCOL_PROJECTS] },
        { data: gNameCol[gCOL_DESCRIPTION] },
        { data: gNameCol[gCOL_NOTE] },
        { data: gNameCol[gCOL_ACTION] },
    ],
    columnDefs: [
        {
            targets: gCOL_ID,
            render: function (data) {
                return `
                <div class="text-center">
                    ${gSTT++}
                    <i data-id="${data}" class="far fa-edit text-primary btn-edit" style="cursor: pointer"></i>
                    <i data-id="${data}" class="fas fa-trash text-primary btn-delete" style="cursor: pointer"></i>
                </div>
                `
            },

        },
        {
            className: 'dtr-control',
            orderable: false,
            targets: -1
        },
        {
            targets: gCOL_ACTION,
            defaultContent: ``
        },
    ],
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    autoWidth: false,
    responsive: {
        details: {
            type: 'column',
            target: -1
        }
    },
    stateSave: true,
    searching: true,
})

$(document).ready(function () {
    getAllDesignUnit();
    loadDataToTable(gDesignUnitList);
})

$("#table-designunit tbody").on("click", "tr.odd,tr.even", function () { //Click dòng
    changeColorRow(this);
})
$(document).on("click", ".btn-edit", function () { //Nút hiện thị Update DesignUnit Modal
    gDesignUnitId = $(this).data("id");
    $("#edit-designunit-modal").modal("show")
    getDesignUnitById(gDesignUnitId);
})
$("#btn-confirm-update").on("click", function(){ //Nút confirm update
    onBtnUpdateDesignUnitClick();
})
$("#btn-add-designunit").on("click", function () { //Nút hiện thị Add DesignUnit Modal
    $("#add-designunit-modal").modal("show");
})
$("#btn-confirm-add").on("click", function () { //Nút Add DesignUnit
    onBtnAddDesignUnitClick();
})
$(document).on("click", ".btn-delete", function () { //Nút hiện thị Update DesignUnit Modal
    gDesignUnitId = $(this).data("id");
    $("#delete-designunit-modal").modal("show")
    getDesignUnitById(gDesignUnitId);
})
$("#btn-confirm-delete-designunit").on("click", function () { //Nút Xác Nhận Delete DesignUnit
    onBtnConfirmDeleteClick();
    $("#delete-designunit-modal").modal("hide");
})

//Hàm yêu cầu lấy dữ liệu All DesignUnit từ server
function getAllDesignUnit() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/designunits",
        dataType: "json",
        type: "GET",
        async: false,
        success: function (res) {
            console.log(res);
            gDesignUnitList = res;
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: 'Get All Design Unit Failed:' + err.responseJSON.error
              })
        }
    })
}
//Hàm load dữ liệu vào Bảng
function loadDataToTable(paramDesignUnit) {
    "use strict"
    gTableDesignUnit.clear();
    gTableDesignUnit.rows.add(paramDesignUnit);
    gTableDesignUnit.draw();
    gTableDesignUnit.buttons().container().prependTo('.table-tool');
}
//Hàm xử lý nút Add DesignUnit
function onBtnAddDesignUnitClick() {
    "use strict"
    let vDesignUnitObj = {
        name: "",
        address: "",
        description: "",
        note: "",
        fax: "",
        phone: "",
        phone2: "",
        email: "",
        website: "",
        projects: ""
    }
    getDataByFormAddModal(vDesignUnitObj);
    let vCheck = validateDesignUnitData(vDesignUnitObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/designunits",
            type: "POST",
            data: JSON.stringify(vDesignUnitObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log(res);
                gSTT = 1;
                getAllDesignUnit();
                loadDataToTable(gDesignUnitList);
                refeshForm();
                Toast.fire({
                    icon: 'success',
                    title: "Add DesignUnit Success!"
                })
            },
            error: function (err) {
                Toast.fire({
                    icon: 'error',
                    title: "Add DesignUnit Failed: " + err.responseJSON.error
                })
            }
        })
    }
}

//Hàm kiểm tra dữ liệu DesignUnit
function validateDesignUnitData(paramDesignUnitObj) {
    let vResult = true;
    if (paramDesignUnitObj.name == "") {
        alert("Name Is Empty");
        vResult = false;
    }
    else if (paramDesignUnitObj.phone == "") {
        alert("Phone Is Empty");
        vResult = false;
    }
    else if (paramDesignUnitObj.address == "") {
        alert("Address Is Empty");
        vResult = false;
    }
    return vResult;
}
//Hàm Truy xuất dữ liệu từ form Edit Modal
function getDataByFormEditModal(paramDesignUnitObj) {
    "use strict"
    paramDesignUnitObj.name = $("#inp-name-edit-modal").val().trim();
    paramDesignUnitObj.address = $("#inp-address-edit-modal").val().trim();
    paramDesignUnitObj.description = $("#textarea-description-edit-modal").val().trim();
    paramDesignUnitObj.note = $("#textarea-note-edit-modal").val().trim();
    paramDesignUnitObj.fax = $("#inp-fax-edit-modal").val().trim();
    paramDesignUnitObj.phone = $("#inp-phone-edit-modal").val().trim();
    paramDesignUnitObj.phone2 = $("#inp-phone2-edit-modal").val().trim();
    paramDesignUnitObj.email = $("#inp-email-edit-modal").val().trim();
    paramDesignUnitObj.website = $("#inp-website-edit-modal").val().trim();
    paramDesignUnitObj.projects = $("#textarea-project-edit-modal").val().trim();
}
//Hàm Truy xuất dữ liệu từ form Edit Modal
function getDataByFormAddModal(paramDesignUnitObj) {
    "use strict"
    paramDesignUnitObj.name = $("#inp-name-add-modal").val().trim();
    paramDesignUnitObj.address = $("#inp-address-add-modal").val().trim();
    paramDesignUnitObj.description = $("#textarea-description-add-modal").val().trim();
    paramDesignUnitObj.note = $("#textarea-note-add-modal").val().trim();
    paramDesignUnitObj.fax = $("#inp-fax-add-modal").val().trim();
    paramDesignUnitObj.phone = $("#inp-phone-add-modal").val().trim();
    paramDesignUnitObj.phone2 = $("#inp-phone2-add-modal").val().trim();
    paramDesignUnitObj.email = $("#inp-email-add-modal").val().trim();
    paramDesignUnitObj.website = $("#inp-website-add-modal").val().trim();
    paramDesignUnitObj.projects = $("#textarea-project-add-modal").val().trim();
}
//Hàm thay đổi màu nút
function changeColorRow(paramRow) {
    $("#table-designunit tbody tr").removeClass("table-primary");
    $(paramRow).addClass("table-primary");

}
//Hàm load dữ liệu lên Form
function loadDataDesignUnitToForm(paramDesignUnitObj) {
    "use strict"
    $("#inp-name-edit-modal").val(paramDesignUnitObj.name);
    $("#inp-address-edit-modal").val(paramDesignUnitObj.address);
    $("#textarea-description-edit-modal").val(paramDesignUnitObj.description);
    $("#textarea-note-edit-modal").val(paramDesignUnitObj.note);
    $("#inp-fax-edit-modal").val(paramDesignUnitObj.fax);
    $("#inp-phone-edit-modal").val(paramDesignUnitObj.phone);
    $("#inp-phone2-edit-modal").val(paramDesignUnitObj.phone2);
    $("#inp-email-edit-modal").val(paramDesignUnitObj.email);
    $("#inp-website-edit-modal").val(paramDesignUnitObj.website);
    $("#textarea-project-edit-modal").val(paramDesignUnitObj.projects);
}
//Hàm xử lý nút Update DesignUnit
function onBtnUpdateDesignUnitClick() {
    "use strict"
    console.log("Nút Update được ấn")
    let vDesignUnitObj = {
        name: "",
        address: "",
        description: "",
        note: "",
        fax: "",
        phone: "",
        phone2: "",
        email: "",
        website: "",
        projects: ""
    }
    getDataByFormEditModal(vDesignUnitObj);
    let vCheck = validateDesignUnitData(vDesignUnitObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/designunits/" + gDesignUnitId,
            type: "PUT",
            data: JSON.stringify(vDesignUnitObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log(res);
                gSTT = 1;
                getAllDesignUnit();
                loadDataToTable(gDesignUnitList);
                Toast.fire({
                    icon: 'success',
                    title: "Update DesignUnit Success!!!" 
                })
            },
            error: function (err) {
                Toast.fire({
                    icon: 'error',
                    title: "Update DesignUnit Failed: " + err.responseJSON.error
                })
            }
        })
    }
}
//Hàm yêu câu Get DesignUnit By Id từ server
function getDesignUnitById(paramDesignUnitId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/designunits/" + paramDesignUnitId,
        dataType: "json",
        type: "GET",
        async: false,
        success: function (res) {
            console.log(res);
            loadDataDesignUnitToForm(res);
            Toast.fire({
                icon: 'success',
                title: 'Get DesignUnit Success!!!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: 'Get DesignUnit Failed: ' + err.responseJSON.error
            })
        }
    })
}

//Hàm xử lý nút Xác nhận xóa DesignUnit
function onBtnConfirmDeleteClick() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/designunits/" + gDesignUnitId,
        type: "DELETE",
        headers: gHeader,
        success: function (res) {
            gSTT = 1;
            getAllDesignUnit();
            loadDataToTable(gDesignUnitList);
            Toast.fire({
                icon: 'success',
                title: 'Delete DesignUnit Success!!!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: "Delete DesignUnit Failed: " + err.responseJSON.error
            })
        }
    })
}
//Clear Form
function refeshForm() {
    $(document).find("#add-designunit-modal input").val("");
    $(document).find("#add-designunit-modal textarea").val("");
}

"use strict"
var gCustomerList = "";
var gProjectList = "";
var gProvinceList = "";
var gDistrictList = "";
var gWardList = "";
var gStreetList = "";
const gBASE_URL = "http://localhost:8080"
$(document).ready(function () {
    getAllCustomer();
    getAllProject();
    getAllProvince();
    getAllDistrict();
    getAllWard();
    getAllStreet();
})
$("#select-province").on("change", function () { //Thay đổi Tỉnh thành
    onChangeProvince($(this).val());
})

$("#select-district").on("change", function () { //Thay đổi quận huyện
    onChangeDistrict($(this).val());
})
$("#form-add-realestate").submit(function (event) { //Nút Add Estate
    event.preventDefault();
    onBtnAddEstateClick(this);
})
$("#table-estate tbody").on("click", "tr.odd,tr.even", function () { //Click dòng
    changeColorRow(this);
})

$("#btn-customer-modal").on("click", function () {
    $("#add-customer-modal").modal("show");
})
$("#btn-add-customer").on("click", function () {
    onBtnAddCustomerClick()
})
//Hàm ajax lấy dữ liệu Investor
function getAllCustomer() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/employees/" + gInfo.userId + "/customers",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            console.log(res);
            gCustomerList = res;
            res.forEach(element => {
                $("#select-customer").append($("<option>", {
                    value: element.id,
                    text: element.contactName + " - " + element.mobile,
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm xử lý nút Add Customer
function onBtnAddCustomerClick() {
    "use strict"
    let vCustomerObj = {
        contactTitle: "",
        note: "",
        contactName: "",
        mobile: "",
        email: "",
        address: ""
    }
    console.log(gHeader);
    getDataByFormCustomerModal(vCustomerObj);
    let vCheck = validateCustomerData(vCustomerObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/customers",
            type: "POST",
            data: JSON.stringify(vCustomerObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log(res);
                alert("Add Customer Success!");
                getAllCustomer();
                $("#add-customer-modal").find("input, textarea").val("");
                $("#add-customer-modal").modal("hide");
            },
            error: function (err) {
                Toast.fire({
                    icon: 'error',
                    title: 'Add Customer Failed:' + err.responseText
                })
            }
        })
    }
}
//Hàm Truy xuất dữ liệu từ form
function getDataByFormCustomerModal(paramCustomerObj) {
    "use strict"
    paramCustomerObj.contactTitle = $("#inp-title-customer").val().trim();
    paramCustomerObj.note = $("#textarea-note").val().trim();
    paramCustomerObj.contactName = $("#inp-name").val().trim();
    paramCustomerObj.mobile = $("#inp-mobile").val().trim();
    paramCustomerObj.email = $("#inp-email").val().trim();
    paramCustomerObj.address = $("#inp-address").val().trim();

}
//Hàm kiểm tra dữ liệu Customer
function validateCustomerData(paramCustomerObj) {
    let vResult = true;
    if (paramCustomerObj.contactTitle == "") {
        alert("Contact Title chưa được nhập!");
        vResult = false;
    }
    else if (paramCustomerObj.contactName == "") {
        alert("Contact Name chưa được nhập!");
        vResult = false;
    }
    else if (paramCustomerObj.mobile == "") {
        alert("Phone Number chưa được nhập!");
        vResult = false;
    }
    else if (paramCustomerObj.email == "") {
        alert("Email chưa được nhập!");
        vResult = false;
    }
    return vResult;
}
//Hàm ajax lấy dữ liệu Project
function getAllProject() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/projects",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            console.log(res);
            gProjectList = res;
            res.forEach(element => {
                $("#select-project").append($("<option>", {
                    value: element.id,
                    text: element.name,
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu Street
function getAllStreet() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/streets",
        dataType: "json",
        type: "GET",
        async: false,
        success: function (res) {
            gStreetList = res;
            console.log(res);
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm xử lý nút Add Estate
function onBtnAddEstateClick(event) {
    "use strict"
    let vEstateObj = {
        title: "",
        type: "",
        request: "",
        provinceId: "",
        districtId: "",
        wardId: "",
        streetId: "",
        projectId: "",
        customerId: "",
        address: "",
        price: "",
        priceMin: "",
        priceTime: "",
        acreage: "",
        direction: "",
        totalFloors: "",
        numberFloors: "",
        bath: "",
        apartCode: "",
        wallArea: "",
        bedroom: "",
        balcony: "",
        landscapeView: "",
        apartLoca: "",
        apartType: "",
        furnitureType: "",
        priceRent: "",
        returnRate: "",
        legalDoc: "",
        description: "",
        widthY: "",
        longX: "",
        streetHouse: "",
        viewNum: "",
        shape: "",
        distance2facade: "",
        adjacentFacadeNum: "",
        adjacentRoad: "",
        alleyMinWidth: "",
        adjacentAlleyMinWidth: "",
        factor: "",
        structure: "",
        photo: "",
        lat: "",
        lng: "",
        clcl: "",
        ctxdprice: "",
        ctxdvalue: "",
        dtsxd: "",
        fsbo: "",
        censorred: false
    }
    var formData = new FormData(event);
    getDataByForm(vEstateObj);
    formData.append('realEstate', JSON.stringify(vEstateObj));
    if($('#inp-photo')[0].files[0]){
        formData.append('image',$('#inp-photo')[0].files[0]);
    }else {
        const defaultImage = new Blob([''], { type: 'image/jpeg' }); //Chuyển file rỗng thành file image
        formData.append('image',defaultImage,);
    }
    console.log(formData);
    console.log($('#inp-photo')[0].files[0]);
    let vCheck = validateEstateData(vEstateObj);
    console.log(formData);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/realestates",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            headers: gHeader,
            success: function (res) {
                console.log(res);
                Toast.fire({
                    icon: 'success',
                    title: 'Add Real Estate Success!!!'
                })
                window.location.href="realestate.html"
            },
            error: function (err) {
                Toast.fire({
                    icon: 'error',
                    title: 'Add Real Estate Failed: ' + err.responseText
                })
            }
        })
    }
}
//Hàm Truy xuất dữ liệu từ form
function getDataByForm(paramEstateObj) {
    "use strict"
    paramEstateObj.title = $("#inp-title").val().trim();
    paramEstateObj.type = $("#select-type").val();
    paramEstateObj.request = $("#select-request").val();
    paramEstateObj.provinceId = $("#select-province").val();
    paramEstateObj.districtId = $("#select-district").val();
    paramEstateObj.wardId = $("#select-ward").val();
    paramEstateObj.streetId = $("#select-street").val();
    paramEstateObj.address = $("#inp-address").val().trim();
    paramEstateObj.projectId = $("#select-project").val();
    paramEstateObj.customerId = $("#select-customer").val();
    paramEstateObj.price = $("#inp-price").val().trim();
    paramEstateObj.priceMin = $("#inp-price-min").val().trim();
    paramEstateObj.priceTime = $("#inp-price-time").val().trim();
    paramEstateObj.acreage = $("#inp-acreage").val().trim();
    paramEstateObj.direction = $("#select-direction").val();
    paramEstateObj.totalFloors = $("#inp-total-floors").val().trim();
    paramEstateObj.bath = $("#inp-bath").val().trim();
    paramEstateObj.bedroom = $("#inp-bedroom").val().trim();
    paramEstateObj.balcony = $("#inp-balcony").val().trim();
    paramEstateObj.landscapeView = $("#inp-landscape").val().trim();
    paramEstateObj.apartCode = $("#inp-apartment-code").val().trim();
    paramEstateObj.apartType = $("#inp-apartment-type").val().trim();
    paramEstateObj.apartLoca = $("#inp-apartment-location").val().trim();
    paramEstateObj.wallArea = $("#inp-wall-area").val().trim();
    paramEstateObj.furnitureType = $("#select-furniture").val();
    paramEstateObj.priceRent = $("#inp-price-rent").val().trim();
    paramEstateObj.returnRate = $("#inp-return-rate").val().trim();
    paramEstateObj.legalDoc = $("#inp-legaldoc").val().trim();
    paramEstateObj.widthY = $("#inp-width").val().trim();
    paramEstateObj.longX = $("#inp-long").val().trim();
    paramEstateObj.streetHouse = $("#inp-street-house").val().trim();
    paramEstateObj.fsbo = $("#inp-fsbo").val().trim();
    paramEstateObj.viewNum = $("#inp-view-num").val().trim();
    paramEstateObj.description = $("#inp-description").val().trim();
    paramEstateObj.distance2facade = $("#inp-distance-facade").val().trim();
    paramEstateObj.adjacentFacadeNum = $("#inp-adjacent-facade").val().trim();
    paramEstateObj.adjacentRoad = $("#inp-adjacent-road").val().trim();
    paramEstateObj.alleyMinWidth = $("#inp-alley").val().trim();
    paramEstateObj.adjacentAlleyMinWidth = $("#inp-adjacent-alley").val().trim();
    paramEstateObj.factor = $("#inp-factor").val().trim();
    paramEstateObj.structure = $("#inp-structure").val().trim();
    paramEstateObj.dtsxd = $("#inp-dtsxd").val().trim();
    paramEstateObj.clcl = $("#inp-clcl").val().trim();
    paramEstateObj.ctxdprice = $("#inp-ctxd-price").val().trim();
    paramEstateObj.ctxdvalue = $("#inp-ctxd-value").val().trim();
    paramEstateObj.photo = $("#inp-photo").val().split('\\').pop();
    paramEstateObj.lat = $("#inp-lat").val().trim();
    paramEstateObj.lng = $("#inp-lng").val().trim();
}
//Hàm kiểm tra dữ liệu Estate
function validateEstateData(paramEstateObj) {
    let vResult = true;
    if (paramEstateObj.title == "") {
        alert("Title Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.provinceId == "") {
        alert("Province Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.price == "") {
        alert("Price Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.acreage == "") {
        alert("Acreage Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.request == "") {
        alert("Request Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.type == "") {
        alert("Type Request Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.address == "") {
        alert("Address Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.provinceId == "") {
        alert("Province Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.districtId == "") {
        alert("District Not Empty");
        vResult = false;
    }
    else if (paramEstateObj.customerId == "") {
        alert("Customer Not Empty");
        vResult = false;
    }
    return vResult;
}

// -----------------------------------------------------------------------------------------------------------------------------------------//
//Hàm xử lý on change Select Province
function onChangeProvince(pProvinceId) {
    $("#select-district").find("option").remove().end().append('<option value="">Select District</option>');
    $("#select-ward").find("option").remove().end().append('<option value="null">Select Ward</option>');
    $("#select-street").find("option").remove().end().append('<option value="null">Select Street</option>');
    if (pProvinceId != "") {
        getDistrictsOfProvince(pProvinceId);
    }
}

//Hàm xử lý thay đổi District
function onChangeDistrict(pDistrictId) {
    $("#select-ward").find("option").remove().end().append('<option value="">Select Ward</option>');
    $("#select-street").find("option").remove().end().append('<option value="">Select Street</option>');
    if (pDistrictId != "") {
        getWardsOfDistrict(pDistrictId);
        getStreetsOfDistrict(pDistrictId);
    }
}
//Hàm ajax lấy dữ liệu Province
function getAllProvince() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/provinces",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            gProvinceList = res
            console.log(res);
            res.forEach(element => {
                $("#select-province").append($("<option>", {
                    value: element.id,
                    text: element.name,
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu District
function getAllDistrict() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/districts",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            gDistrictList = res
            console.log(res);
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu Ward
function getAllWard() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/wards",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            gWardList = res
            console.log(res);
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}

//Hàm yêu cầu ajax lấy Ward của District
function getWardsOfDistrict(pDistrictId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/districts/" + pDistrictId + "/wards",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            res.sort((a, b) => a.name - b.name);
            res.forEach(element => {
                $("#select-ward").append($("<option>", {
                    value: element.id,
                    text: [element.prefix, element.name].join(" "),
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm yêu cầu ajax get all District
function getDistrictsOfProvince(pProvinceId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/provinces/" + pProvinceId + "/districts",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            console.log(res);
            res.forEach(element => {
                $("#select-district").append($("<option>", {
                    value: element.id,
                    text: [element.prefix, element.name].join(" ")
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm yêu cầu ajax lấy Street của District
function getStreetsOfDistrict(pDistrictId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/districts/" + pDistrictId + "/streets",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            res.sort((a, b) => a.name - b.name);
            res.forEach(element => {
                $("#select-street").append($("<option>", {
                    value: element.id,
                    text: [element.prefix, element.name].join(" "),
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Clear Form
function refeshForm() {
    $(document).find(".content-info input").val("");
    $(document).find(".content-info textarea").val("");
    $(document).find(".content-info select").val("").change();
    $(document).find("#checkbox-censor").prop('checked', false)
    $(document).find("#checkbox-censor").val("true");
}

$("#btn-display-image").on("click", () => {
    $("#img-estate-modal").modal("show");
});

var uploadImage = "";
$("#inp-photo").on("change", function (e) {
    console.log(e)
    const READER = new FileReader();
    READER.onload = () => {
        uploadImage = READER.result;
        console.log(uploadImage);
        $("#display-image").css("background-image", `url(${uploadImage})`)
            .css("background-size", "cover")
    }
    READER.readAsDataURL(this.files[0]);
})




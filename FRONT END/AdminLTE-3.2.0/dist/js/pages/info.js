var gHeader = "";
var gInfo = "";
$(document).ready(function () {
    const token = getCookie("Token");
    //Gọi API để lấy thông tin người dùng
    //Khai báo xác thực ở headers
    if (token != "") {
        var headers = {
            Authorization: "Token " + token,
        };
        gHeader = headers;
        console.log(gHeader);
        var urlInfo = "http://localhost:8080/users/info";
        $.ajax({
            url: urlInfo,
            method: "GET",
            headers: headers,
            async: false,
            success: function (responseObject) {
                console.log(responseObject);
                gInfo = responseObject;
                displayUser(responseObject);
                displayUserMenu(responseObject);
            },
            error: function (xhr) {
                console.log(xhr);
                // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
                redirectToLogin();
            }
        });
    }
    else {
        redirectToLogin();
    }
    //Set sự kiện cho nút logout
    $("#logout").on("click", function () {
        redirectToLogin();
    });
    //Set sự kiện cho nút logout
    $(".info ").on("click", ".btn-logout", function () {
        redirectToLogin();
    });

    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("Token", "", 1);
        window.location.href = "Login.html";
    }

    //Hiển thị thông tin người dùng
    function displayUser(data) {
        $("#name").html(data.firstname);
        $("#id").html(data.userId);
        $("#username").html(data.username);
        $("#firstname").html(data.firstname);
        $("#lastname").html(data.lastname);
    };

    //Hiển thị thông tin người dùng
    function displayUserMenu(data) {
        let vAuth = "";
        if (data.authorities.includes("ROLE_ADMIN")) {
            vAuth = "Adminstrator";
        }
        else if (data.authorities.includes("ROLE_MANAGER")) {
            vAuth = "Manager"
        }
        else if (data.authorities.includes("ROLE_HOMESELLER")) {
            vAuth = "Home Seller"
        }
        else {
            vAuth = "Employee"
        }
        var vEmployeeInf = getEmployeeById(data.userId);
        console.log(vEmployeeInf);
        let vHTMl = `
                <a class="text-light" href="info.html">${data.firstname} ${data.lastname}</a>
                <p class="text-light" > ${vAuth} </p>
                <a href="#" class="text-danger btn-logout">Logout</a> 
        `
        $(".user-panel .info").append(vHTMl);
        $(".user-panel .img-circle")
        .attr("src", `${gBASE_URL}/images/${vEmployeeInf.photo}`)
        .attr("style","width: 40px; height: 40px")

    };

    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm setCookie
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    if (token == "" || gInfo.authorities.length == 0
    ) {
        Toast.fire({
            icon: 'error',
            title: "Can't access. Please Contact Manager/Adminstrator"
        })
        setTimeout(() => { redirectToLogin() }, 1500)
    }
    if (!gInfo.authorities.includes("ROLE_ADMIN" || "ROLE_MANEGER") || token == "") {
        $(".menu-user, .menu-region, .nav-project, .nav-investor, .nav-regionlink, .nav-utility, .nav-contractor, .nav-design-unit,.nav-region-link").hide();
    }
    // if(gInfo.authorities.includes("ROLE_HOMESELLER")) {
    //     $(".main-sidebar .nav-employee").show();
    // }



})
function convertVNToEN(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;

}
//Hàm yêu câu Get Employee By Id từ server
function getEmployeeById(paramEmployeeId) {
    "use strict"
    let vEmployeeInf = "";
    $.ajax({
        url: gBASE_URL + "/employees/" + paramEmployeeId,
        dataType: "json",
        type: "GET",
        headers: gHeader,
        async: false,
        success: function (res) {
            // console.log(res);
            vEmployeeInf = res;
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
    return vEmployeeInf;
}

"use strict"
const gBASE_URL = "http://localhost:8080"
var gProjectList = "";
var gProvinceList = "";
var gDistrictList = "";
var gWardList = "";
var gStreetList = "";
var gInvetorList = "";
var gContractorList = "";
var gDesignUnitList = "";
var gSTT = 1;
var gProjectId = "";
var gProvinceId = "";
var gDistrictId = "";
var gWardId = "";
//Định nghĩa bảng
var gNameCol = ["id", "name", "province", "district", "acreage", "contructArea", "numBlock", "numFloors", "numApartment",
    "investor", "constructionContractor", "designUnit", "slogan", "description", "utilities", "regionLink",
    "photo", "ward", "street", "address", "lat", "lng", "action"];
const gCOL_ID = 0;
const gCOL_NAME = 1;
const gCOL_PROVINCE = 2;
const gCOL_DISTRICT = 3;
const gCOL_ACREAGE = 4;
const gCOL_CONTRUCTION_AREA = 5;
const gCOL_NUM_BLOCK = 6;
const gCOL_NUM_FLOORS = 7;
const gCOL_NUM_APARTMENT = 8;
const gCOL_INVESTOR = 9;
const gCOL_CONTRACTOR = 10;
const gCOL_DESIGN_UNIT = 11;
const gCOL_SLOGAN = 12;
const gCOL_DESCRIPTION = 13;
const gCOL_UTILITIES = 14;
const gCOL_REGION_LINK = 15;
const gCOL_PHOTO = 16;
const gCOL_WARD = 17;
const gCOL_STREET = 18;
const gCOL_ADDRESS = 19;
const gCOL_LAT = 20;
const gCOL_LNG = 21;
const gCOL_ACTION = 22;
var gTableProject = $("#table-project").DataTable({
    columns: [
        { data: gNameCol[gCOL_ID] },
        { data: gNameCol[gCOL_NAME] },
        { data: gNameCol[gCOL_PROVINCE] },
        { data: gNameCol[gCOL_DISTRICT] },
        { data: gNameCol[gCOL_ACREAGE] },
        { data: gNameCol[gCOL_CONTRUCTION_AREA] },
        { data: gNameCol[gCOL_NUM_BLOCK] },
        { data: gNameCol[gCOL_NUM_FLOORS] },
        { data: gNameCol[gCOL_NUM_APARTMENT] },
        { data: gNameCol[gCOL_INVESTOR] },
        { data: gNameCol[gCOL_CONTRACTOR] },
        { data: gNameCol[gCOL_DESIGN_UNIT] },
        { data: gNameCol[gCOL_SLOGAN] },
        { data: gNameCol[gCOL_DESCRIPTION] },
        { data: gNameCol[gCOL_UTILITIES] },
        { data: gNameCol[gCOL_REGION_LINK] },
        { data: gNameCol[gCOL_PHOTO] },
        { data: gNameCol[gCOL_WARD] },
        { data: gNameCol[gCOL_STREET] },
        { data: gNameCol[gCOL_ADDRESS] },
        { data: gNameCol[gCOL_LAT] },
        { data: gNameCol[gCOL_LNG] },
        { data: gNameCol[gCOL_ACTION] },
    ],
    columnDefs: [
        {
            targets: gCOL_ID,
            render: function (data) {
                return `
                <div class="text-center">
                    ${gSTT++}
                    <i data-id="${data}" class="far fa-edit text-primary btn-edit" style="cursor: pointer"></i>
                    <i data-id="${data}" class="fas fa-trash text-primary btn-delete" style="cursor: pointer"></i>
                </div>
                `
            },
            width: "5px"
        },
        {
            targets: gCOL_PROVINCE,
            render: function (data) {
                if (data != null) {
                    let vProvince = gProvinceList.filter(e => e.id == data)
                    return vProvince[0].name;
                }
                else {
                    return ""
                }
            }
        },
        {
            targets: gCOL_WARD,
            render: function (data) {
                if (data != null) {
                    let vWard = gWardList.filter(e => e.id == data)
                    return vWard[0].name;
                }
                else {
                    return ""
                }
            }
        },
        {
            targets: gCOL_STREET,
            render: function (data) {
                if (data != null) {
                    let vStreet = gStreetList.filter(e => e.id == data)
                    return vStreet[0].name;
                }
                else {
                    return ""
                }
            }
        },
        {
            targets: gCOL_DISTRICT,
            render: function (data) {
                if (data != null) {
                    let vDistrict = gDistrictList.filter(e => e.id == data)
                    return vDistrict[0].name;
                }
                else {
                    return ""
                }
            }
        },
        {
            targets: gCOL_INVESTOR,
            render: function (data) {
                if (data != null & data > 0) {
                    let vInvetor = gInvetorList.filter(e => e.id == data)
                    return vInvetor[0].name;
                }
                else {
                    return ""
                }
            }
        },
        {
            targets: gCOL_CONTRACTOR,
            render: function (data) {
                if (data != null & data > 0) {
                    let vContract = gContractorList.filter(e => e.id == data)
                    return vContract[0].name;
                }
                else {
                    return ""
                }
            }
        },
        {
            targets: gCOL_DESIGN_UNIT,
            render: function (data) {
                if (data != null & data > 0) {
                    let vDesignUnit = gDesignUnitList.filter(e => e.id == data)
                    return vDesignUnit[0].name;
                }
                else {
                    return ""
                }
            }
        },
        {
            className: 'dtr-control',
            orderable: false,
            targets: -1
        },
        {
            targets: gCOL_ACTION,
            defaultContent: ``
        },
        {
            targets: gCOL_PHOTO,
            render: function (data) {
                return `<img src="dist/picture/${data}" style="height: 100px; width: 100px"></img>`;
            }
        }
    ],
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    autoWidth: false,
    responsive: {
        details: {
            type: 'column',
            target: -1
        }
    },
    stateSave: true,
})

$(document).ready(function () {
    let vURLString = window.location.href;
    console.log(vURLString);
    if (vURLString.indexOf("provinceId")!=-1) {
        let vProvinceId = new URL(vURLString).searchParams.get("provinceId")
        gProvinceId = vProvinceId;
        console.log(vProvinceId)
        getProjectByProvinceId(vProvinceId);
    }
    if (vURLString.indexOf("districtId")!=-1) {
        let vDistrictId = new URL(vURLString).searchParams.get("districtId")
        gDistrictId = vDistrictId;
        console.log(vDistrictId)
        getProjectByDistrictId(gDistrictId);
    }
    if (vURLString.indexOf("wardId")!=-1) {
        let vWardId = new URL(vURLString).searchParams.get("wardId")
        gWardId = vWardId;
        console.log(vWardId)
        getProjectByWardId(vWardId);
    }
    getAllInvestor();
    getAllContractor();
    getAllDesignUnit();
    getAllProvince();
    getAllDistrict();
    getAllWard();
    getAllStreet();
    // getAllEstateByUser();
    loadDataToTable(gProjectList);
})
$("#select-province").on("change", function () { //Thay đổi Tỉnh thành
    onChangeProvince($(this).val());
})

$("#select-district").on("change", function () { //Thay đổi quận huyện
    onChangeDistrict($(this).val());
})
$("#table-project tbody").on("click", "tr.odd,tr.even", function () { //Click dòng
    changeColorRow(this);
})
$("#btn-add-project-modal").on("click", function () { //Nút hiện thị Add Modal Project
    $("#add-project-modal").modal("show");
})
$("#btn-confirm-add").on("click", function () { //Nút Add Project
    onBtnAddProjectClick();
})
$(document).on("click", ".btn-edit", function () { //Nút hiện thị Update Modal
    gProjectId = $(this).data("id");
    getProjectById(gProjectId);
    $("#edit-project-modal").modal("show");
})
$("#btn-confirm-update").on("click", function () { //Nút Add Project
    onBtnUpdateProjectClick();
})

$(document).on("click", ".btn-delete", function () { //Nút Delete Project
    gProjectId = $(this).data("id");
    getProjectById(gProjectId);
    $("#delete-project-modal").modal("show");
})
$("#btn-confirm-delete-project").on("click", function () { //Nút Xác Nhận Delete Project
    onBtnConfirmDeleteClick();
    $("#delete-project-modal").modal("hide");
})

//Hàm xử lý nút Add Project
function onBtnAddProjectClick() {
    "use strict"
    let vProjectObj = {
        name: "",
        province: "",
        district: "",
        ward: "",
        street: "",
        address: "",
        description: "",
        slogan: "",
        acreage: "",
        constructArea: "",
        numBlock: "",
        numFloors: "",
        numApartment: "",
        apartmenttArea: "",
        investor: "",
        constructionContractor: "",
        designUnit: "",
        utilities: "",
        regionLink: "",
        photo: "",
        lat: "",
        lng: "",
    }
    getDataByFormAddModal(vProjectObj);
    let vCheck = validateProjectData(vProjectObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/projects",
            type: "POST",
            data: JSON.stringify(vProjectObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log(res);
                Toast.fire({
                    icon: 'success',
                    title: 'Add Project Success!!!'
                })
                gSTT = 1;
                getAllProject();
                loadDataToTable(gProjectList);
                $("#add-project-modal").modal("hide");
                refeshForm();
            },
            error: function (err) {
                Toast.fire({
                    icon: 'error',
                    title: '"Add Project Failed: ' + err.responseText
                })
            }
        })
    }
}

//Hàm kiểm tra dữ liệu Project
function validateProjectData(paramProjectObj) {
    let vResult = true;
    if (paramProjectObj.name == "") {
        alert("Name Not Empty");
        vResult = false;
    }
    else if (paramProjectObj.province == -1) {
        alert("Province Not Empty");
        vResult = false;
    }
    else if (paramProjectObj.district == -1) {
        alert("District Not Empty");
        vResult = false;
    }
    else if (paramProjectObj.investor == -1) {
        alert("Investor Not Empty");
        vResult = false;
    }
    else if (paramProjectObj.constructionContractor == -1) {
        alert("Contractor Not Empty");
        vResult = false;
    }
    else if (paramProjectObj.designUnit == -1) {
        alert("Design Unit Not Empty");
        vResult = false;
    }
    return vResult;
}
//Hàm Truy xuất dữ liệu từ form Add Modal
function getDataByFormAddModal(paramProjectObj) {
    "use strict"
    paramProjectObj.name = $("#inp-name-add-modal").val().trim();
    paramProjectObj.province = $("#select-province-add-modal").val();
    paramProjectObj.district = $("#select-district-add-modal").val();
    paramProjectObj.ward = $("#select-ward-add-modal").val();
    paramProjectObj.address = $("#inp-address-add-modal").val().trim();
    paramProjectObj.street = $("#select-street-add-modal").val();
    paramProjectObj.description = $("#textarea-description-add-modal").val().trim();
    paramProjectObj.slogan = $("#inp-slogan-add-modal").val().trim();
    paramProjectObj.acreage = $("#inp-acreage-add-modal").val().trim();
    paramProjectObj.constructArea = $("#inp-construct-area-add-modal").val().trim();
    paramProjectObj.numBlock = $("#inp-num-block-add-modal").val().trim();
    paramProjectObj.numFloors = $("#inp-num-floors-add-modal").val().trim();
    paramProjectObj.numApartment = $("#inp-num-apartment-add-modal").val().trim();
    paramProjectObj.apartmenttArea = $("#inp-apartmentt-area-add-modal").val().trim();
    paramProjectObj.investor = $("#select-investor-add-modal").val();
    paramProjectObj.constructionContractor = $("#select-contractor-add-modal").val();
    paramProjectObj.designUnit = $("#select-design-unit-add-modal").val();
    paramProjectObj.utilities = $("#inp-utilites-add-modal").val().trim();
    paramProjectObj.regionLink = $("#inp-region-link-add-modal").val().trim();
    paramProjectObj.photo = $("#inp-photo-add-modal").val().trim();
    paramProjectObj.lat = $("#inp-lat-add-modal").val().trim();
    paramProjectObj.lng = $("#inp-lng-add-modal").val().trim();
}
//Hàm Truy xuất dữ liệu từ form Edit Modal
function getDataByFormEditModal(paramProjectObj) {
    "use strict"
    paramProjectObj.name = $("#inp-name-edit-modal").val().trim();
    paramProjectObj.province = $("#select-province-edit-modal").val();
    paramProjectObj.district = $("#select-district-edit-modal").val();
    paramProjectObj.ward = $("#select-ward-edit-modal").val();
    paramProjectObj.address = $("#inp-address-edit-modal").val().trim();
    paramProjectObj.street = $("#select-street-edit-modal").val();
    paramProjectObj.description = $("#textarea-description-edit-modal").val().trim();
    paramProjectObj.slogan = $("#inp-slogan-edit-modal").val().trim();
    paramProjectObj.acreage = $("#inp-acreage-edit-modal").val().trim();
    paramProjectObj.constructArea = $("#inp-construct-area-edit-modal").val().trim();
    paramProjectObj.numBlock = $("#inp-num-block-edit-modal").val().trim();
    paramProjectObj.numFloors = $("#inp-num-floors-edit-modal").val().trim();
    paramProjectObj.numApartment = $("#inp-num-apartment-edit-modal").val().trim();
    paramProjectObj.apartmenttArea = $("#inp-apartmentt-area-edit-modal").val().trim();
    paramProjectObj.investor = $("#select-investor-edit-modal").val();
    paramProjectObj.constructionContractor = $("#select-contractor-edit-modal").val();
    paramProjectObj.designUnit = $("#select-design-unit-edit-modal").val();
    paramProjectObj.utilities = $("#inp-utilites-edit-modal").val().trim();
    paramProjectObj.regionLink = $("#inp-region-link-edit-modal").val().trim();
    paramProjectObj.photo = $("#inp-photo-edit-modal").val().trim();
    paramProjectObj.lat = $("#inp-lat-edit-modal").val().trim();
    paramProjectObj.lng = $("#inp-lng-edit-modal").val().trim();
}

//Hàm load dữ liệu vào Bảng
function loadDataToTable(paramProject) {
    "use strict"
    gTableProject.clear();
    gTableProject.rows.add(paramProject);
    gTableProject.draw();
    gTableProject.buttons().container().prependTo('.table-tool');
}

//Hàm thay đổi màu nút
function changeColorRow(paramRow) {
    $("#table-project tbody tr").removeClass("table-primary");
    $(paramRow).addClass("table-primary");

}
//Hàm yêu câu Get Project By Id từ server
function getProjectById(paramProjectId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/projects/" + paramProjectId,
        dataType: "json",
        type: "GET",
        headers: gHeader,
        success: function (res) {
            console.log(res);
            loadDataProjectToForm(res);
            Toast.fire({
                icon: 'success',
                title: 'Get Project Success!!!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: 'Get Project Failed: ' + err.responseText
            })
        }
    })
}
//Hàm load dữ liệu lên Form
function loadDataProjectToForm(paramProjectObj) {
    "use strict"
    $("#inp-lat-edit-modal").val(paramProjectObj.lat);
    $("#inp-lng-edit-modal").val(paramProjectObj.lng);
    $("#inp-name-edit-modal").val(paramProjectObj.name);
    $("#select-province-edit-modal").val(paramProjectObj.province).change();
    $("#select-district-edit-modal").val(paramProjectObj.district).change();
    $("#select-ward-edit-modal").val(paramProjectObj.ward).change();
    $("#select-street-edit-modal").val(paramProjectObj.street).change();
    $("#inp-address-edit-modal").val(paramProjectObj.address);
    $("#textarea-description-edit-modal").val(paramProjectObj.description);
    $("#inp-slogan-edit-modal").val(paramProjectObj.slogan);
    $("#inp-acreage-edit-modal").val(paramProjectObj.acreage);
    $("#inp-construct-area-edit-modal").val(paramProjectObj.constructArea);
    $("#inp-num-block-edit-modal").val(paramProjectObj.numBlock);
    $("#inp-num-floors-edit-modal").val(paramProjectObj.numFloors);
    $("#inp-num-apartment-edit-modal").val(paramProjectObj.numApartment);
    $("#inp-apartmentt-area-edit-modal").val(paramProjectObj.apartmenttArea);
    $("#select-investor-edit-modal").val(paramProjectObj.investor).change();
    $("#select-contractor-edit-modal").val(paramProjectObj.constructionContractor).change();
    $("#select-design-unit-edit-modal").val(paramProjectObj.designUnit).change();
    $("#inp-utilite-edit-modals").val(paramProjectObj.utilities);
    $("#inp-region-link-edit-modal").val(paramProjectObj.regionLink);
    // $("#inp-photo-edit-modal").val(paramProjectObj.photo);
}
//Hàm xử lý nút Update Project
function onBtnUpdateProjectClick() {
    "use strict"
    console.log("Nút Update được ấn")
    let vProjectObj = {
        name: "",
        province: "",
        district: "",
        ward: "",
        street: "",
        address: "",
        description: "",
        slogan: "",
        acreage: "",
        constructArea: "",
        numBlock: "",
        numFloors: "",
        numApartment: "",
        apartmenttArea: "",
        investor: "",
        constructionContractor: "",
        designUnit: "",
        utilities: "",
        regionLink: "",
        photo: "",
        lat: "",
        lng: "",
    }
    getDataByFormEditModal(vProjectObj);
    let vCheck = validateProjectData(vProjectObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/projects/" + gProjectId,
            type: "PUT",
            data: JSON.stringify(vProjectObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log(res);
                gSTT = 1;
                getAllProject();
                loadDataToTable(gProjectList);
                Toast.fire({
                    icon: 'success',
                    title: 'Update Project Success!!!'
                })
                $("#edit-project-modal").modal("hide");
            },
            error: function (err) {
                Toast.fire({
                    icon: 'error',
                    title: 'Update Project Failed: ' + err.responseText
                })
            }
        })
    }
}
//Hàm xử lý nút Xác nhận xóa Project
function onBtnConfirmDeleteClick() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/projects/" + gProjectId,
        type: "DELETE",
        headers: gHeader,
        success: function (res) {
            gSTT = 1;
            getAllProject();
            loadDataToTable(gProjectList);
            Toast.fire({
                icon: 'success',
                title: 'Delete Project Success!!!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: 'Delete Project Failed: ' + err.responseText
            })
        }
    })
}
// -----------------------------------------------------------------------------------------------------------------------------------------//
//Hàm xử lý on change Select Province
function onChangeProvince(paramProvinceCode) {
    $("#select-district").find("option").remove().end().append('<option selected value="">Select District</option>');
    $("#select-ward").find("option").remove().end().append('<option selected value="">Select Ward</option>');
    $("#select-street").find("option").remove().end().append('<option selected value="">Select Street</option>');
    if (paramProvinceCode != "") {
        getDistrictsOfProvince(paramProvinceCode);
    }
}

//Hàm xử lý thay đổi District
function onChangeDistrict(paramDistrictId) {
    $("#select-ward").find("option").remove().end().append('<option selected value="">Select Ward</option>');
    $("#select-Street").find("option").remove().end().append('<option selected value="">Select Street</option>');
    if (paramDistrictId != "") {
        getWardsOfDistrict(paramDistrictId);
        getStreetsOfDistrict(paramDistrictId);
    }
}
//Hàm ajax lấy dữ liệu Province
function getAllProvince() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/provinces",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            gProvinceList = res
            console.log(res);
            res.forEach(element => {
                $("#select-province").append($("<option>", {
                    value: element.id,
                    text: element.name,
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu District
function getAllDistrict() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/districts",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            gDistrictList = res
            console.log(res);
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu Ward
function getAllWard() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/wards",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            gWardList = res
            console.log(res);
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu Street
function getAllStreet() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/streets",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            gStreetList = res;
            console.log(res);
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm yêu cầu ajax lấy Ward của District
function getWardsOfDistrict(paramDistrictId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/districts/" + paramDistrictId + "/wards",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            res.sort((a, b) => a.name - b.name);
            res.forEach(element => {
                $("#select-ward").append($("<option>", {
                    value: element.id,
                    text: [element.prefix, element.name].join(" "),
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm yêu cầu ajax get all District
function getDistrictsOfProvince(pProvinceId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/provinces/" + pProvinceId + "/districts",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            console.log(res);
            res.forEach(element => {
                $("#select-district").append($("<option>", {
                    value: element.id,
                    text: [element.prefix, element.name].join(" ")
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm yêu cầu ajax lấy Street của District
function getStreetsOfDistrict(paramDistrictId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/districts/" + paramDistrictId + "/streets",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            res.sort((a, b) => a.name - b.name);
            res.forEach(element => {
                $("#select-street").append($("<option>", {
                    value: element.id,
                    text: [element.prefix, element.name].join(" "),
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu Investor
function getAllInvestor() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/investors",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            console.log(res);
            gInvetorList = res;
            res.forEach(element => {
                $("#select-investor-edit-modal, #select-investor-add-modal ").append($("<option>", {
                    value: element.id,
                    text: element.name,
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu Contractor
function getAllContractor() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/contractors",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            console.log(res);
            gContractorList = res;
            res.forEach(element => {
                $("#select-contractor-edit-modal, #select-contractor-add-modal").append($("<option>", {
                    value: element.id,
                    text: element.name,
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu Design Unit
function getAllDesignUnit() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/designunits",
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            console.log(res);
            gDesignUnitList = res;
            res.forEach(element => {
                $("#select-design-unit-edit-modal, #select-design-unit-add-modal").append($("<option>", {
                    value: element.id,
                    text: element.name,
                }));
            });
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm ajax lấy dữ liệu Project
function getProjectByProvinceId(pProvinceId) {
    console.log("Chạy")
    "use strict"
    $.ajax({
        url: gBASE_URL + "/projects?provinceId=" + pProvinceId,
        dataType: "json",
        type: "GET",
        async: false,
        headers: gHeader,
        success: function (res) {
            console.log(res);
            gProjectList = res;
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Clear Form
function refeshForm() {
    $(document).find("#add-project-modal input").val("");
    $(document).find("#add-project-modal textarea").val("");
    $(document).find("#add-project-modal select").val("").change();
}
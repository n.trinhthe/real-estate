"use strict"
const gBASE_URL = "http://localhost:8080";
var gRegionLinkList = [];

var gSTT = 1;
var gRegionLinkId = "";
//Định nghĩa bảng
var gNameCol = ["id", "name", "description", "photo","address","lat","lng"];
const gCOL_ID = 0;
const gCOL_NAME = 1;
const gCOL_DESCRIPTION = 2;
const gCOL_PHOTO = 3;
const gCOL_ADDRESS = 4;
const gCOL_LAT = 5;
const gCOL_LNG = 6;
var gTableRegionLink = $("#table-regionlink").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_NAME] },
    { data: gNameCol[gCOL_DESCRIPTION] },
    { data: gNameCol[gCOL_PHOTO] },
    { data: gNameCol[gCOL_ADDRESS] },
    { data: gNameCol[gCOL_LAT] },
    { data: gNameCol[gCOL_LNG] },
  ],
  columnDefs: [
    {
      targets: gCOL_ID,
      render: function (data) {
        return `
        <div class="text-center">
            ${gSTT++}
            <i data-id="${data}" class="far fa-edit text-primary btn-edit" style="cursor: pointer"></i>
            <i data-id="${data}" class="fas fa-trash text-primary btn-delete" style="cursor: pointer"></i>
        </div>
        `
      }
    },
    {
      targets: gCOL_PHOTO,
      render: function (data) {
        return `<img src="dist/picture/${(data == null || data == '') ? 'placeholder.jpg' : data}" style="height: 100px; width: 100px"></img>`;
      }
    },
  ],
  autoWidth: false,
  searching: true,
  // dom: "rtip",
  stateSave: true,
  buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
})

$(document).ready(function () {
  getAllRegionLink();
  loadDataToTable(gRegionLinkList);
})
$("#table-regionlink tbody").on("click", "tr.odd,tr.even", function () { //Click dòng
  changeColorRow(this);
})
$(document).on("click",".btn-edit", function () { //Nút Update RegionLink
  gRegionLinkId = $(this).data("id");
  getRegionLinkById(gRegionLinkId);
  $("#edit-regionlink-modal").modal("show");
})
$("#btn-confirm-update").on("click", function(){
  onBtnUpdateRegionLinkClick();
})
$(document).on("click",".btn-delete", function () { //Nút Delete RegionLink
  gRegionLinkId = $(this).data("id");
  $("#delete-regionlink-modal").modal("show");
})
$("#btn-confirm-delete-regionlink").on("click", function () { //Nút Xác Nhận Delete RegionLink
  onBtnConfirmDeleteClick();
  $("#delete-regionlink-modal").modal("hide");
})
$("#btn-add-regionlink").on("click", function () { //Nút Add RegionLink
  $("#add-regionlink-modal").modal("show");
})
$("#btn-confirm-add").on("click", function () { //Nút Confirm Add RegionLink
  onBtnAddRegionLinkClick();
})
//Hàm yêu cầu lấy dữ liệu All RegionLink từ server
function getAllRegionLink() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/regionlinks",
    dataType: "json",
    type: "GET",
    async: false,
    headers: gHeader,
    success: function (res) {
      console.log(res);
      gRegionLinkList = res;
    },
    error: function (err) {
      Toast.fire({
        icon: 'error',
        title: "Add Region Link Failed: " + err.responseJSON.error
        
      })
      console.log(err);
    }
  })
}
//Hàm load dữ liệu vào Bảng
function loadDataToTable(paramRegionLink) {
  "use strict"
  gTableRegionLink.clear();
  gTableRegionLink.rows.add(paramRegionLink);
  gTableRegionLink.draw();
}
//Hàm xử lý nút Add RegionLink
function onBtnAddRegionLinkClick() {
  "use strict"
  let vRegionLinkObj = {
    name: "",
    description: "",
    photo: "",
    address: "",
    lat: "",
    lng: ""
  }
  getDataByFormAddModal(vRegionLinkObj);
  let vCheck = validateRegionLinkData(vRegionLinkObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/regionlinks",
      type: "POST",
      data: JSON.stringify(vRegionLinkObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      headers: gHeader,
      success: function (res) {
        console.log(res);
        gSTT = 1;
        getAllRegionLink();
        loadDataToTable(gRegionLinkList);
        refeshForm();
        Toast.fire({
          icon: 'success',
          title: "Add Region Link Success!!!"
        })
        $("#add-regionlink-modal").modal("hide");
      },
      error: function (err) {
        Toast.fire({
          icon: 'error',
          title: "Add Region Link Failed: " + err.responseJSON.error
        })
      }
    })
  }
}
//Hàm Truy xuất dữ liệu từ form Edit modal
function getDataByFormEditModal(paramRegionLinkObj) {
  "use strict"
  paramRegionLinkObj.name = $("#inp-name-edit-modal").val().trim();
  paramRegionLinkObj.description = $("#inp-description-edit-modal").val().trim();
  paramRegionLinkObj.photo = $("#inp-photo-edit-modal").val().split('\\').pop();
  paramRegionLinkObj.address = $("#inp-address-edit-modal").val();
  paramRegionLinkObj.lat = $("#inp-lat-edit-modal").val();
  paramRegionLinkObj.lng = $("#inp-lng-edit-modal").val();
}

//Hàm Truy xuất dữ liệu từ form Edit modal
function getDataByFormAddModal(paramRegionLinkObj) {
  "use strict"
  paramRegionLinkObj.name = $("#inp-name-add-modal").val().trim();
  paramRegionLinkObj.description = $("#inp-description-add-modal").val().trim();
  paramRegionLinkObj.photo = $("#inp-photo-add-modal").val().split('\\').pop();
  paramRegionLinkObj.address = $("#inp-address-add-modal").val();
  paramRegionLinkObj.lat = $("#inp-lat-add-modal").val();
  paramRegionLinkObj.lng = $("#inp-lng-add-modal").val();
}
//Hàm kiểm tra dữ liệu RegionLink
function validateRegionLinkData(paramRegionLinkObj) {
  let vResult = true;
  if (paramRegionLinkObj.name == "") {
    Toast.fire({
      icon: 'error',
      title: "Name is empty"
    })
    vResult = false;
  }
  return vResult;
}
//Hàm yêu câu Get RegionLink By Id từ server
function getRegionLinkById(paramRegionLinkId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/regionlinks/" + paramRegionLinkId,
    dataType: "json",
    type: "GET",
    headers: gHeader,
    success: function (res) {
      console.log(res);
      loadDataRegionLinkToForm(res);
      Toast.fire({
        icon: 'success',
        title: "Get Region Link Success!!!"
      })
    },
    error: function (err) {
      Toast.fire({
        icon: 'error',
        title: "Get Region Link Failed: " + err.responseJSON.error
      })
    }
  })
}
//Hàm load dữ liệu lên Form
function loadDataRegionLinkToForm(paramRegionLinkData) {
  "use strict"
  $("#inp-name-edit-modal").val(paramRegionLinkData.name);
  $("#inp-description-edit-modal").val(paramRegionLinkData.description);
  // $("#inp-photo").val(paramRegionLinkData.photo);
  $("#inp-address-edit-modal").val(paramRegionLinkData.address);
  $("#inp-lat-edit-modal").val(paramRegionLinkData.lat);
  $("#inp-lng-edit-modal").val(paramRegionLinkData.lng);
}
//Hàm thay đổi màu hàng
function changeColorRow(paramRow) {
  $("#table-regionlink tbody tr").removeClass("table-primary");
  $(paramRow).addClass("table-primary");

}
//Hàm xử lý nút Update RegionLink
function onBtnUpdateRegionLinkClick() {
  "use strict"
  console.log("Nút Update được ấn")
  let vRegionLinkObj = {
    name: "",
    description: "",
    photo: "",
    address: "",
    lat: "",
    lng: ""
  }
  getDataByFormEditModal(vRegionLinkObj);
  let vCheck = validateRegionLinkData(vRegionLinkObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/regionlinks/" + gRegionLinkId,
      type: "PUT",
      data: JSON.stringify(vRegionLinkObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      headers: gHeader,
      success: function (res) {
        console.log(res);
        gSTT = 1;
        getAllRegionLink();
        loadDataToTable(gRegionLinkList);
        Toast.fire({
          icon: 'success',
          title: "Update Region Link Success!!!"
        })
        $("#edit-regionlink-modal").modal("hide");
      },
      error: function (err) {
        Toast.fire({
          icon: 'error',
          title: "Update Region Link Failed: " + err.responseJSON.error
        })
      }
    })
  }
}
//Hàm xử lý nút Xác nhận xóa RegionLink
function onBtnConfirmDeleteClick() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/regionlinks/" + gRegionLinkId,
    type: "DELETE",
    headers: gHeader,
    success: function (res) {
      gSTT = 1;
      getAllRegionLink();
      loadDataToTable(gRegionLinkList);
      Toast.fire({
        icon: 'success',
        title: "Delete RegionLink Success!!!"
      })
    },
    error: function (err) {
      Toast.fire({
        icon: 'error',
        title: "Delete RegionLink Failed: " + err.responseJSON.error
      })
    }
  })
  refeshForm();
}
//Clear Form
function refeshForm() {
  $(document).find("#add-regionlink-modal input").val("");
}





















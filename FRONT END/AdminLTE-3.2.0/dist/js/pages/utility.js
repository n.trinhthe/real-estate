"use strict"
const gBASE_URL = "http://localhost:8080";
var gUtilityList = [];
var gSTT = 1;
var gUtilityId = "";
//Định nghĩa bảng
var gNameCol = ["id", "name", "description", "photo"];
const gCOL_ID = 0;
const gCOL_NAME = 1;
const gCOL_DESCRIPTION = 2;
const gCOL_PHOTO = 3;
var gTableUtility = $("#table-utility").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_NAME] },
    { data: gNameCol[gCOL_DESCRIPTION] },
    { data: gNameCol[gCOL_PHOTO] },
  ],
  columnDefs: [
    {
      targets: gCOL_ID,
      render: function (data) {
        return `
                <div class="text-center">
                    ${gSTT++}
                    <i data-id="${data}" class="far fa-edit text-primary btn-edit" style="cursor: pointer"></i>
                    <i data-id="${data}" class="fas fa-trash text-primary btn-delete" style="cursor: pointer"></i>
                </div>
                `
      }
    },
    {
      targets: gCOL_PHOTO,
      render: function (data) {
        return `<img src="dist/picture/${(data == null || data == '') ? 'placeholder.jpg' : data}" style="height: 100px; width: 100px"></img>`;
      }
    },
  ],
  autoWidth: false,
  // dom: "rtip",
  buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
  stateSave: true,
  searching: true,
})

$(document).ready(function () {
  getAllUtility();
  loadDataToTable(gUtilityList);
})
$("#table-utility tbody").on("click", "tr.odd,tr.even", function () { //Click dòng
  changeColorRow(this);
})
$(document).on("click", ".btn-edit", function () { //Nút Update Utility
  gUtilityId = $(this).data("id");
  getUtilityById(gUtilityId);
  $("#edit-utility-modal").modal("show");
})
$("#btn-confirm-update").on("click", function () {
  onBtnUpdateUtilityClick();
})
$(document).on("click", ".btn-delete", function () { //Nút Update Utility
  gUtilityId = $(this).data("id");
  $("#delete-utility-modal").modal("show");
})
$("#btn-confirm-delete-utility").on("click", function () { //Nút Xác Nhận Delete Utility
  onBtnConfirmDeleteClick();
  $("#delete-utility-modal").modal("hide");
})
$("#btn-add-utility").on("click", function(){
  $("#add-utility-modal").modal("show");
})
$("#btn-confirm-add").on("click", function () { //Nút Add Utility
  onBtnAddUtilityClick();
})

//Hàm yêu cầu lấy dữ liệu All Utility từ server
function getAllUtility() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/utilities",
    dataType: "json",
    type: "GET",
    async: false,
    headers: gHeader,
    success: function (res) {
      console.log(res);
      gUtilityList = res;
    },
    error: function (err) {
      Toast.fire({
        icon: 'error',
        title: "Get All Utility Failed: " + err.responseText
      })
    }
  })
}
//Hàm load dữ liệu vào Bảng
function loadDataToTable(paramUtility) {
  "use strict"
  gTableUtility.clear();
  gTableUtility.rows.add(paramUtility);
  gTableUtility.draw();
  gTableUtility.buttons().container().prependTo('.table-tool');
}
//Hàm xử lý nút Add Utility
function onBtnAddUtilityClick() {
  "use strict"
  let vUtilityObj = {
    name: "",
    description: "",
    photo: ""
  }
  getDataByFormAddModal(vUtilityObj);
  let vCheck = validateUtilityData(vUtilityObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/utilities",
      type: "POST",
      data: JSON.stringify(vUtilityObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      headers: gHeader,
      success: function (res) {
        console.log(res);
        Toast.fire({
          icon: 'success',
          title: "Add Utility Success!!!"
        })
        gSTT = 1;
        getAllUtility();
        loadDataToTable(gUtilityList);
        $("#add-utility-modal").modal("hide");
        refeshForm();
      },
      error: function (err) {
        Toast.fire({
          icon: 'error',
          title: "Add Utility Failed: " + err.responseText
        })
      }
    })
  }
}
//Hàm Truy xuất dữ liệu từ form Edit Modal
function getDataByFormEditModal(paramUtilityObj) {
  "use strict"
  paramUtilityObj.name = $("#inp-name-edit-modal").val().trim();
  paramUtilityObj.description = $("#inp-description-edit-modal").val().trim();
  paramUtilityObj.photo = $("#inp-photo-edit-modal").val().split('\\').pop();
}
//Hàm Truy xuất dữ liệu từ form Add Modal
function getDataByFormAddModal(paramUtilityObj) {
  "use strict"
  paramUtilityObj.name = $("#inp-name-add-modal").val().trim();
  paramUtilityObj.description = $("#inp-description-add-modal").val().trim();
  paramUtilityObj.photo = $("#inp-photo-add-modal").val().split('\\').pop();
}
//Hàm kiểm tra dữ liệu Utility
function validateUtilityData(paramUtilityObj) {
  let vResult = true;
  if (paramUtilityObj.name == "") {
    alert("Name chưa được nhập!");
    vResult = false;
  }
  return vResult;
}
//Hàm yêu câu Get Utility By Id từ server
function getUtilityById(paramUtilityId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/utilities/" + paramUtilityId,
    dataType: "json",
    type: "GET",
    headers: gHeader,
    success: function (res) {
      console.log(res);
      loadDataUtilityToForm(res);
    },
    error: function (err) {
      Toast.fire({
        icon: 'error',
        title: "Get Utility Failed: " + err.responseText
      })
    }
  })
}
//Hàm load dữ liệu lên Form
function loadDataUtilityToForm(paramUtilityData) {
  "use strict"
  $("#inp-name-edit-modal").val(paramUtilityData.name);
  $("#inp-description-edit-modal").val(paramUtilityData.description);
  $("#inp-photo-edit-modal").val(paramUtilityData.photo);
}
//Hàm thay đổi màu hàng
function changeColorRow(paramRow) {
  $("#table-utility tbody tr").removeClass("table-primary");
  $(paramRow).addClass("table-primary");

}
//Hàm xử lý nút Update Utility
function onBtnUpdateUtilityClick() {
  "use strict"
  console.log("Nút Update được ấn")
  let vUtilityObj = {
    name: "",
    description: "",
    photo: "",
  }
  getDataByFormEditModal(vUtilityObj);
  let vCheck = validateUtilityData(vUtilityObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/utilities/" + gUtilityId,
      type: "PUT",
      data: JSON.stringify(vUtilityObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      headers: gHeader,
      success: function (res) {
        console.log(res);
        gSTT = 1;
        getAllUtility();
        loadDataToTable(gUtilityList);
        Toast.fire({
          icon: 'success',
          title: "Update Utility Success!!!"
        })
        $("#edit-utility-modal").modal("hide");
      },
      error: function (err) {
        Toast.fire({
          icon: 'error',
          title: "Update Utility Failed: " + err.responseText
        })
      }
    })
  }
}
//Hàm xử lý nút Xác nhận xóa Utility
function onBtnConfirmDeleteClick() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/utilities/" + gUtilityId,
    type: "DELETE",
    headers: gHeader,
    success: function (res) {
      gSTT = 1;
      getAllUtility();
      loadDataToTable(gUtilityList);
      Toast.fire({
        icon: 'success',
        title: "Delete Utility Succcess!!!"
      })
    },
    error: function (err) {
      Toast.fire({
        icon: 'error',
        title: "Delete Utility Failed: " + err.responseText
      })
    }
  })
  refeshForm();
}
//Clear Form
function refeshForm() {
  $(document).find("#add-utility-modal input").val("");
}





















"use strict"
const gBASE_URL = "http://localhost:8080";
var gCustomerList = [];
var gRealEstateList = [];
var gSTT = 1;
var gCustomerId = "";
//Định nghĩa bảng
var gNameCol = ["id", "contactTitle", "note", "realEstates", "contactName", "mobile", "email", "address", "createDate", "updateDate"];
const gCOL_ID = 0;
const gCOL_CONTACT_TITLE = 1;
const gCOL_NOTE = 2;
const gCOL_REAL_ESTATE = 3;
const gCOL_CONTACT_NAME = 4;
const gCOL_MOBILE = 5;
const gCOL_EMAIL = 6;
const gCOL_ADDRESS = 7;
const gCOL_CREATE_DATE = 8;
const gCOL_UPDATE_DATE = 9;
var gTableCustomer = $("#table-customer").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_CONTACT_TITLE] },
    { data: gNameCol[gCOL_NOTE] },
    { data: gNameCol[gCOL_REAL_ESTATE] },
    { data: gNameCol[gCOL_CONTACT_NAME] },
    { data: gNameCol[gCOL_MOBILE] },
    { data: gNameCol[gCOL_EMAIL] },
    { data: gNameCol[gCOL_ADDRESS] },
    { data: gNameCol[gCOL_CREATE_DATE] },
    { data: gNameCol[gCOL_UPDATE_DATE] },
  ],
  columnDefs: [
    {
      targets: gCOL_ID,
      render: function (data) {
        return `
          <div class="text-center">
              ${gSTT++}
              <i data-id="${data}" class="far fa-edit text-primary btn-edit" style="cursor: pointer"></i>
              <i data-id="${data}" class="fas fa-trash text-primary btn-delete" style="cursor: pointer"></i>
          </div>
          `
      },
    },
    {
      targets: gCOL_REAL_ESTATE,
      render: function (data) {
        let vCount;
        data != null ? vCount = data.length : vCount = 0;
        let vHTML = `
          <a class="btn btn-primary btn-info-estate">Real Estate Info  <span class="badge badge-light">${vCount}</span></a> 
          `
        return vHTML;
      }
    }
  ],
  buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
  width: "100%"
})

$(document).ready(function () {
  getAllCustomer();
  loadDataToTable(gCustomerList);
})
//Hàm yêu cầu lấy dữ liệu All Customer từ server
function getAllCustomer() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/employees/" + gInfo.userId + "/customers",
    dataType: "json",
    type: "GET",
    async: false,
    headers: gHeader,
    success: function (res) {
      console.log(res);
      gCustomerList = res;
    },
    error: function (err) {
      Toast.fire({
        icon: 'error',
        title: 'Get All Customer Failed:' + err.responseJSON.error
      })
    }
  })
}
//Hàm load dữ liệu vào Bảng
function loadDataToTable(paramCustomer) {
  "use strict"
  gTableCustomer.clear();
  gTableCustomer.rows.add(paramCustomer);
  gTableCustomer.draw();
  gTableCustomer.buttons().container().prependTo('.table-tool');
}
$(document).on("click", ".btn-info-estate", function () {
  window.location.href = "RealEstateInfo.html?customerId=" + gCustomerId;
})
$("#btn-add-customer").on("click", function () { //Nút Add Customer
  $("#add-customer-modal").modal("show");
})
$("#btn-confirm-add").on("click", function () { //Nút Add Customer
  onBtnAddCustomerClick();
})

$("#table-customer tbody").on("click", "tr.odd,tr.even", function () { //Click dòng
  changeColorRow(this);
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableCustomer.row(vRowClick).data();
  console.log(vRowData);
  gCustomerId = vRowData.id;
})
$(document).on("click", ".btn-edit", function () { //Nút hiện thị form update
  gCustomerId = $(this).data("id");
  $("#edit-customer-modal").modal("show");
  getCustomerById(gCustomerId);
})
$("#btn-confirm-update").on("click", function () { //Nút Confirm Update Customer
  onBtnUpdateCustomerClick();
})
$(document).on("click", ".btn-delete", function () {
  gCustomerId = $(this).data("id");
  $("#delete-customer-modal").modal("show")
})
$("#btn-confirm-delete-customer").on("click", function () { //Nút Xác Nhận Delete Customer
  onBtnConfirmDeleteClick();
  $("#delete-customer-modal").modal("hide");
})
//Hàm xử lý nút Add Customer
function onBtnAddCustomerClick() {
  "use strict"
  let vCustomerObj = {
    contactTitle: "",
    note: "",
    contactName: "",
    mobile: "",
    email: "",
    address: ""
  }
  console.log(gHeader);
  getDataByFormAddModal(vCustomerObj);
  let vCheck = validateCustomerData(vCustomerObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/customers",
      type: "POST",
      data: JSON.stringify(vCustomerObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      headers: gHeader,
      success: function (res) {
        console.log(res);
        gSTT = 1;
        getAllCustomer();
        loadDataToTable(gCustomerList);
        refeshForm();
        Toast.fire({
          icon: 'success',
          title: 'Add Customer Success!!!'
        })
        $("#add-customer-modal").modal("hide");
      },
      error: function (err) {
        Toast.fire({
          icon: 'error',
          title: 'Add Customer Failed:' + err.responseJSON.error
        })
      }
    })
  }
}
//Hàm thay đổi màu hàng
function changeColorRow(paramRow) {
  $("#table-customer tbody tr").removeClass("table-primary");
  $(paramRow).addClass("table-primary");
}
//Hàm Truy xuất dữ liệu từ form
function getDataByFormEditModal(paramCustomerObj) {
  "use strict"
  paramCustomerObj.contactTitle = $("#inp-title-edit-modal").val().trim();
  paramCustomerObj.note = $("#textarea-note-edit-modal").val().trim();
  paramCustomerObj.contactName = $("#inp-name-edit-modal").val().trim();
  paramCustomerObj.mobile = $("#inp-mobile-edit-modal").val().trim();
  paramCustomerObj.email = $("#inp-email-edit-modal").val().trim();
  paramCustomerObj.address = $("#inp-address-edit-modal").val().trim();
}
//Hàm Truy xuất dữ liệu từ form
function getDataByFormAddModal(paramCustomerObj) {
  "use strict"
  paramCustomerObj.contactTitle = $("#inp-title-add-modal").val().trim();
  paramCustomerObj.note = $("#textarea-note-add-modal").val().trim();
  paramCustomerObj.contactName = $("#inp-name-add-modal").val().trim();
  paramCustomerObj.mobile = $("#inp-mobile-add-modal").val().trim();
  paramCustomerObj.email = $("#inp-email-add-modal").val().trim();
  paramCustomerObj.address = $("#inp-address-add-modal").val().trim();
}
//Hàm kiểm tra dữ liệu Customer
function validateCustomerData(paramCustomerObj) {
  let vResult = true;
  if (paramCustomerObj.contactTitle == "") {
    Toast.fire({
      icon: 'error',
      title: 'Contact Title Is Empty!'
    })
    vResult = false;
  }
  else if (paramCustomerObj.contactName == "") {
    Toast.fire({
      icon: 'error',
      title: 'Contact Name Is Empty'
    })
    vResult = false;
  }
  else if (paramCustomerObj.mobile == "") {
    Toast.fire({
      icon: 'error',
      title: 'Phone Number Is Empty'
    })
    vResult = false;
  }
  else if (paramCustomerObj.email == "") {
    Toast.fire({
      icon: 'error',
      title: 'Email Is Empty!'
    })
    vResult = false;
  }
  return vResult;
}
//Hàm yêu câu Get Customer By Id từ server
function getCustomerById(paramCustomerId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/customers/" + paramCustomerId,
    dataType: "json",
    type: "GET",
    headers: gHeader,
    success: function (res) {
      console.log(res);
      loadDataCustomerToForm(res);
      Toast.fire({
        icon: 'success',
        title: 'Get Customer Success!!!'
      })
    },
    error: function (err) {
      Toast.fire({
        icon: 'error',
        title: 'Get Customer Failed:' + err.responseJSON.error
      })
    }
  })
}
//Hàm load dữ liệu lên Form Edit Modal
function loadDataCustomerToForm(paramCustomerData) {
  "use strict"
  $("#inp-name-edit-modal").val(paramCustomerData.contactName);
  $("#inp-title-edit-modal").val(paramCustomerData.contactTitle);
  $("#textarea-note-edit-modal").val(paramCustomerData.note);
  $("#inp-mobile-edit-modal").val(paramCustomerData.mobile);
  $("#inp-address-edit-modal").val(paramCustomerData.address);
  $("#inp-email-edit-modal").val(paramCustomerData.email);
}
//Hàm xử lý nút Update Customer
function onBtnUpdateCustomerClick() {
  "use strict"
  console.log("Nút Update được ấn")
  let vCustomerObj = {
    contactTitle: "",
    note: "",
    contactName: "",
    mobile: "",
    email: "",
    address: ""
  }
  getDataByFormEditModal(vCustomerObj);
  let vCheck = validateCustomerData(vCustomerObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/customers/" + gCustomerId,
      type: "PUT",
      headers: gHeader,
      data: JSON.stringify(vCustomerObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      success: function (res) {
        console.log(res);
        gSTT = 1;
        getAllCustomer();
        loadDataToTable(gCustomerList);
        Toast.fire({
          icon: 'success',
          title: 'Update Customer Success!!!'
        })
        $("#edit-customer-modal").modal("hide");
      },
      error: function (err) {
        Toast.fire({
          icon: 'error',
          title: 'Update Customer Failed:' + err.responseJSON.error
        })
      }
    })
  }
}
//Hàm xử lý nút Xác nhận xóa Customer
function onBtnConfirmDeleteClick() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/customers/" + gCustomerId,
    type: "DELETE",
    headers: gHeader,
    success: function (res) {
      alert("Delete Customer Success!");
      gSTT = 1;
      getAllCustomer();
      loadDataToTable(gCustomerList);
      Toast.fire({
        icon: 'success',
        title: 'Delete Customer Success!!!'
      })
    },
    error: function (err) {
      Toast.fire({
        icon: 'error',
        title: 'Delete Customer Failed:' + err.responseJSON.error
      })
    }
  })

}
//Clear Form
function refeshForm() {
  $(document).find("#add-customer-modal input").val("");
  $(document).find("#add-customer-modal textarea").val("");
}




















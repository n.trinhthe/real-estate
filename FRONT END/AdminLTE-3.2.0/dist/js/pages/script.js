"use strict"
const gBASE_URL = "http://localhost:8080"
function pagination (pPageIndex,pTotalPages){
    let vLiTagHTML = "";
    let active;
    // let pPageIndex = $(this).data("index");
    let vBeforePage = pPageIndex - 1;
    let vAfterPage = pPageIndex + 1;
    
    if (pPageIndex > 1) { //Nếu Index = 1 sẽ ẩn Button Prev
        vLiTagHTML += `<li class="page-item"><a class="page-link" href="javascript:void(0);" data-index="${pPageIndex - 1}">Previous</a></li>`
    } else {
        vLiTagHTML += `<li class="page-item disabled"><a class="page-link" href="javascript:void(0);" data-index="${pPageIndex}">Previous</a></li>`
    }
    if (pPageIndex > 2) { //Nếu Index = 
        if (pTotalPages>3){
            vLiTagHTML += `<li class="page-item"><a class="page-link" href="javascript:void(0);" data-index="1">1</a></li>`
        }
        if (pPageIndex > 3) { //Nếu Index lớn hơn 3 sẽ thêm '...'
            vLiTagHTML += `<li class="page-item disabled"><a class="page-link">...</a></li>`
        }
    }
   //Hiện thị <li> trước index
    // if (pPageIndex == pTotalPages + 2) {
    //     vBeforePage = vBeforePage - 2;
    // } else if (pPageIndex == pTotalPages + 1) {
    //     vBeforePage = vBeforePage - 1;
    // }
    //Hiện thị <li> sau khi 
    if (pPageIndex == 1) { //Index trang = 2 thì add thêm 2 để  có index 4
        vAfterPage = vAfterPage + 2;
    } else if (pPageIndex == 2) { //Index trang = 2 thì add thêm 1 để  có index 4
        vAfterPage = vAfterPage + 1;
    }
    for (var pLength = vBeforePage; pLength <= vAfterPage; pLength++) {
        if (pLength > pTotalPages) { //if plength is greater than totalPage length then continue
            continue;
        }
        if (pLength == 0) { //Nếu chiều dài = 0 thì thêm 1
            pLength = pLength + 1;
        }
        if (pPageIndex == pLength) { //Neeys trang bằng chiều dài thì active
            active = "active";
        } else {
            active = "";
        }
        vLiTagHTML += `<li class="page-item ${active}"><a class="page-link" href="javascript:void(0);" data-index="${pLength}">${pLength}</a></li>`;
    }
    if (pPageIndex < pTotalPages - 1) { //Nếu trang nhỏ hơn thì hiện thị <li>
        if (pPageIndex < pTotalPages - 2) { //Nếu trang có giá trị nhỏ hơn -2 tổng số trang thì hiện (...) /
            vLiTagHTML += `<li class="page-item disabled"><a class="page-link disabled">...</a></li>`;
        }
        if(pTotalPages > 3){ //Nếu tổng số trang lớn hơn 3 thì mới hiện tổng số trang
            vLiTagHTML += `<li class="page-item"><a class="page-link" href="javascript:void(0);" data-index="${pTotalPages}">${pTotalPages}</a></li>`
        }
    }

    if (pPageIndex < pTotalPages) { //Trang hiện tại nhot hơn tổng số trang thì index thêm 1
        vLiTagHTML += `<li class="page-item"><a class="page-link" href="javascript:void(0);" data-index="${pPageIndex + 1}">Next</a></li>`;
    }
    else {
        vLiTagHTML += `<li class="page-item disabled"><a class="page-link" href="javascript:void(0);" data-index="${pPageIndex}">Next</a></li>`;
    }
    $(".pagination").html("");
    $(".pagination").append(vLiTagHTML);
    console.log(pPageIndex);
}
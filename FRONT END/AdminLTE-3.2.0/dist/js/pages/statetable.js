//Set Local Storage cho trang Real Estate
const vStatusRealEstateTable = {
    time: new Date(),
    "start": 0,
    "length": 10,
    "order": [
        [
            0,
            "desc"
        ]
    ],
    "search": {
        "search": "",
        "smart": true,
        "regex": false,
        "caseInsensitive": true
    },
    "columns": [
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        }
    ],
    "childRows": []
}
localStorage.setItem("DataTables_table-estate_/RealEstate.html", JSON.stringify(vStatusRealEstateTable));
localStorage.setItem("DataTables_table-estate_/RealEstateInfo.html", JSON.stringify(vStatusRealEstateTable));
localStorage.setItem("DataTables_table-estate_/RealEstateRegion.html", JSON.stringify(vStatusRealEstateTable));

//Set Local Storage cho trang Employee
const vStatusEmployeeTable = {
    time: new Date(),
    "start": 0,
    "length": 10,
    "order": [
        [
            0,
            "desc"
        ]
    ],
    "search": {
        "search": "",
        "smart": true,
        "regex": false,
        "caseInsensitive": true
    },
    "columns": [
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": false,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        },
        {
            "visible": true,
            "search": {
                "search": "",
                "smart": true,
                "regex": false,
                "caseInsensitive": true
            }
        }
    ],
    "childRows": []
}
localStorage.setItem("DataTables_table-estate_/Employee.html", JSON.stringify(vStatusEmployeeTable));
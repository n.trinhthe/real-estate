"use strict"
const gBASE_URL = "http://localhost:8080"
var gInvestorList = "";
var gContractorList = "";
var gDesignUnitList = "";
var gSTT = 1;
var gInvestorId = -1;
//Định nghĩa bảng
var gNameCol = ["id", "name", "address", "phone", "phone2", "fax", "email", "website", "projects",
    "description", "note", "action"];
const gCOL_ID = 0;
const gCOL_NAME = 1;
const gCOL_ADDRESS = 2;
const gCOL_PHONE = 3;
const gCOL_PHONE2 = 4;
const gCOL_FAX = 5;
const gCOL_EMAIL = 6;
const gCOL_WEBSITE = 7;
const gCOL_PROJECTS = 8;
const gCOL_DESCRIPTION = 9;
const gCOL_NOTE = 10;
const gCOL_ACTION = 11;
var gTableInvestor = $("#table-investor").DataTable({
    columns: [
        { data: gNameCol[gCOL_ID] },
        { data: gNameCol[gCOL_NAME] },
        { data: gNameCol[gCOL_ADDRESS] },
        { data: gNameCol[gCOL_PHONE] },
        { data: gNameCol[gCOL_PHONE2] },
        { data: gNameCol[gCOL_FAX] },
        { data: gNameCol[gCOL_EMAIL] },
        { data: gNameCol[gCOL_WEBSITE] },
        { data: gNameCol[gCOL_PROJECTS] },
        { data: gNameCol[gCOL_DESCRIPTION] },
        { data: gNameCol[gCOL_NOTE] },
        { data: gNameCol[gCOL_ACTION] },
    ],
    columnDefs: [
        {
            targets: gCOL_ID,
            render: function (data) {
                return `
                <div class="text-center">
                    ${gSTT++}
                    <i data-id="${data}" class="far fa-edit text-primary btn-edit" style="cursor: pointer"></i>
                    <i data-id="${data}" class="fas fa-trash text-primary btn-delete" style="cursor: pointer"></i>
                </div>
                `
            },

        },
        {
            className: 'dtr-control',
            orderable: false,
            targets: -1
        },
        {
            targets: gCOL_ACTION,
            defaultContent: ``
        },
    ],
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    autoWidth: false,
    responsive: {
        details: {
            type: 'column',
            target: -1
        }
    },
    stateSave: true,
    searching: true,
})

$(document).ready(function () {
    getAllInvestor();
    loadDataToTable(gInvestorList);
})

$("#table-investor tbody").on("click", "tr.odd,tr.even", function () { //Click dòng
    changeColorRow(this);
})
$(document).on("click", ".btn-edit", function () { //Nút hiện thị Update Investor Modal
    gInvestorId = $(this).data("id");
    $("#edit-investor-modal").modal("show")
    getInvestorById(gInvestorId);
})
$("#btn-confirm-update").on("click", function(){ //Nút confirm update
    onBtnUpdateInvestorClick();
})
$("#btn-add-investor").on("click", function () { //Nút hiện thị Add Investor Modal
    $("#add-investor-modal").modal("show");
})
$("#btn-confirm-add").on("click", function () { //Nút Add Investor
    onBtnAddInvestorClick();
})
$(document).on("click", ".btn-delete", function () { //Nút hiện thị Update Investor Modal
    gInvestorId = $(this).data("id");
    $("#delete-investor-modal").modal("show")
    getInvestorById(gInvestorId);
})
$("#btn-confirm-delete-investor").on("click", function () { //Nút Xác Nhận Delete Investor
    onBtnConfirmDeleteClick();
    $("#delete-investor-modal").modal("hide");
})

//Hàm yêu cầu lấy dữ liệu All Investor từ server
function getAllInvestor() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/investors",
        dataType: "json",
        type: "GET",
        async: false,
        success: function (res) {
            console.log(res);
            gInvestorList = res;
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
//Hàm load dữ liệu vào Bảng
function loadDataToTable(paramInvestor) {
    "use strict"
    gTableInvestor.clear();
    gTableInvestor.rows.add(paramInvestor);
    gTableInvestor.draw();
    gTableInvestor.buttons().container().prependTo('.table-tool');
}
//Hàm xử lý nút Add Investor
function onBtnAddInvestorClick() {
    "use strict"
    let vInvestorObj = {
        name: "",
        address: "",
        description: "",
        note: "",
        fax: "",
        phone: "",
        phone2: "",
        email: "",
        website: "",
        projects: ""
    }
    getDataByFormAddModal(vInvestorObj);
    let vCheck = validateInvestorData(vInvestorObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/investors",
            type: "POST",
            data: JSON.stringify(vInvestorObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log(res);
                gSTT = 1;
                getAllInvestor();
                loadDataToTable(gInvestorList);
                refeshForm();
                Toast.fire({
                    icon: 'success',
                    title: "Add Investor Success!"
                })
            },
            error: function (err) {
                Toast.fire({
                    icon: 'error',
                    title: "Add Investor Failed: " + err.responseText
                })
            }
        })
    }
}

//Hàm kiểm tra dữ liệu Investor
function validateInvestorData(paramInvestorObj) {
    let vResult = true;
    if (paramInvestorObj.name == "") {
        alert("Name Is Empty");
        vResult = false;
    }
    else if (paramInvestorObj.phone == "") {
        alert("Phone Is Empty");
        vResult = false;
    }
    else if (paramInvestorObj.address == "") {
        alert("Address Is Empty");
        vResult = false;
    }
    return vResult;
}
//Hàm Truy xuất dữ liệu từ form Edit Modal
function getDataByFormEditModal(paramInvestorObj) {
    "use strict"
    paramInvestorObj.name = $("#inp-name-edit-modal").val().trim();
    paramInvestorObj.address = $("#inp-address-edit-modal").val().trim();
    paramInvestorObj.description = $("#textarea-description-edit-modal").val().trim();
    paramInvestorObj.note = $("#textarea-note-edit-modal").val().trim();
    paramInvestorObj.fax = $("#inp-fax-edit-modal").val().trim();
    paramInvestorObj.phone = $("#inp-phone-edit-modal").val().trim();
    paramInvestorObj.phone2 = $("#inp-phone2-edit-modal").val().trim();
    paramInvestorObj.email = $("#inp-email-edit-modal").val().trim();
    paramInvestorObj.website = $("#inp-website-edit-modal").val().trim();
    paramInvestorObj.projects = $("#textarea-project-edit-modal").val().trim();
}
//Hàm Truy xuất dữ liệu từ form Edit Modal
function getDataByFormAddModal(paramInvestorObj) {
    "use strict"
    paramInvestorObj.name = $("#inp-name-add-modal").val().trim();
    paramInvestorObj.address = $("#inp-address-add-modal").val().trim();
    paramInvestorObj.description = $("#textarea-description-add-modal").val().trim();
    paramInvestorObj.note = $("#textarea-note-add-modal").val().trim();
    paramInvestorObj.fax = $("#inp-fax-add-modal").val().trim();
    paramInvestorObj.phone = $("#inp-phone-add-modal").val().trim();
    paramInvestorObj.phone2 = $("#inp-phone2-add-modal").val().trim();
    paramInvestorObj.email = $("#inp-email-add-modal").val().trim();
    paramInvestorObj.website = $("#inp-website-add-modal").val().trim();
    paramInvestorObj.projects = $("#textarea-project-add-modal").val().trim();
}
//Hàm thay đổi màu nút
function changeColorRow(paramRow) {
    $("#table-investor tbody tr").removeClass("table-primary");
    $(paramRow).addClass("table-primary");

}
//Hàm load dữ liệu lên Form
function loadDataInvestorToForm(paramInvestorObj) {
    "use strict"
    $("#inp-name-edit-modal").val(paramInvestorObj.name);
    $("#inp-address-edit-modal").val(paramInvestorObj.address);
    $("#textarea-description-edit-modal").val(paramInvestorObj.description);
    $("#textarea-note-edit-modal").val(paramInvestorObj.note);
    $("#inp-fax-edit-modal").val(paramInvestorObj.fax);
    $("#inp-phone-edit-modal").val(paramInvestorObj.phone);
    $("#inp-phone2-edit-modal").val(paramInvestorObj.phone2);
    $("#inp-email-edit-modal").val(paramInvestorObj.email);
    $("#inp-website-edit-modal").val(paramInvestorObj.website);
    $("#textarea-project-edit-modal").val(paramInvestorObj.projects);
}
//Hàm xử lý nút Update Investor
function onBtnUpdateInvestorClick() {
    "use strict"
    console.log("Nút Update được ấn")
    let vInvestorObj = {
        name: "",
        address: "",
        description: "",
        note: "",
        fax: "",
        phone: "",
        phone2: "",
        email: "",
        website: "",
        projects: ""
    }
    getDataByFormEditModal(vInvestorObj);
    let vCheck = validateInvestorData(vInvestorObj);
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL + "/investors/" + gInvestorId,
            type: "PUT",
            data: JSON.stringify(vInvestorObj),
            contentType: "application/json;charset=utf8",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                console.log(res);
                gSTT = 1;
                getAllInvestor();
                loadDataToTable(gInvestorList);
                Toast.fire({
                    icon: 'success',
                    title: "Update Investor Success!!!" 
                })
            },
            error: function (err) {
                Toast.fire({
                    icon: 'error',
                    title: "Update Investor Failed: " + err.responseText
                })
            }
        })
    }
}
//Hàm yêu câu Get Investor By Id từ server
function getInvestorById(paramInvestorId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/investors/" + paramInvestorId,
        dataType: "json",
        type: "GET",
        async: false,
        success: function (res) {
            console.log(res);
            loadDataInvestorToForm(res);
            Toast.fire({
                icon: 'success',
                title: 'Get Investor Success!!!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: 'Get Investor Failed: ' + err.responseText
            })
        }
    })
}
//Hàm xử lý nút Xác nhận xóa ALL Investor
function onBtnConfirmDeleteAllClick() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/investors/all",
        type: "DELETE",
        success: function (res) {
            alert("Delete All Investor Success!");
            gSTT = 1;
            getAllInvestor();
            loadDataToTable(gInvestorList);
        },
        error: function (err) {
            alert("Delete All Investor Failed: " + err.responseText);
        }
    })
}
//Hàm xử lý nút Xác nhận xóa Investor
function onBtnConfirmDeleteClick() {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/investors/" + gInvestorId,
        type: "DELETE",
        headers: gHeader,
        success: function (res) {
            gSTT = 1;
            getAllInvestor();
            loadDataToTable(gInvestorList);
            Toast.fire({
                icon: 'success',
                title: 'Delete Investor Success!!!'
            })
        },
        error: function (err) {
            Toast.fire({
                icon: 'error',
                title: "Delete Investor Failed: " + err.responseText
            })
        }
    })
}
//Clear Form
function refeshForm() {
    $(document).find("#add-investor-modal input").val("");
    $(document).find("#add-investor-modal textarea").val("");
}

"use strict"
const gBASE_URL_REAL_ESTATE = "http://localhost:8080/realestates"
const gBASE_URL_CUSTOMER = "http://localhost:8080/customers"
const gREGEX_EMAIL = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
var gProvinceList = "";
var gDistrictList = "";
var gWardList = "";
var gStreetList = "";
$(document).ready(function () {
    let vURL = new URL(window.location.href)
    let vPropertyId = vURL.searchParams.get("propertyId");
    getAllProvince();
    getAllDistrict();
    getAllWard();
    getAllStreet();
    getRealEstateById(vPropertyId);
    getPriceMax();
})
//Hàm yêu cầu Get All Real Estate từ server
function getRealEstateById(pPropertyId) {
    "use strict"
    $.ajax({
        url: gBASE_URL_REAL_ESTATE + "/" + pPropertyId,
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log(res);
            let vContact = getCustomerById(res.customerId);
            loadDataToPropertyDetail(res,vContact);
        },
        error: function (err) {
            console.log(err.responseText);
        }
    })
}
//Hàm load dữ liệu lên trang detail
function loadDataToPropertyDetail(pProperty,pContact) {
    "use strict"
    let vRequest = ""
    switch (pProperty.request) {
        case 0:
            vRequest = "Sell";
            break;
        case 1:
            vRequest = "Buy";
            break;
        case 2:
            vRequest = "Lease";
            break;
        case 3:
            vRequest = "Rent";
            break;
    }
    let vDirection = "";
    switch (pProperty.direction) {
        case 0:
            vDirection = "North-West";
            break;
        case 1:
            vDirection = "West";
            break;
        case 2:
            vDirection = "North";
            break;
        case 3:
            vDirection = "East";
            break;
        case 4:
            vDirection = "South";
            break;
        case 5:
            vDirection = "South-West";
            break;
        case 6:
            vDirection = "North-East";
            break;
        case 7:
            vDirection = "East-South";
            break;
    }
    let vFurniture = "";
    switch (pProperty.furnitureType) {
        case 0:
            vFurniture = "Basic";
            break;
        case 1:
            vFurniture = "Full Option";
            break;
        case 2:
            vFurniture = "Unkown";
            break;
    }
    let vArrayAddress = [pProperty.address]
    let vStreet = gStreetList.filter(e => e.id == pProperty.streetId);
    vStreet == "" ? vArrayAddress : vArrayAddress.push([vStreet[0].prefix, vStreet[0].name].join(" "));
    let vWard = gWardList.filter(e => e.id == pProperty.wardId);
    vWard == "" ? vArrayAddress : vArrayAddress.push([vWard[0].prefix, vWard[0].name].join(" "));
    let vDistrict = gDistrictList.filter(e => e.id == pProperty.districtId);
    vDistrict == "" ? vArrayAddress : vArrayAddress.push([vDistrict[0].prefix, vDistrict[0].name].join(" "));
    let vProvince = gProvinceList.filter(e => e.id == pProperty.provinceId);
    !vProvince ? vArrayAddress : vArrayAddress.push([vProvince[0].prefix, vProvince[0].name].join(" "));
    let vLocation = vArrayAddress.join(", ")

    let vHTMLDetail = `
    <span class="badge badge-secondary">${vRequest}</span>
    <img class="rounded" src="${gBASE_URL}/images/${pProperty.photo}" alt="" style="width: 100%; height: 500px;">
    <h2>${!pProperty.title?"":pProperty.title}</h2>
    <p><b style="font-size:110%">Address: </b> ${vLocation} </p>
    <p><b style="font-size:110%">Price: </b> ${!pProperty.price ? "" : pProperty.price} $</p>
    <p><b style="font-size:110%">Price Rent: </b> ${!pProperty.priceRent ? "" : pProperty.priceRent} $</p>
    <p><b style="font-size:110%">Contact: </b> ${pContact}</p>
    <p>${!pProperty.description ? "" : pProperty.description}</p>
    <div class="row form-group">
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-8 p-2">
                    <img class="rounded" src="./dist/picture/housedetail1.jpg" alt=""
                        style="width: 100%; height: 160px;">
                </div>
                <div class="col-sm-4 p-2">
                    <img class="rounded" src="./dist/picture/housedetail2.jpg" alt=""
                        style="width: 100%; height: 160px;">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 p-2">
                    <img class="rounded" src="./dist/picture/housedetail4.jpg" alt=""
                        style="width: 100%; height: 160px;">
                </div>
                <div class="col-sm-8 p-2">
                    <img class="rounded" src="./dist/picture/housedetail3.jpg" alt=""
                        style="width: 100%; height: 160px;">
                </div>
            </div>
        </div>
    </div>
    <h3>Property Amenities</h3>
    <div class="row">
        <div class="col-sm-4">
            <div class="row form-group">
                <span><i class="far fa-check-circle text-warning fa-xl"></i> Area: ${pProperty.acreage}m2</span>
            </div>
            <div class="row form-group">
                <span><i class="far fa-check-circle text-warning fa-xl"></i> Floor: ${!pProperty.totalFloors ? "" : pProperty.totalFloors}</span>
            </div>
            <div class="row form-group">
                <span><i class="far fa-check-circle text-warning fa-xl"></i> Bath Room: ${!pProperty.bath ? "" : pProperty.bath}</span>
            </div>
            <div class="row form-group">
                <span><i class="far fa-check-circle text-warning fa-xl"></i> Bed Room: ${!pProperty.bedroom ? "" : pProperty.bedroom}</span>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="row form-group">
                <span><i class="far fa-check-circle text-warning fa-xl"></i> Direction: ${vDirection} </span>
            </div>
            <div class="row form-group">
                <span><i class="far fa-check-circle text-warning fa-xl"></i> Furniture: ${vFurniture}</span>
            </div>
            <div class="row form-group">
                <span><i class="far fa-check-circle text-warning fa-xl"></i> Nearby Schools</span>
            </div>
            <div class="row form-group">
                <span><i class="far fa-check-circle text-warning fa-xl"></i> Nearby Market</span>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="row form-group">
                <span><i class="far fa-check-circle text-warning fa-xl"></i> Internet</span>
            </div>
            <div class="row form-group">
                <span><i class="far fa-check-circle text-warning fa-xl"></i> Air Controller</span>
            </div>
            <div class="row form-group">
                <span><i class="far fa-check-circle text-warning fa-xl"></i> Cable TV</span>
            </div>
            <div class="row form-group">
                <span><i class="far fa-check-circle text-warning fa-xl"></i> CC Camera</span>
            </div>
        </div>
    </div>
    `
    $("#content-detail").append(vHTMLDetail);
}

$(document).on("click", "#btn-submit", function () {
    "use strict"
    let vCustomerObj = {
        contactName: "",
        contactTitle: "",
        mobile: "",
        address: "",
        note: "",
        email: ""
    }
    getDataFromForm(vCustomerObj);
    let vCheck = validateData(vCustomerObj)
    if (vCheck == true) {
        $.ajax({
            url: gBASE_URL_CUSTOMER,
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(vCustomerObj),
            dataType: "json",
            success: function (res) {
                console.log(res);
                alert("Message Send Success!!");
                refeshForm();
            },
            error: function (err) {
                alert(err.responseText);
            }
        })
    }
})

//Hàm truy xuất thông tin từ form
function getDataFromForm(pCustomerObj) {
    "use strict"
    pCustomerObj.contactName = $("#inp-fullname").val().trim();
    pCustomerObj.mobile = $("#inp-mobile").val().trim();
    pCustomerObj.email = $("#inp-email").val().trim();
    pCustomerObj.address = $("#inp-address").val().trim();
    pCustomerObj.contactTitle = $("#inp-title").val().trim();
    pCustomerObj.note = $("#textarea-note").val().trim();
}

//Hàm kiểm tra dữ liệu truy xuất
function validateData(pCustomerObj) {
    "use strict"
    let vResult = true;
    if (pCustomerObj.contactName == "") {
        alert("Full Name Is Empty")
        vResult = false;
    }
    else if (pCustomerObj.mobile == "") {
        alert("Mobile Is Empty");
        vResult = false;
    }
    else if (pCustomerObj.email != "") {
        if (!gREGEX_EMAIL.test(pCustomerObj.email)) {
            alert("Email Is Not In A Correct Format");
            vResult = false;
        }
    }
    else if (pCustomerObj.contactTitle == "") {
        alert("Title Is Empty");
        vResult = false;
    }
    return vResult;
}
/* --------------------SEARCH PROPERTY------------------*/
/* ----------------------------------------------------------------------------*/
$("#range-price").on("change", function () {
    $("#price-search").text($(this).val());
    console.log($(this).val())
})
$("#btn-search").on("click", function () {
    let vSearchObj = {
        title: "",
        type: "",
        request: "",
        price: "",
        acreage: ""
    }
    getDataToFormSearch(vSearchObj);
    window.location.href = "properties.html"
        + "?title=" + vSearchObj.title
        + "&type=" + vSearchObj.type
        + "&request=" + vSearchObj.request
        + "&price=" + vSearchObj.price
        + "&acreage=" + vSearchObj.acreage
})

function getDataToFormSearch(pSearchObj) {
    "use strict"
    pSearchObj.title = $("#title-search").val();
    pSearchObj.type = $("input[name='type']:checked").val();
    pSearchObj.request = $("input[name='request']:checked").val();
    pSearchObj.price = $("#range-price").val();
    pSearchObj.pSearchObj = $("inp-acreage").val();
}
function getAllProvince() {
    $.ajax({
        url: "http://localhost:8080/provinces",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gProvinceList = res;
        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
}
function getAllDistrict() {
    $.ajax({
        url: "http://localhost:8080/districts",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gDistrictList = res;
        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
}
function getAllWard() {
    $.ajax({
        url: "http://localhost:8080/wards",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gWardList = res;
        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
}
function getAllStreet() {
    $.ajax({
        url: "http://localhost:8080/streets",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gStreetList = res;
        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
}

//Clear Form
function refeshForm() {
    $(document).find(".div-message input").val("");
    $(document).find(".div-message textarea").val("");
}

//Hàm yêu cầu ajax get Customer By Id
function getCustomerById(paramCustomerId) {
    "use strict"
    let vContact = ""
    $.ajax({
        url: "http://localhost:8080/customers/" + paramCustomerId,
        dataType: "json",
        type: "GET",
        async: false,
        success: function (res) {
            vContact = res.contactName + " - " + res.mobile
        }
    })
    return vContact;
}

//Hàm lấy giá trị price Max
function getPriceMax(){
    $.ajax({
        url: gBASE_URL + "/realestates/pricemax",
        type: "GET",
        dataType: "json",
        success: function (res) {
           let price = formatPrice(res);
           $("#range-price").attr({"max": res, "value": res/2});
           $("#p-price-max").html(price);
           $("#price-search").html(formatPrice(res/2));
        },
        error: function (err) {
            console.log(err.responseText);
        }
    })
}



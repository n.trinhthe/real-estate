"use strict"
$(document).ready(function () {
    const TOKEN = getCookie("Token")
    if (TOKEN) {
        window.location.href = "Info.html";
    }
})
$("#btn-submit").on("click", function () {
    console.log("Nút được ấn")
    var vLoginData = {
        username: '',
        password: ''
    }
    getDataByForm(vLoginData);
    console.log(vLoginData)
    var vCheck = validateData(vLoginData)
    if (vCheck == true) {
        $.ajax({
            url: "http://localhost:8080/login",
            type: "POST",
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify(vLoginData),
            success: function (res) {
                console.log(res)
                alert("Sign In Success!")
                responseHandler(res)
            },
            error: function (err) {
                alert("Sign In Failed: " + err.responseText)
            }
        })
    }
})
//Xử lý object trả về khi login thành công
function responseHandler(data) {
    //Lưu token vào cookie trong 1 ngày
    setCookie("Token", data, 1);
    window.location.href = "Info.html";
}

//Hàm setCookie đã giới thiệu ở bài trước
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getDataByForm(pData) {
    pData.username = $("#username").val();
    pData.password = $("#password").val();
}
function validateData(pData) {
    let vResult = true;
    if (pData.username == "") {
        alert("Username is empty");
        $("#username").focus();
        vResult = false;
    }
    else if (pData.password == "") {
        alert("Password is empty");
        $("#password").focus();
        vResult = false;
    }
    else if (pData.password.length < 4) {
        alert("Password phải có chiều dài tối thiểu 4 ký tự");
        $("#password").focus();
        vResult = false;
    }
    return vResult
}
//Hàm get Cookie
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
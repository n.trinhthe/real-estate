"use strict"
var gProvinceList = "";
var gDistrictList = "";
var gWardList = "";
var gStreetList = "";
$("#btn-extension").on("click", function () {
    if ($("#div-extension").css("display") == "none") {
        $("#div-extension").show()
    }
    else {
        $("#div-extension").hide()
    }
})
$(document).ready(function () {
    getAllProvince();
    getAllDistrict();
    getAllWard();
    getAllStreet();
    loadDataToPopularProperties(getTypeProperty(""), ".popular-property");
    loadDataToPopularProperties(getTypeProperty(1), ".house-property");
    loadDataToPopularProperties(getTypeProperty(2), ".apartment-property");
    loadDataToPopularProperties(getTypeProperty(3), ".office-property");
})
function loadDataToPopularProperties(pPropertyList, popular) {
    $(popular).html("");
    for (let property of pPropertyList) {
        var requestName = "";
        if (property.request == 1) {
            requestName = "Bán";
        }
        else if (property.request == 2) {
            requestName = "Mua";
        }
        else if (property.request == 3) {
            requestName = "Cho thuê";
        }
        else if (property.request == 4) {
            requestName = "Thuê";
        }

        let vDirection = "";
        switch (property.direction) {
            case 1:
                vDirection = "W";
                break
            case 2:
                vDirection = "N";
                break
            case 3:
                vDirection = "E";
                break
            case 4:
                vDirection = "S";
                break
            case 5:
                vDirection = "WS";
                break
            case 6:
                vDirection = "NE";
                break
            case 7:
                vDirection = "NS";
                break
            case 8:
                vDirection = "NW";
                break
        }
        let vArrayAddress = [property.address]
        let vStreet = gStreetList.filter(e => e.id == property.streetId);
        vStreet == "" ? vArrayAddress : vArrayAddress.push([vStreet[0].prefix, vStreet[0].name].join(" "));
        let vWard = gWardList.filter(e => e.id == property.wardId);
        vWard == "" ? vArrayAddress : vArrayAddress.push([vWard[0].prefix, vWard[0].name].join(" "));
        let vDistrict = gDistrictList.filter(e => e.id == property.districtId);
        vDistrict == "" ? vArrayAddress : vArrayAddress.push([vDistrict[0].prefix, vDistrict[0].name].join(" "));
        let vProvince = gProvinceList.filter(e => e.id == property.provinceId);
        vProvince == "" ? vArrayAddress : vArrayAddress.push(vProvince[0].name);
        let vLocation = vArrayAddress.join(", ")
        let vHTML = `
        <div class="card">
            <span class="badge badge-secondary">${requestName}</span>
            <img class="card-img-top" src="${gBASE_URL}/images/${property.photo}" alt="Card image">
            <div class="card-body d-flex just-content-end flex-column">
                <h4 class="card-title text-left">${!property.title ? "" : property.title}</h4>
                <p class="card-text text-left text-monospace">${vLocation}</p>
                <div class="row form-group utility">
                    <div class="col-sm-3 d-flex border-right flex-column">
                    <i class="fa-solid fa-vector-square"></i>
                    <span>${property.acreage} m2</span>
                    </div>
                    <div class="col-sm-3 d-flex border-right flex-column">
                    <i class="fa-solid fa-bath"></i>
                    <span>${!property.bath ? "" : property.bath}</span>
                    </div>
                    <div class="col-sm-3 d-flex border-right flex-column">
                    <i class="fa-solid fa-bed"></i>
                    <span class="text-small-left">${!property.bedroom ? "" : property.bedroom}</span>
                    </div>
                    <div class="col-sm-3 d-flex flex-column">
                    <i class="fas fa-location-arrow"></i>
                    <span class="text-small-left">${vDirection}</span>
                    </div>
                </div>
                <p class="card-text text-left font-weight-bold">Price: <span class="font-italic">${property.price} </span> $ </p>
                <a data-id="${property.id}" class="btn btn-warning w-100 mt-auto btn-see-more">See More</a>
            </div>
        </div>
        `
        $(popular).append(vHTML);
    }
}

$(document).on("click", ".btn-see-more", function () {
    let vPropertyId = $(this).data("id");
    window.location.href = "propertydetail.html?propertyId=" + vPropertyId;
})

//Hàm load Ajax lấy dữ liệu Type Property
function getTypeProperty(pType) {
    "use strict"
    let gPropertyList = ""
    $.ajax({
        url: "http://localhost:8080/realestates?type=" + pType,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gPropertyList = res;

        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
    console.log(gPropertyList);
    return gPropertyList;
}

function getAllProvince() {
    $.ajax({
        url: "http://localhost:8080/provinces",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gProvinceList = res;
        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
}
function getAllDistrict() {
    $.ajax({
        url: "http://localhost:8080/districts",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gDistrictList = res;
        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
}
function getAllWard() {
    $.ajax({
        url: "http://localhost:8080/wards",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gWardList = res;
        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
}
function getAllStreet() {
    $.ajax({
        url: "http://localhost:8080/streets",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gStreetList = res;
        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
}

/* --------------------SEARCH PROPERTY------------------*/
/* ----------------------------------------------------------------------------*/
var gResquest = "";
$("#btn-search").on("click", function () {
    let vSearchObj = {
        title: "",
        type: "",
        request: "",
        price: "",
        acreage: ""
    }
    getDataToFormSearch(vSearchObj);
    window.location.href = "properties.html"
        + "?title=" + vSearchObj.title
        + "&type=" + vSearchObj.type
        + "&request=" + vSearchObj.request
        + "&price=" + vSearchObj.price
        + "&acreage=" + vSearchObj.acreage
})


function getDataToFormSearch(pSearchObj) {
    "use strict"
    pSearchObj.title = $("#title-search").val();
    pSearchObj.type = $("#select-type").val();
    pSearchObj.request = gResquest;
    pSearchObj.price = $("#range-price").val();
    pSearchObj.acreage = $("#input-acreage").val();
}

$(".btn-group button").on("click", function () {
    $(".btn-group button").removeClass("btn-primary").addClass("btn-warning");
    $(this).removeClass("btn-warning").addClass("btn-primary");
    gResquest = $(this).val();
});

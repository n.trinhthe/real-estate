"use strict"
var gNameCol = ["id", "name", "prefix", "action"];
var gDistrictList = [];
var gDistrictId = "";
$(document).ready(function () {
  getAllProvince();
  getAllCustomer();
})

$("#select-province").on("change", function () { //Thay đổi Tỉnh thành
  onChangeProvince($(this).val());
})

$("#select-district").on("change", function () { //Thay đổi quận huyện
  onChangeDistrict($(this).val());
})

$("#btn-customer-modal").on("click", function () { //Nút hiện thị modal
  $("#add-customer-modal").modal("show");
})
$("#btn-add-customer").on("click", function () { //Nút add customer
  onBtnAddCustomerClick()
})

$("#form-add-realestate").submit(function (event) { //Nút Add Estate
  event.preventDefault();
  onBtnAddEstateClick(this);
})

//Hàm ajax lấy dữ liệu Investor
function getAllCustomer() {
  "use strict"
  $.ajax({
    url: "http://localhost:8080/employees/" + gInfo.userId + "/customers",
    dataType: "json",
    type: "GET",
    async: false,
    headers: gHeader,
    success: function (res) {
      console.log(res);
      res.forEach(element => {
        $("#select-customer").append($("<option>", {
          value: element.id,
          text: element.contactName + " - " + element.mobile,
        }));
      });
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
//Hàm xử lý nút Add Customer
function onBtnAddCustomerClick() {
  "use strict"
  let vCustomerObj = {
    contactTitle: "",
    note: "",
    contactName: "",
    mobile: "",
    email: "",
    address: ""
  }
  console.log(gHeader);
  getDataByFormModal(vCustomerObj);
  let vCheck = validateCustomerData(vCustomerObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/customers",
      type: "POST",
      data: JSON.stringify(vCustomerObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      headers: gHeader,
      success: function (res) {
        console.log(res);
        alert("Add Customer Success!");
        $("#select-customer").find("option").remove().end(); //Refesh customer
        $("#select-customer").append($("<option>", { //Add customer vào select
          value: "",
          text: "Select Cutomer",
        }));
        getAllCustomer();
        refeshForm();
        $("#add-customer-modal").modal("hide");
      },
      error: function (err) {
        Toast.fire({
          icon: 'error',
          title: 'Add Customer Failed:' + err.responseText
        })
      }
    })
  }
}
//Hàm Truy xuất dữ liệu từ form
function getDataByFormModal(paramCustomerObj) {
  "use strict"
  paramCustomerObj.contactTitle = $("#inp-title-customer-modal").val().trim();
  paramCustomerObj.note = $("#textarea-note-modal").val().trim();
  paramCustomerObj.contactName = $("#inp-name-modal").val().trim();
  paramCustomerObj.mobile = $("#inp-mobile-modal").val().trim();
  paramCustomerObj.email = $("#inp-email-modal").val().trim();
  paramCustomerObj.address = $("#inp-address-modal").val().trim();

}
//Hàm kiểm tra dữ liệu Customer
function validateCustomerData(paramCustomerObj) {
  let vResult = true;
  if (paramCustomerObj.contactTitle == "") {
    alert("Contact Title chưa được nhập!");
    vResult = false;
  }
  else if (paramCustomerObj.contactName == "") {
    alert("Contact Name chưa được nhập!");
    vResult = false;
  }
  else if (paramCustomerObj.mobile == "") {
    alert("Phone Number chưa được nhập!");
    vResult = false;
  }
  else if (paramCustomerObj.email == "") {
    alert("Email chưa được nhập!");
    vResult = false;
  }
  return vResult;
}
//Hàm xử lý nút Add Estate
function onBtnAddEstateClick(event) {
  "use strict"
  let vEstateObj = {
    title: "",
    type: "",
    request: "",
    provinceId: "",
    districtId: "",
    wardId: "",
    streetId: "",
    projectId: "",
    customerId: "",
    address: "",
    price: "",
    priceMin: "",
    priceTime: "",
    acreage: "",
    direction: "",
    totalFloors: "",
    numberFloors: "",
    bath: "",
    apartCode: "",
    wallArea: "",
    bedroom: "",
    balcony: "",
    landscapeView: "",
    apartLoca: "",
    apartType: "",
    furnitureType: "",
    priceRent: "",
    returnRate: "",
    legalDoc: "",
    description: "",
    widthY: "",
    longX: "",
    streetHouse: "",
    viewNum: "",
    shape: "",
    distance2facade: "",
    adjacentFacadeNum: "",
    adjacentRoad: "",
    alleyMinWidth: "",
    adjacentAlleyMinWidth: "",
    factor: "",
    structure: "",
    photo: "",
    lat: "",
    lng: "",
    clcl: "",
    ctxdprice: "",
    ctxdvalue: "",
    dtsxd: "",
    fsbo: "",
    censorred: false
  }
  var formData = new FormData(event);
  getDataByForm(vEstateObj)
  formData.append('realEstate', JSON.stringify(vEstateObj));
  if($('#inp-photo')[0].files[0]){
      formData.append('image',$('#inp-photo')[0].files[0]);
  }else {
      const defaultImage = new Blob([''], { type: 'image/jpeg' }); //Chuyển 
      formData.append('image',defaultImage);
  }
  let vCheck = validateEstateData(vEstateObj, $('#inp-photo')[0].files[0]);
  if (vCheck == true) {
    $.ajax({
      url: "http://localhost:8080/realestates",
      type: "POST",
      data: formData,
      processData: false,
      contentType: false,
      headers: gHeader,
      success: function (res) {
        console.log(res);
        refeshForm();
        alert("Add Real Estate Success!!!");

      },
      error: function (err) {
        alert('Add Real Estate Failed: ' + err.responseText)
      }
    })
  }
}

//Hàm Truy xuất dữ liệu từ form
function getDataByForm(paramEstateObj) {
  "use strict"
  paramEstateObj.title = $("#inp-title-add").val().trim();
  paramEstateObj.type = $("#select-property-type").val();
  paramEstateObj.request = $("#select-property-request").val();
  paramEstateObj.provinceId = $("#select-province").val();
  paramEstateObj.districtId = $("#select-district").val();
  paramEstateObj.wardId = $("#select-ward").val();
  paramEstateObj.streetId = $("#select-street").val();
  paramEstateObj.address = $("#inp-address-add").val().trim();
  paramEstateObj.price = $("#inp-price").val().trim();
  paramEstateObj.lat = $("#inp-lat").val().trim();
  paramEstateObj.lng = $("#inp-lng").val().trim();
  paramEstateObj.acreage = $("#inp-area").val().trim();
  paramEstateObj.dtsxd = $("#inp-dtsxd").val().trim();
  paramEstateObj.widthY = $("#inp-width").val().trim();
  paramEstateObj.longX = $("#inp-long").val().trim();
  paramEstateObj.direction = $("#select-direction").val();
  paramEstateObj.bath = $("#inp-bathroom").val().trim();
  paramEstateObj.bedroom = $("#inp-bedroom").val().trim();
  paramEstateObj.furnitureType = $("#select-furniture").val();
  paramEstateObj.adjacentFacadeNum = $("#inp-adjacent-facade").val().trim();
  paramEstateObj.customerId = $("#select-customer").val();
  paramEstateObj.photo = $("#inp-photo").val().split("\\").pop();
}
//Hàm kiểm tra dữ liệu Estate
function validateEstateData(paramEstateObj, pImageFile) {
  let vResult = true;
  if (paramEstateObj.title == "") {
    alert("Title Not Empty");
    vResult = false;
  }
  else if (paramEstateObj.provinceId == "") {
    alert("Province Not Empty");
    vResult = false;
  }
  else if (paramEstateObj.price == "") {
    alert("Price Not Empty");
    vResult = false;
  }
  else if (paramEstateObj.acreage == "") {
    alert("Acreage Not Empty");
    vResult = false;
  }
  else if (paramEstateObj.request == "") {
    alert("Request Not Empty");
    vResult = false;
  }
  else if (paramEstateObj.type == "") {
    alert("Type Request Not Empty");
    vResult = false;
  }
  else if (paramEstateObj.address == "") {
    alert("Address Not Empty");
    vResult = false;
  }
  else if (!pImageFile) {
    alert("Image Not Empty");
    vResult = false;
  }
  return vResult;
}
//Hàm xử lý on change Select Province
function onChangeProvince(paramProvinceId) {
  $("#select-district").find("option").remove().end().append('<option selected value="">Select District</option>');
  $("#select-ward").find("option").remove().end().append('<option selected value="">Select Ward</option>');
  $("#select-street").find("option").remove().end().append('<option selected value="">Select Street</option>');
  if (paramProvinceId != "") {
    getDistrictsOfProvince(paramProvinceId);
  }
}

//Hàm xử lý thay đổi District
function onChangeDistrict(paramDistrictId) {
  $("#select-ward").find("option").remove().end().append('<option selected value="">Select Ward</option>');
  $("#select-Street").find("option").remove().end().append('<option selected value="">Select Street</option>');
  if (paramDistrictId != "") {
    getWardsOfDistrict(paramDistrictId);
    getStreetsOfDistrict(paramDistrictId);
  }
}
//Hàm ajax lấy dữ liệu Province
function getAllProvince() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/provinces",
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      res.forEach(element => {
        $("#select-province").append($("<option>", {
          value: element.id,
          text: element.name,
        }));
      });
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
//Hàm yêu cầu ajax lấy Ward của District
function getWardsOfDistrict(paramDistrictId) {
  "use strict"
  $.ajax({
    url: "http://localhost:8080/districts/" + paramDistrictId + "/wards",
    dataType: "json",
    type: "GET",
    success: function (res) {
      res.sort((a, b) => a.name - b.name);
      res.forEach(element => {
        $("#select-ward").append($("<option>", {
          value: element.id,
          text: [element.prefix, element.name].join(" "),
        }));
      });
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
//Hàm yêu cầu ajax get all District
function getDistrictsOfProvince(pProvinceId) {
  "use strict"
  $.ajax({
    url: "http://localhost:8080/provinces/" + pProvinceId + "/districts",
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      res.forEach(element => {
        $("#select-district").append($("<option>", {
          value: element.id,
          text: [element.prefix, element.name].join(" ")
        }));
      });
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
//Hàm yêu cầu ajax lấy Street của District
function getStreetsOfDistrict(paramDistrictId) {
  "use strict"
  $.ajax({
    url: "http://localhost:8080/districts/" + paramDistrictId + "/streets",
    dataType: "json",
    type: "GET",
    success: function (res) {
      res.sort((a, b) => a.name - b.name);
      res.forEach(element => {
        $("#select-street").append($("<option>", {
          value: element.id,
          text: [element.prefix, element.name].join(" "),
        }));
      });
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
//Clear Form
function refeshForm() {
  $(document).find("input").val("");
  $(document).find("textarea").val("");
  $(document).find("select").val("").change();
}


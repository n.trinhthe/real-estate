"use strict"
var gHeader = "";
var gInfo = "";
const gBASE_URL = "http://localhost:8080"
$(document).on("click",".btn-logout-menu", function () {
    btnLogoutMenu();
});
$(document).ready(function () {
    const token = getCookie("Token");
    //Gọi API để lấy thông tin người dùng
    //Khai báo xác thực ở headers
    if (token != "") {
        var headers = {
            Authorization: "Token " + token,
        };
        gHeader = headers;
        console.log(gHeader);
        var urlInfo = gBASE_URL + "/users/info";
        $.ajax({
            url: urlInfo,
            method: "GET",
            headers: gHeader,
            async: false,
            success: function (responseObject) {
                console.log(responseObject);
                gInfo = responseObject;
                getEmployeeById(gInfo.userId)
            },
            error: function (xhr) {
            }
        });
    }
});
//Hàm xử lý logout Dropdown Menu
function btnLogoutMenu() {
    // Trước khi logout cần xóa token đã lưu trong cookie
    setCookie("Token", "", 1);
    window.location.href = "home.html";
}

//Hàm setCookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//Hàm yêu câu Get Employee By Id từ server
function getEmployeeById(paramEmployeeId) {
    "use strict"
    $.ajax({
        url: gBASE_URL + "/employees/" + paramEmployeeId,
        dataType: "json",
        type: "GET",
        headers: gHeader,
        success: function (res) {
            console.log(res);
            displayLogin(res)
        },
        error: function (err) {
            alert(err.responseText);
        }
    })
}
 
//Hàm xử lý hiện thị login
function displayLogin(pUser) {
    let vPhoto = pUser.photo
    console.log(vPhoto);
    $(".btn-header-login").hide();
    $(".fa-user").hide();
    $(".div-btn").prepend(`
    <div class="dropdown">
        <a href="info.html"><img class="rounded-circle m-2 logo-user" src="${gBASE_URL}/images/${(vPhoto == null || vPhoto == "") ? "logouser.jpg" : vPhoto}" alt="" style="width:40px; height:40px; cursor: pointer"></a>
        <div class="dropdown-content">
            <a href="info.html">Account</a>
            <a href="#" class="btn-logout-menu">Logout</a>
        </div>
    </div>`)
    $(".icon-user").prepend(`<a href="info.html"><img class="rounded-circle m-2 logo-user" src="${gBASE_URL}/images/${(vPhoto == null || vPhoto == "") ? "logouser.jpg" : vPhoto}" alt="" style="width:40px; height:40px; cursor: pointer"></a>`)
}
//Hàm xử lý nút ẩn hiện Menu
function btnMenu() {
    $(".navbar-mobile").toggle();
}
//Hàm get Cookie
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//Hàm phân trang
function pagination(pPageIndex, pTotalPages) {
    let vLiTagHTML = "";
    let active;
    // let pPageIndex = $(this).data("index");
    let vBeforePage = pPageIndex - 1;
    let vAfterPage = pPageIndex + 1;

    if (pPageIndex > 1) { //Nếu Index = 1 sẽ ẩn Button Prev
        vLiTagHTML += `<li class="page-item"><a class="page-link" href="javascript:void(0);" data-index="${pPageIndex - 1}">Previous</a></li>`
    } else {
        vLiTagHTML += `<li class="page-item disabled"><a class="page-link" href="javascript:void(0);" data-index="${pPageIndex}">Previous</a></li>`
    }
    if (pPageIndex > 2) { //Nếu Index = 
        if (pTotalPages > 3) {
            vLiTagHTML += `<li class="page-item"><a class="page-link" href="javascript:void(0);" data-index="1">1</a></li>`
        }
        if (pPageIndex > 3) { //Nếu Index lớn hơn 3 sẽ thêm '...'
            vLiTagHTML += `<li class="page-item disabled"><a class="page-link">...</a></li>`
        }
    }
    //Hiện thị <li> trước index
    // if (pPageIndex == pTotalPages + 2) {
    //     vBeforePage = vBeforePage - 2;
    // } else if (pPageIndex == pTotalPages + 1) {
    //     vBeforePage = vBeforePage - 1;
    // }
    //Hiện thị <li> sau khi 
    if (pPageIndex == 1) { //Index trang = 2 thì add thêm 2 để  có index 4
        vAfterPage = vAfterPage + 2;
    } else if (pPageIndex == 2) { //Index trang = 2 thì add thêm 1 để  có index 4
        vAfterPage = vAfterPage + 1;
    }
    for (var pLength = vBeforePage; pLength <= vAfterPage; pLength++) {
        if (pLength > pTotalPages) { //if plength is greater than totalPage length then continue
            continue;
        }
        if (pLength == 0) { //Nếu chiều dài = 0 thì thêm 1
            pLength = pLength + 1;
        }
        if (pPageIndex == pLength) { //Neeys trang bằng chiều dài thì active
            active = "active";
        } else {
            active = "";
        }
        vLiTagHTML += `<li class="page-item ${active}"><a class="page-link" href="javascript:void(0);" data-index="${pLength}">${pLength}</a></li>`;
    }
    if (pPageIndex < pTotalPages - 1) { //Nếu trang nhỏ hơn thì hiện thị <li>
        if (pPageIndex < pTotalPages - 2) { //Nếu trang có giá trị nhỏ hơn -2 tổng số trang thì hiện (...) /
            vLiTagHTML += `<li class="page-item disabled"><a class="page-link disabled">...</a></li>`;
        }
        if (pTotalPages > 3) { //Nếu tổng số trang lớn hơn 3 thì mới hiện tổng số trang
            vLiTagHTML += `<li class="page-item"><a class="page-link" href="javascript:void(0);" data-index="${pTotalPages}">${pTotalPages}</a></li>`
        }
    }

    if (pPageIndex < pTotalPages) { //Trang hiện tại nhot hơn tổng số trang thì index thêm 1
        vLiTagHTML += `<li class="page-item"><a class="page-link" href="javascript:void(0);" data-index="${pPageIndex + 1}">Next</a></li>`;
    }
    else {
        vLiTagHTML += `<li class="page-item disabled"><a class="page-link" href="javascript:void(0);" data-index="${pPageIndex}">Next</a></li>`;
    }
    $(".pagination").html("");
    $(".pagination").append(vLiTagHTML);
    console.log(pPageIndex);
}
//Xử lý Toast
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function getToast(pStatus, pTitle) {
    Toast.fire({
        icon: pStatus,
        title: pTitle
    })
}

//Hàm định dạng Price
function formatPrice(pNumber){
    let formattedNumber = Math.floor(pNumber).toLocaleString('en-US');
    return formattedNumber;
}


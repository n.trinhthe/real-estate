var gHeader = "";
var gInfo = "";
var gTotalPages = 0;
var gSize = 10;

$(document).ready(function () {
    const token = getCookie("Token");
    //Gọi API để lấy thông tin người dùng
    //Khai báo xác thực ở headers
    if (token != "") {
        var headers = {
            Authorization: "Token " + token,
        };
        gHeader = headers;
        console.log(gHeader);
        var urlInfo = "http://localhost:8080/users/info";
        $.ajax({
            url: urlInfo,
            method: "GET",
            headers: gHeader,
            async: false,
            success: function (responseObject) {
                console.log(responseObject);
                gInfo = responseObject;
                displayUser(responseObject);
                displayUserMenu(responseObject);
            },
            error: function (xhr) {
                console.log(xhr);
                // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
                redirectToLogin();
            }
        });
    }
    else {
        redirectToLogin();
    }
    getEmployeeById(gInfo.userId);
    //Set sự kiện cho nút logout
    $("#btn-logout").on("click", function () {
        redirectToLogin();
    });
    //Set sự kiện cho nút logout
    $(".info ").on("click", ".btn-logout", function () {
        redirectToLogin();
    });
    $(".button-info").on("click", "button", function () {
        $(".button-info").find("button").removeClass("btn-primary");
        $(this).addClass("btn-primary");
        showDivInfo($(this).val());
    })
    //Nút hiện thị profile trong Dashboard
    $("#btn-profile").on("click", function () {
        $(".tab-content #tab-dashboard").hide();
        $(".tab-content #tab-profile").show();
        $(".tab-content #tab-realestate").hide();
        $(".button-info").find("button").removeClass("btn-primary");
        $(".button-info button:nth-child(2)").addClass("btn-primary");
    });
    //Nút hiện thị Real Estate trong Dashboard
    $("#btn-realestate").on("click", function () {
        $(".tab-content #tab-dashboard").hide();
        $(".tab-content #tab-profile").hide();
        $(".tab-content #tab-realestate").show();
        $(".button-info").find("button").removeClass("btn-primary");
        $(".button-info button:nth-child(3)").addClass("btn-primary");
    })
    //Hàm xử lý nút ấn phân trang
    $(document).on("click", "li .page-link", function () {
        let vPageIndex = $(this).data("index");
        if ($("#btn-close-search").attr('class').includes('d-none')) {
            getAllEstateByUser(vPageIndex - 1, gSize);
        }
        else {
            advancedSearch(vPageIndex - 1, gSize);
        }
        loadDataToTable(gEstateList);
    })
    $("#btn-advanced-search").on("click", function () { //Nút hiện thị modal search
        $("#advanced-search-modal").modal("show")
    })
    $("#btn-confirm-search").on("click", function () { //Nút confirm search
        advancedSearch(0, gSize);
    })

    $("#btn-close-search").on("click", function () { //Nút đóng search
        getAllEstateByUser(0, gSize);
        loadDataToTable(gEstateList);
        $("#btn-close-search").addClass('d-none');
        // $("#advanced-search-modal select").val("").change();
        // $("#advanced-search-modal input").val("");
    })
    $("#btn-update-profile").on("click", function () {
        onBtnUpdateProfileClick();
    })
    //Hàm xử lý nút update profile
    function onBtnUpdateProfileClick() {
        let vUserObj = {
            firstname: "",
            lastname: "",
            titleOfCourtesy: "",
            birthDate: "",
            address: "",
            city: "",
            region: "",
            postalCode: "",
            country: "",
            homePhone: "",
            extension: "",
            email: "",
            photo: ""
        }
        getEmployeeProfile(vUserObj);
        let vCheck = validateProfile(vUserObj);
        if (vCheck == true) {
            $.ajax({
                url: gBASE_URL + "/employees/" + gInfo.userId + "/profile",
                type: "PUT",
                data: JSON.stringify(vUserObj),
                contentType: "application/json;charset=utf8",
                dataType: "json",
                headers: gHeader,
                success: function (res) {
                    console.log(res);
                    getToast("success", "Update Profile Success!!!")
                },
                error: function (err) {
                    getToast("error", "Update Profile Failed: " + err.responseText)
                }
            })
        }
    }
    //Hàm kiểm tra dữ liệu profile
    function validateProfile(pUserObj) {
        "use strict"
        let vResult = true;
        if (pUserObj.firstname == "") {
            getToast("error", "First Name Must Not Empty");
            vResult = false;
        }
        else if (pUserObj.lastname == "") {
            getToast("error", "Last Name Must Not Empty");
            vResult = false;
        }
        else if (pUserObj.titleOfCourtesy == "") {
            getToast("error", "Gender Must Not Empty");
            vResult = false;
        }
        else if (pUserObj.birthDate == "") {
            getToast("error", "Birth Date Must Not Empty");
            vResult = false;
        }
        else if (pUserObj.email == "") {
            getToast("error", "Email Must Not Empty");
            vResult = false;
        }
        else if (pUserObj.homePhone == "") {
            getToast("error", "Home Phone Must Not Empty");
            vResult = false;
        }
        return vResult;
    }
    //Hàm truy xuất dữ liệu form profile
    function getEmployeeProfile(pUserObj) {
        pUserObj.firstname = $("#inp-firstname").val();
        pUserObj.lastname = $("#inp-lastname").val();
        pUserObj.titleOfCourtesy = $("#inp-courtesy").val();
        pUserObj.birthDate = $("#inp-birthdate").val();
        pUserObj.address = $("#inp-address").val();
        pUserObj.city = $("#inp-city").val();
        pUserObj.region = $("#inp-region").val();
        pUserObj.postalCode = $("#inp-postalcode").val();
        pUserObj.country = $("#inp-country").val();
        pUserObj.homePhone = $("#inp-homephone").val();
        pUserObj.extension = $("#inp-extension").val();
        pUserObj.email = $("#inp-email").val();
        pUserObj.photo = $("#inp-photo").val().split("\\").pop();
    }
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("Token", "", 1);
        window.location.href = "Login.html";
    }

    //Hiện thị div
    function showDivInfo(data) {
        if (data == "dashboard") {
            $(".tab-content #tab-dashboard").show();
            $(".tab-content #tab-profile").hide();
            $(".tab-content #tab-realestate").hide();
        }
        if (data == "profile") {
            $(".tab-content #tab-dashboard").hide();
            $(".tab-content #tab-profile").show();
            $(".tab-content #tab-realestate").hide();
        }
        if (data == "realestate") {
            $(".tab-content #tab-dashboard").hide();
            $(".tab-content #tab-profile").hide();
            $(".tab-content #tab-realestate").show();
        }
    }

    //Hiển thị thông tin người dùng
    function displayUser(data) {
        $("#hello-name").html(data.firstname + " " + data.lastname)
        $("#inp-name").val(data.firstname);
        $("#inp-id").val(data.userId);
        $("#inp-username").val(data.username);
        $("#inp-firstname").val(data.firstname);
        $("#inp-lastname").val(data.lastname);
    };

    //Hiển thị thông tin người dùng
    function displayUserMenu(data) {
        let vHTMl = `
                <a href="info.html">${data.firstname} ${data.lastname}</a>
                <a href="#" class="text-danger btn-logout">Logout</a> 
        `
        $(".info").append(vHTMl);
    };

    //Hàm get Cookie
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm setCookie
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    if (!gInfo.authorities.includes("ROLE_ADMIN" || "ROLE_MANEGER") || token == "") {
        $(".main-sidebar .nav-employee, .nav-province, .nav-district, .nav-ward, .nav-region-link, .nav-utility").hide();
    }
    else {
        $(".nav-all .nav-item").show();
    }
    // if(gInfo.authorities.includes("ROLE_HOMESELLER")) {
    //     $(".main-sidebar .nav-employee").show();
    // }

    if (window.location.href.toLocaleLowerCase().includes("info.html")) {
        var gEstateList = "";
        var gSTT = 1;
        var gCustomerList = "";
        var gNameCol = ["id", "censorred", "title", "type", "request", "address", "photo", "price", "acreage", "customerId", "dateCreate"];
        const gCOL_ID = 0;
        const gCOL_CENSORRED = 1;
        const gCOL_TITLE = 2;
        const gCOL_TYPE = 3;
        const gCOL_REQUEST = 4;
        const gCOL_ADDRESS = 5;
        const gCOL_PHOTO = 6;
        const gCOL_PRICE = 7;
        const gCOL_ACREAGE = 8;
        const gCOL_CUSTOMER = 9;
        const gCOL_DATE_CREATE = 10;
        var gTableEstate = $("#table-estate").DataTable({
            columns: [
                { data: gNameCol[gCOL_ID] },
                { data: gNameCol[gCOL_CENSORRED] },
                { data: gNameCol[gCOL_TITLE] },
                { data: gNameCol[gCOL_TYPE] },
                { data: gNameCol[gCOL_REQUEST] },
                { data: gNameCol[gCOL_ADDRESS] },
                { data: gNameCol[gCOL_PHOTO] },
                { data: gNameCol[gCOL_PRICE] },
                { data: gNameCol[gCOL_ACREAGE] },
                { data: gNameCol[gCOL_CUSTOMER] },
                { data: gNameCol[gCOL_DATE_CREATE] },
            ],
            columnDefs: [
                {
                    targets: gCOL_ID,
                    render: function (data) {
                        return data;
                    }
                },
                {
                    targets: gCOL_TYPE,
                    render: function (data) {
                        let vTypeName = new String();
                        switch (data) {
                            case 1:
                                vTypeName = "Nhà ở";
                                break;
                            case 2:
                                vTypeName = "Căn hộ/ Chung cư";
                                break;
                            case 3:
                                vTypeName = "Văn phòng";
                                break;
                            case 4:
                                vTypeName = "Kinh doanh";
                                break;
                            case 5:
                                vTypeName = "Phòng trọ";
                                break;
                            case 6:
                                vTypeName = "Đất nền";
                                break;
                        }
                        return vTypeName;
                    }
                },
                {
                    targets: gCOL_REQUEST,
                    render: function (data) {
                        let requestName = "";
                        if (data == 1) {
                            requestName = "Bán";
                        }
                        else if (data == 2) {
                            requestName = "Mua";
                        }
                        else if (data == 3) {
                            requestName = "Cho thuê";
                        }
                        else if (data == 4) {
                            requestName = "Thuê";
                        }
                        return requestName;
                    }
                },
                {
                    targets: gCOL_CUSTOMER,
                    render: function (data) {
                        let vCustomer = gCustomerList.filter(e => e.id == data)
                        return vCustomer == "" ? "" : vCustomer[0].contactName + " - " + vCustomer[0].mobile;
                    }
                },
                {
                    targets: gCOL_PHOTO,
                    render: function (data) {
                        return !data ? "" : `<img src="${gBASE_URL}/images/${data}" style="height: 100px; width: 100px"></img>`
                    }
                },
                {
                    targets: gCOL_CENSORRED,
                    render: function (data) {
                        return data ? `<i class="fas fa-check text-success"></i>` : `<i class="fas fa-times text-danger"></i>`;
                    },
                    className: "dt-center"
                }
            ],
            scrollX: false,
            autoWidth: true,
            responsive: true,
            searching: false,
            info: false,
            paging: false,
        })
        getAllCustomer();
        getAllEstateByUser(0, 10);
        loadDataToTable(gEstateList);
        //Hàm yêu cầu lấy dữ liệu All Estate từ server
        function getAllEstateByUser(pPage, pSize) {
            "use strict"
            $.ajax({
                url: "http://localhost:8080/employees/" + gInfo.userId + "/realestates"
                    + "?page=" + pPage + "&size=" + pSize,
                dataType: "json",
                type: "GET",
                async: false,
                headers: gHeader,
                success: function (res) {
                    console.log(res.content);
                    gEstateList = res.content;
                    gTotalPages = res.totalPages;
                    pagination(pPage + 1, gTotalPages);
                },
                error: function (err) {
                    alert(err.responseText);
                }
            })
        }

    }

    //Hàm load dữ liệu vào Bảng
    function loadDataToTable(paramEstate) {
        "use strict"
        gTableEstate.clear();
        gTableEstate.rows.add(paramEstate);
        gTableEstate.draw();
    }
    //Hàm ajax lấy dữ liệu Investor
    function getAllCustomer() {
        "use strict"
        $.ajax({
            url: "http://localhost:8080/employees/" + gInfo.userId + "/customers",
            dataType: "json",
            type: "GET",
            async: false,
            headers: gHeader,
            success: function (res) {
                console.log(res);
                gCustomerList = res;
            },
            error: function (err) {
                alert(err.responseText);
            }
        })
    }
    //Hàm yêu câu Get Employee By Id từ server
    function getEmployeeById(paramEmployeeId) {
        "use strict"
        $.ajax({
            url: gBASE_URL + "/employees/" + paramEmployeeId,
            dataType: "json",
            type: "GET",
            headers: gHeader,
            success: function (res) {
                console.log(res);
                loadDataEmployeeToForm(res);
            },
            error: function (err) {
                alert(err.responseText);
            }
        })
    }
    //Hàm load dữ liệu lên Form
    function loadDataEmployeeToForm(paramEmployeeData) {
        "use strict"
        $("#inp-firstname").val(paramEmployeeData.firstname);
        $("#inp-lastname").val(paramEmployeeData.lastname);
        $("#inp-title").val(paramEmployeeData.title);
        $("#inp-courtesy").val(paramEmployeeData.titleOfCourtesy);
        $("#inp-birthdate").val(paramEmployeeData.birthDate);
        $("#inp-hiredate").val(paramEmployeeData.hireDate);
        $("#inp-address").val(paramEmployeeData.address);
        $("#inp-city").val(paramEmployeeData.city);
        $("#inp-region").val(paramEmployeeData.region);
        $("#inp-postalcode").val(paramEmployeeData.postalCode);
        $("#inp-country").val(paramEmployeeData.country);
        $("#inp-homephone").val(paramEmployeeData.homePhone);
        $("#inp-extension").val(paramEmployeeData.extension);
        $("#inp-email").val(paramEmployeeData.email);
        // $("#inp-photo").attr("value",paramEmployeeData.photo);
        // $("#textarea-notes").val(paramEmployeeData.notes);
        // $("#select-reportsto").val(paramEmployeeData.reportsTo).change();
        // $("#select-activated").val(paramEmployeeData.activated).change();
        // $("#textarea-profiles").val(paramEmployeeData.profile);
        // $("#select-user-level").val(paramEmployeeData.userLevel).change();
    }

    //Hà xử lý search estate
    function advancedSearch(pPage, pSize) {
        let vSearchObj = {
            location: "",
            request: "",
            type: "",
            direction: "",
            acreage: "",
            priceMin: "",
            priceMax: "",
            customerId: "",
            startDate: "",
            endDate: "",
            censored: ""
        }
        getDataByFormSearch(vSearchObj);
        $.ajax({
            url: gBASE_URL + "/employees/" + gInfo.userId + "/realestates/advancedsearch"
                + "?location=" + vSearchObj.location + "&request=" + vSearchObj.request
                + "&type=" + vSearchObj.type + "&direction=" + vSearchObj.direction
                + "&acreage=" + vSearchObj.acreage + "&priceMin=" + vSearchObj.priceMin
                + "&priceMax=" + vSearchObj.priceMax + "&customerId=" + vSearchObj.customerId
                + "&startDate=" + vSearchObj.startDate + "&endDate=" + vSearchObj.endDate
                + "&censored=" + vSearchObj.censored + "&page=" + pPage + "&size=" + pSize,
            dataType: "json",
            type: "GET",
            async: false,
            headers: gHeader,
            success: function (res) {
                console.log(res);
                gEstateList = res.content;
                gTotalPages = res.totalPages;
                loadDataToTable(gEstateList);
                pagination(pPage + 1, gTotalPages);
                $("#advanced-search-modal").modal("hide");
                $("#btn-close-search").removeClass('d-none');
            },
            error: function (err) {
                alert(err.responseText);
            }
        })
    }
    //Hàm lấy dư liệu từ form search
    function getDataByFormSearch(pSearchObj) {
        pSearchObj.location = $("#inp-location-search").val();
        pSearchObj.request = $("#select-request-search").val();
        pSearchObj.type = $("#select-type-search").val();
        pSearchObj.direction = $("#select-direction-search").val();
        pSearchObj.acreage = $("#inp-acreage-search").val();
        pSearchObj.priceMin = $("#inp-price-min-search").val();
        pSearchObj.priceMax = $("#inp-price-max-search").val();
        pSearchObj.customerId = $("#select-customer-search").val();
        pSearchObj.startDate = $("#inp-date-start").val();
        pSearchObj.endDate = $("#inp-date-end").val();
        pSearchObj.censored = $('input[name=censoredRadio]:checked').val();
    }
    
})





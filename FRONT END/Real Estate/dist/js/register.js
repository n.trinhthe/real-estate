$("#btn-submit").on("click", function () {
    var vSignUpData = {
        firstname: '',
        lastname: '',
        username: '',
        password: ''
    }
    getDataByForm(vSignUpData);
    var vCheck = validateData(vSignUpData)
    if (vCheck == true){
        $.ajax({
        url: "http://localhost:8080/register",
        type: "POST",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(vSignUpData),
        success: function (res) {
            console.log(res)
            alert("Sign Up Success!");
            redirectToLogin();
        },
        error: function (err) {
            alert("Sign Up Failed: " + err.responseText)
        }
    })
    }
})
function getDataByForm(pData) {
    pData.firstname = $("#firstname").val();
    pData.lastname = $("#lastname").val();
    pData.username = $("#username").val();
    pData.password = $("#password").val();
}
function validateData(pData) {
    let vResult = true;
    if (pData.firstname == "") {
        alert("Firstname is empty");
        $("#firstname").focus();
        vResult = false;
    }
    else if (pData.lastname == "") {
        alert("Lastname is empty");
        $("#lastname").focus();
        vResult = false;
    }
    else if (pData.username == "") {
        alert("Username is empty");
        $("#username").focus();
        vResult = false;
    }
    else if (pData.password == "") {
        alert("Password is empty");
        $("#password").focus();
        vResult = false;
    }
    else if (pData.password.length < 6) {
        alert("Password phải có chiều dài tối thiểu 6 ký tự");
        $("#password").focus();
        vResult = false;
    }
    return vResult
}
function redirectToLogin() {
    window.location.href = "Login.html";
}
"use strict"
var gTotalPages = 0;
var gPage = 0;
var gSize = 24;
var gProvinceList = "";
var gDistrictList = "";
var gWardList = "";
var gStreetList = "";
var gResProperties = "";

$(document).on("click", "li .page-link", function () {
    let vPageIndex = $(this).data("index");
    getAllRealEstate(vPageIndex-1, gSize);
})
function pagination (pPageIndex){
    let vLiTagHTML = "";
    let active;
    // let pPageIndex = $(this).data("index");
    let vBeforePage = pPageIndex - 1;
    let vAfterPage = pPageIndex + 1;
    
    if (pPageIndex > 1) { //Nếu Index = 1 sẽ ẩn Button Prev
        vLiTagHTML += `<li class="page-item"><a class="page-link" data-index="${pPageIndex - 1}">Previous</a></li>`
    } else {
        vLiTagHTML += `<li class="page-item disabled"><a class="page-link disabled" data-index=${pPageIndex}>Previous</a></li>`
    }
    if (pPageIndex > 2) { //Nếu Index = 
        if (gTotalPages>3){
            vLiTagHTML += `<li class="page-item"><a class="page-link" href="javascript:void(0);" data-index="1">1</a></li>`
        }
        if (pPageIndex > 3) { //Nếu Index lớn hơn 3 sẽ thêm '...'
            vLiTagHTML += `<li class="page-item disabled"><a class="page-link disabled">...</a></li>`
        }
    }
    // how many pages or li show before the current li
    if (pPageIndex == gTotalPages) {
        vBeforePage = vBeforePage - 2;
    } else if (pPageIndex == gTotalPages - 1) {
        vBeforePage = vBeforePage - 1;
    }
    // how many pages or li show after the current li
    if (pPageIndex == 1) {
        vAfterPage = vAfterPage + 2;
    } else if (pPageIndex == 2) {
        vAfterPage = vAfterPage + 1;
    }
    for (var pLength = vBeforePage; pLength <= vAfterPage; pLength++) {
        if (pLength > gTotalPages) { //if plength is greater than totalPage length then continue
            continue;
        }
        if (pLength == 0) { //Nếu chiều dài = 0 thì thêm 1
            pLength = pLength + 1;
        }
        if (pPageIndex == pLength) { //Neeys trang bằng chiều dài thì active
            active = "active";
        } else {
            active = "";
        }
        vLiTagHTML += `<li class="page-item ${active}"><a class="page-link" href="javascript:void(0);" data-index="${pLength}">${pLength}</a></li>`;
    }
    if (pPageIndex < gTotalPages - 1) { //Nếu trang nhỏ hơn thì hiện thị <li>
        if (pPageIndex < gTotalPages - 2) { //Nếu trang có giá trị nhỏ hơn -2 tổng số trang thì hiện (...) /

            vLiTagHTML += `<li class="page-item disabled"><a class="page-link disabled">...</a></li>`;
        }
        if(gTotalPages > 3){ //Nếu tổng số trang lớn hơn 3 thì mới hiện tổng số trang
            vLiTagHTML += `<li class="page-item"><a class="page-link" href="javascript:void(0);" data-index="${gTotalPages}">${gTotalPages}</a></li>`
        }
    }

    if (pPageIndex < gTotalPages) { //Trang hiện tại nhot hơn tổng số trang thì index thêm 1
        vLiTagHTML += `<li class="page-item"><a class="page-link" href="javascript:void(0);" data-index="${pPageIndex + 1}">Next</a></li>`;
    }
    else {
        vLiTagHTML += `<li class="page-item disabled"><a class="page-link" href="javascript:void(0);" data-index="${pPageIndex}">Next</a></li>`;
    }
    $(".pagination").html("");
    $(".pagination").append(vLiTagHTML);
    console.log(pPageIndex);
}

//Xử lý tải trang
$(document).ready(function () {
    getPriceMax();
    getAllProvince();
    getAllDistrict();
    getAllWard();
    getAllStreet();
    let vStringURL = window.location.href;
    if (vStringURL.includes("?")) {
        searchPropertyByQuery();
    }
    else {
        getAllRealEstate(0, gSize);
    }

})

//Lọc theo giá và ngày
$("#sort-property").on("change", function () {
    var gPropertySort = "";
    console.log($(this).val())
    if ($(this).val() == "new") {
        gPropertySort = gResProperties.sort((a, b) => {
            return b.dateCreate.split("/").reverse().join("/").localeCompare(a.dateCreate.split("/").reverse().join("/"));
        })
    }
    else if ($(this).val() == "last") {
        gPropertySort = gResProperties.sort((a, b) => {
            return a.dateCreate.split("/").reverse().join("/").localeCompare(b.dateCreate.split("/").reverse().join("/"));
        })
    }
    else if ($(this).val() == "low") {
        gPropertySort = gResProperties.sort((a, b) => {
            return a.price - b.price;
        })
    }
    else if ($(this).val() == "hight") {
        gPropertySort = gResProperties.sort((a, b) => {
            return b.price - a.price;
        })
    }
    loadDataToProperties(gPropertySort);
})

//Hàm yêu cầu Get All Real Estate từ server
function getAllRealEstate(page, size) {
    $.ajax({
        url: gBASE_URL + "/realestates/censorred?page=" + page + "&size=" + size,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gResProperties = res.content;
            gTotalPages = res.totalPages;
            console.log(gResProperties);
            loadDataToProperties(gResProperties);
            pagination(page+1);
        },
        error: function (err) {
            console.log(err.responseText);
        }
    })
}

//Hàm load dữ liệu lên trang danh mục
function loadDataToProperties(pResProperties) {
    "use strict"
    $(".properties").html("")
    for (let property of pResProperties) {
        // console.log(property.request);
        var requestName = "";
        if (property.request == 1) {
            requestName = "Bán";
        }
        else if (property.request == 2) {
            requestName = "Mua";
        }
        else if (property.request == 3) {
            requestName = "Cho thuê";
        }
        else if (property.request == 4) {
            requestName = "Thuê";
        }

        let vDirection = ""; 
        if (property.direction == 1) {
            vDirection = "W";
        }
        else if (property.direction == 2) {
            vDirection = "N";
        }
        else if (property.direction == 3) {
            vDirection = "E";
        }
        else if (property.direction == 4) {
            vDirection = "S";
        }
        else if (property.direction == 5) {
            vDirection = "WS";
        }
        else if (property.direction == 6) {
            vDirection = "NE";
        }
        else if (property.direction == 7) {
            vDirection = "NS";
        }
        else if (property.direction == 0) {
            vDirection = "NW";
        }
        let vArrayAddress = [property.address]
        let vStreet = gStreetList.filter(e => e.id == property.streetId);
        vStreet == "" ? vArrayAddress : vArrayAddress.push([vStreet[0].prefix, vStreet[0].name].join(" "));
        let vWard = gWardList.filter(e => e.id == property.wardId);
        vWard == "" ? vArrayAddress : vArrayAddress.push([vWard[0].prefix, vWard[0].name].join(" "));
        let vDistrict = gDistrictList.filter(e => e.id == property.districtId);
        vDistrict == "" ? vArrayAddress : vArrayAddress.push([vDistrict[0].prefix, vDistrict[0].name].join(" "));
        let vProvince = gProvinceList.filter(e => e.id == property.provinceId);
        vProvince == "" ? vArrayAddress : vArrayAddress.push(vProvince[0].name);
        let vLocation = vArrayAddress.join(", ")
        // if (property.request == 0 || property.request == 2) {
        let vHTML = `<div class="card">
            <span class="badge badge-secondary">${requestName}</span>
            <img class="card-img-top" src="${gBASE_URL}/images/${property.photo}" alt="Card image">
                <div class="card-body d-flex align-items-end flex-column">
                    <h4 class="card-title w-100">${!property.title ? "" : property.title}</h4>
                    <p class="card-text text-left text-monospace w-100">${vLocation}</p>
                    <div class="form-group utility w-100">
                        <div class=" d-flex flex-column">
                            <i class="fa-solid fa-vector-square text-center"></i>
                            <span class="text-center">${property.acreage} m2</span>
                        </div>
                        <div class="d-flex flex-column">
                            <i class="fa-solid fa-bath text-center"></i>
                            <span class="text-center">${!property.bath ? "" : property.bath}</span>
                        </div>
                        <div class="d-flex flex-column">
                            <i class="fa-solid fa-bed text-center"></i>
                            <span class="text-center">${!property.bedroom ? "" : property.bedroom}</span>
                        </div>
                        <div class="d-flex flex-column">
                            <i class="fas fa-location-arrow text-center"></i>
                            <span class="text-center">${vDirection}</span>
                        </div>
                    </div>
                    <div class="w-100">
                        <div class="row d-flex justify-content-between">
                            <p class="card-text text-left font-weight-bold">Price: <span
                                    class="font-italic ">${formatPrice(property.price)}</span> $
                            </p>
                            <span class="text-right">Date: ${property.dateCreate}</span>
                        </div>
                    </div>
                    <div class="w-100 mt-auto">
                        <a data-id="${property.id}" class="btn btn-warning btn-see-more w-100">See More</a>
                    </div>
                </div>
            </div>`
        $(".properties").append(vHTML);
        // }
        // else if (property.request == 3 || property.request == 4) {
        //     let vHTML = `
        //         <div class="card">
        //             <span class="badge badge-secondary">${property.requestName}</span>
        //             <img class="card-img-top" src="./dist/picture/${property.photo}" alt="Card image">
        //             <div class="card-body">
        //                 <h4 class="card-title">${property.title}</h4>
        //                 <p class="card-text text-left text-monospace">${property.address}</p>
        //                 <p class="card-text text-left font-weight-bold">Price: <span class="font-italic ">${property.priceRent}</span>
        //                         $ <span class="font-italic ">/month </span> </p>
        //                 <hr>
        //                 <a data-id="${property.id}" class="btn btn-warning btn-see-more">See More</a>
        //             </div>
        //         </div>`;
        //     $(".properties").append(vHTML);
        // }
    }
}
//hàm xử lý nút See more
$(document).on("click", ".btn-see-more", function () {
    let vPropertyId = $(this).data("id"); //Lấy id từ nút
    window.location.href = "propertydetail.html?propertyId=" + vPropertyId;
})

//Hàm Goi API lấy Province
function getAllProvince() {
    $.ajax({
        url: "http://localhost:8080/provinces",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gProvinceList = res;
        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
}

//Hàm Goi API lấy District
function getAllDistrict() {
    $.ajax({
        url: "http://localhost:8080/districts",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gDistrictList = res;
        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
}

//Hàm Goi API lấy Ward
function getAllWard() {
    $.ajax({
        url: "http://localhost:8080/wards",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gWardList = res;
        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
}

//Hàm Goi API lấy Street
function getAllStreet() {
    $.ajax({
        url: "http://localhost:8080/streets",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            gStreetList = res;
        },
        error: function (err) {
            console.log(err.responseText)
        }
    })
}

/* ----------------------------------------------------------------------------*/
$("#range-price").on("change", function () {
    $("#price-search").html(formatPrice($(this).val())); 
    console.log($(this).val())
})
$("#btn-search").on("click", function () {
    $(".pagination").html("");
    let vSearchObj = {
        title: "",
        type: "",
        request: "",
        price: "",
        acreage: ""
    }
    getDataToFormSearch(vSearchObj);
    console.log(vSearchObj);
    $.ajax({
        url: gBASE_URL + "/realestates/search?" + "title=" + vSearchObj.title
            + "&price=" + vSearchObj.price + "&request=" + vSearchObj.request
            + "&type=" + vSearchObj.type + "&acreage=" + vSearchObj.acreage,
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log(res);
            loadDataToProperties(res);
            $(".div-search").find("#btn-search-close").remove();
            $(".div-search").append("<button id='btn-search-close' class='btn btn-warning'><i class='fas fa-times-circle'></i></button>")
        },
        error: function (err) {
            console.log(err.responseText);
        }
    })
})

//Hàm lấy dữ lieeij từ form search
function getDataToFormSearch(pSearchObj) {
    "use strict"
    pSearchObj.title = $("#title-search").val();
    pSearchObj.type = $("input[name='type']:checked").val();
    pSearchObj.request = $("input[name='request']:checked").val();
    pSearchObj.price = $("#range-price").val();
    pSearchObj.acreage = $("#inp-acreage").val();
}

$(document).on("click", "#btn-search-close", function () {
    "use strict"
    $(".pagination").html("");
    $(".div-search").find("#btn-search-close").remove();
    getAllRealEstate(0,gSize);
})

function searchPropertyByQuery() {
    let vStringURL = window.location.href;
    let vURL = new URL(vStringURL);
    let vTitle = vURL.searchParams.get("title");
    let vPrice = vURL.searchParams.get("price");
    let vRequest = vURL.searchParams.get("request");
    let vType = vURL.searchParams.get("type");
    let vAcreage = vURL.searchParams.get("acreage");
    $.ajax({
        url: gBASE_URL + "/realestates/search?" + "title=" + vTitle
            + "&price=" + vPrice + "&request=" + vRequest
            + "&type=" + vType + "&acreage=" + vAcreage,
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log(res);
            loadDataToProperties(res);
            $(".div-search").find("#btn-search-close").remove();
            $(".div-search").append("<button id='btn-search-close' class='btn btn-warning'><i class='fas fa-times-circle'></i></button>")
        },
        error: function (err) {
            console.log(err.responseText);
        }
    })
}
//Ẩn hiện Div Search
$(".div-top .icon").on("click", function () {
    $("#div-search").toggle();
})

//Hàm lấy giá trị price Max
function getPriceMax(){
    $.ajax({
        url: gBASE_URL + "/realestates/pricemax",
        type: "GET",
        dataType: "json",
        success: function (res) {
           let price = formatPrice(res);
           $("#range-price").attr({"max": res, "value": res/2});
           $("#p-price-max").html(price);
           $("#price-search").html(formatPrice(res/2));
        },
        error: function (err) {
            console.log(err.responseText);
        }
    })
}

